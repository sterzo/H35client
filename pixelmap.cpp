/****************************************************************************
** Authors: Stefano Terzo (stefano.terzo@cern.ch)
**
** Institut de Física d'Altes Energies (IFAE)
** Edifici CN UAB Campus
** 08193 Bellaterra (Barcelona), Spain
**
****************************************************************************/

#include "pixelmap.h"
#include <QRgb>

PixelMap::PixelMap(int matrixID, int cols, int rows, int xp, int yp, int zp, QWidget *parent) :
    matrixID_(matrixID), cols_(cols), rows_(rows), xp_(xp),yp_(yp), zp_(zp), QLabel(parent)
{
    pixMap_ = new QImage(cols_*xp_,rows_*yp_,QImage::Format_RGB32);
    pixMap_->fill(QColor(Qt::green).rgb());
    this->setPixmap(QPixmap::fromImage(*pixMap_));
    this->setFixedSize(pixMap_->size());

    for(int c=0; c<cols_; c++){
        std::vector<int> row;
        map_.push_back(row);
        for(int r=0; r<rows_; r++){
            map_.back().push_back(0);
        }
    }

    nmoscode_.reset();
    //this->setMaskFromCode(nmoscode_);
}

PixelMap::~PixelMap() {
    delete pixMap_;
}

void PixelMap::updateMap(){
    this->setPixmap(QPixmap::fromImage(*pixMap_));
}

void PixelMap::setPixel(int col, int row, bool on){
    int color = 0;
    if(on) color = 255;
    for(int c=col * xp_; c<col * xp_+xp_; ++c) {
        for (int r = row * yp_; r < row * yp_ + yp_; ++r) {
            pixMap_->setPixel(c, r, qRgb(color, 0., 0.));
        }
    }
    map_[col][row] = on*zp_;
}

void PixelMap::setPixel(int col, int row, int val){
    int color = 255/zp_*val;
    for(int c=col * xp_; c<col * xp_+xp_; ++c) {
        for (int r = row * yp_; r < row * yp_ + yp_; ++r) {
            pixMap_->setPixel(c, r, qRgb(color, 0., 0.));
        }
    }
    map_[col][row] = val;
}

void PixelMap::setCol(int col, bool on){
        for (int r =0; r < rows_; ++r) {
            this->setPixel(col, r, on);
        }
}

void PixelMap::setCol(int col, int val){
    for (int r =0; r < rows_; ++r) {
        this->setPixel(col, r, val);
    }
}

void PixelMap::setRow(int row, bool on){
    for (int c =0; c < cols_; ++c) {
        this->setPixel(c, row, on);
    }
}

void PixelMap::setRow(int row, int val){
    for (int c =0; c < cols_; ++c) {
        this->setPixel(c, row, val);
    }
}

void PixelMap::setAllPixels(bool on){
    for(int c=0; c<cols_; ++c) {
        for (int r =0; r < rows_; ++r) {
            this->setPixel(c,r,on);
        }
    }
}

void PixelMap::setAllPixels(int val){
    for(int c=0; c<cols_; ++c) {
        for (int r =0; r < rows_; ++r) {
            this->setPixel(c,r,val);
        }
    }
}

bool PixelMap::getPixelStatus(int col, int row){
    return map_[col][row] > 0;
}

int PixelMap::getPixelValue(int col, int row) {
    return map_[col][row];
}

void PixelMap::mouseDoubleClickEvent(QMouseEvent *event){
    this->setPixel(event->x()/xp_, event->y()/yp_,!getPixelStatus(event->x()/xp_,event->y()/yp_));
    this->updateMap();
    emit pixelClicked(matrixID_,event->x()/xp_,event->y()/yp_);
}

void PixelMap::mousePressEvent(QMouseEvent *event){
    emit pixelClicked(matrixID_,event->x()/xp_,event->y()/yp_);
}

void PixelMap::setMaskFromCode(Client::nmoscode code) {
    //CHECK BIT COLUMN
    int col = cols_;
    for(int c=81; c < 1280; c+=4){
        col--;
        if(!code.test(c)) {
            this->setCol(col, false);
            continue;
        }
        //CHECK BIT ROW
        int row = rows_;
        for(int r=1283; r < code.size()-128; r+=4){
            row--;
            if(code.test(r)){
                this->setPixel(col,row,true);
            }
            else{
                this->setRow(row,false);
            }
        }
    }
    this->updateMap();
}
