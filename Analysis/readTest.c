#include <map>
#include <TH2D.h>
#include <TF1.h>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#include <sstream>
#include <stdlib.h>


#include <TROOT.h>
#include <TRandom.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TH1.h>
#include <TH2.h>
#include <TF1.h>
#include <TFile.h>
#include <TMatrixDSym.h>
#include <TMath.h>
#include <TVirtualFitter.h>
#include <TMinuit.h>
#include <TGraph.h>
#include <TGraphAsymmErrors.h>

#include "calc_eff.C"

//#define NINJECTIONS 25

class ThresholdScan {

public:
  ThresholdScan(std::string _filename);


  std::map<int , std::vector<TH2D*> > hitmap;
  std::map<int , std::vector<double> > amplitude;

  std::string filename;

  std::map<int, std::map<int, std::map<int , TGraphAsymmErrors* > > > SCurveGraph;
    //std::map<int, TGraphAsymmErrors*[300][16] > SCurveGraph;

  void FillHitmap();
  void SCurve(int test, int col, int row);
  static double SCurveFunc(double * x, double * par);
  TF1 * SCurveFit(TGraphAsymmErrors *his, double lo_range, double hi_range, double half_maximum);

  std::map<int, TH2D*> thresholdmap;
  std::map<int, TH1D*> thresholddist;

  std::map<int, TH2D*> noisemap;
  std::map<int, TH1D*> noisedist;
  
  //double ninjections;
  int NINJECTIONS;

  void SaveHistos(TString outfile);

  
  
};

ThresholdScan::ThresholdScan(std::string _filename): filename(_filename)
{
    
    FillHitmap();
    
    for(auto t : amplitude){
        thresholdmap[t.first] = new TH2D(Form("thresholdmap%i",t.first),"Threshold map [V]; column ; row ; Threshold [V]",300,0,300,16,0,16);
        
        thresholddist[t.first] = new TH1D(Form("thresholddist%i",t.first),"Threshold distribution [V]; Threshold [V]; NPixel",t.second.size(),t.second.front(),t.second.back());//hitmap.size());
        
        noisemap[t.first] = new TH2D(Form("noisemap%i",t.first),"Noise map [V]; column; row; Noise [V]",300,0,300,16,0,16);
        
        noisedist[t.first] = new TH1D(Form("noisedist%i",t.first),"Noise distribution; Noise[V]; NPixel",200,0,t.second.back()/10);
        
        for(int icol = 0; icol < 300; icol++){
            for(int irow = 0; irow < 16; irow++){
                //SCurveGraph[icol][irow] = new TGraphAsymmErrors(Form("SCurveGraph_%02i_%01i",icol,irow),Form("SCurveGraph_%02i_%01i",icol,irow),amplitude.size(),amplitude[0],amplitude.back());
                SCurve(t.first,icol,irow);
            }
        }
        
    }
    
}



void ThresholdScan::FillHitmap(){


  std::fstream infile;
  infile.open (filename.c_str(), std::fstream::in);

  if(infile.is_open()){
    std::cout << "file " << filename << " successfully opened!\n";
  }
  else {
    std::cout << "failed to open file " << filename << " \n";
 	exit(0);
  }

  std::string line;

  while(infile.good()){

      getline(infile,line);

        if(line.find("THRESHOLD_SCAN") != std::string::npos || line.find("TEST") != std::string::npos) {
            //cout << "Found threshold scan!" << endl;
        }
        else if(line.find("Inj:") != string::npos) {
            stringstream ss(line);
            string buf;

            //Injection
            ss >> buf;
            ss >> buf;
            NINJECTIONS = atof(buf.c_str());

            //Timeout
            ss >> buf;
            ss >> buf;

            //Amplitude
            ss >> buf;
            ss >> buf;
            float ampl = atof(buf.c_str());
            
            //Test
            ss >> buf;
            ss >> buf;
            int test = atof(buf.c_str());
            amplitude[test].push_back(ampl);
            
            hitmap[test].push_back(new TH2D(Form("histo%lu_test%i",hitmap[test].size(),test),Form("histo%lu_test%i",hitmap[test].size(),test),300,0,300,16,0,16));
        }
        else {
            //cout << "data!" << endl;
            stringstream ss(line);
            string buf;
            int col, row, ts, fts;

            ss >> buf;
            col = atoi(buf.c_str());
            //cout << "column: " << col;
            ss >> buf;
            row = atoi(buf.c_str());
            //cout << "  row: " << row << endl;
            ss >> buf;
            ts = atoi(buf.c_str());
            
            ss >> buf;
            fts = atoi(buf.c_str());

            //hitmap.back()->Fill(col,row);
        }
  }
  
  infile.close();
  
}

double ThresholdScan::SCurveFunc(double * x, double * par)
{
    double curve;
    double erf_arg;
    double sqrt2 = sqrt(2.);
    double ynorm;
    ynorm = (x[0] - par[0]) / par[1];
    if (ynorm>0)
    {
        erf_arg = -ynorm/sqrt2;
        curve = TMath::Erfc(erf_arg)/2.;
    }
    else
    {
        erf_arg = ynorm/sqrt2;
        curve = 1. - TMath::Erfc(erf_arg)/2.;
    }
    
    // TODO: Have to add amplitude here -> Currently 50 scans
    return par[2]*curve;
}

TF1* ThresholdScan::SCurveFit(TGraphAsymmErrors *his, double lo_range, double hi_range, double half_maximum)
{
    
    TF1 * fitCurve = new TF1("SCurveFunc", SCurveFunc, lo_range, hi_range, 3);
    fitCurve->SetLineColor(kRed);
    
    fitCurve->SetParameter(0,half_maximum);
    fitCurve->SetParameter(1,1);
    //fitCurve->SetParameter(2,NINJECTIONS);
    
    fitCurve->FixParameter(2,NINJECTIONS);
    
    his->Fit("SCurveFunc","RBQ");//"ERBNQ0");
    
    return fitCurve;
}


void ThresholdScan::SCurve(int test, int col, int row){
    
    bool found_maximum = false;
    vector <double> fill;
    vector <double> fill_e_l;
    vector <double> fill_e_h;
    
    // Simple approach to find the threshold: Check when
    // half maximum is reached
    double half_maximum = 0;
    
    for(unsigned int i = 0; i < hitmap[test].size(); i++){
        fill.push_back(hitmap[test][i]->GetBinContent(col+1,row+1));
        
        // create temporary variables for errors
        // NOTE: return value of calc_eff is in % -> have to multiply by
        // NINJECTIONS
        double e_l_temp, e_h_temp = 0;
        calc_eff(NINJECTIONS,fill.back(),(e_h_temp),(e_l_temp));
        fill_e_l.push_back(NINJECTIONS*e_l_temp);
        fill_e_h.push_back(NINJECTIONS*e_h_temp);
        //TODO: This is wrong if I change binning!!
        //SCurveHisto[col][row]->SetBinContent(i+1,fill);
        
        
        if (fill[i] > NINJECTIONS/2. && found_maximum == false) {
            half_maximum = amplitude[test][i];
            found_maximum = true;
        }
        
    }
    
    SCurveGraph[test][col][row] = new TGraphAsymmErrors(amplitude[test].size(),&(amplitude[test][0]),&(fill[0]),0,0,&(fill_e_l[0]),&(fill_e_h[0]));
    SCurveGraph[test][col][row]->SetTitle(Form("SCurve Pixel %i %i - Test: %i",col,row,test));
    SCurveGraph[test][col][row]->GetXaxis()->SetTitle("Threshold [V]");
    SCurveGraph[test][col][row]->GetYaxis()->SetTitle("NEntries");
    
    
    TF1 * f = SCurveFit(SCurveGraph[test][col][row],amplitude[test].front(),amplitude[test].back(),half_maximum);
    double threshold = f->GetParameter(0);
    double noise = f->GetParameter(1);
    
    //check if it was a good fit by comparing it to the simple method
    if(abs(threshold - half_maximum) < amplitude[test].back()/2. && threshold > 0) {
        thresholdmap[test]->SetBinContent(col+1,row+1,threshold);
        thresholddist[test]->Fill(threshold);
        noisemap[test]->SetBinContent(col+1,row+1,noise);
        noisedist[test]->Fill(noise);
    }
    else {
        //    printf("Using Half maximum method \n");
        thresholdmap[test]->SetBinContent(col+1,row+1,half_maximum);
        thresholddist[test]->Fill(half_maximum);
    }
    
}




void ThresholdScan::SaveHistos(TString outfile){
    TFile f("Threshold_test.root", "RECREATE");
    for(auto t : amplitude){
//        TDirectory *cdtest = f.mkdir(Form("Test%i",t.first));
//        cdtest->cd();
        
        gStyle->SetOptStat(0);
        TCanvas * c1 = new TCanvas(Form("Threshold map test%i",t.first));
        c1->SetRightMargin(c1->GetRightMargin()*1.5);
        thresholdmap[t.first]->Draw("COLZ");
        c1->SaveAs(outfile+"[");
        c1->SaveAs(outfile);
        
        TCanvas * c2 = new TCanvas(Form("Threshold distribution test%i",t.first));
        thresholddist[t.first]->GetYaxis()->SetTitleOffset(thresholddist[t.first]->GetYaxis()->GetTitleOffset()*1.2);
        thresholddist[t.first]->Draw();
        c2->SaveAs(outfile);
        
        TCanvas * c3 = new TCanvas(Form("Noise map test%i",t.first));
        c3->SetRightMargin(c3->GetRightMargin()*1.5);
        noisemap[t.first]->Draw("COLZ");
        noisemap[t.first]->SetMaximum(3*noisedist[t.first]->GetMean());
        c3->SaveAs(outfile);
        
        TCanvas * c4 = new TCanvas(Form("Noise distribution test%i",t.first));
        noisedist[t.first]->Draw();
        c4->SaveAs(outfile);
        
        
        TCanvas * c5 = new TCanvas(Form("Example S Curve test%i",t.first));
        TRandom rand;
        int col = 0;//rand.Integer(150);
        int row = 15;//rand.Integer(16);
        SCurveGraph[t.first][col][row]->Draw("AP");
        c5->SaveAs(outfile);
        
        
        TCanvas * c6 = new TCanvas(Form("Example S Curve#2 test%i",t.first));
        col = rand.Integer(150)+150;
        row = rand.Integer(16);
        SCurveGraph[t.first][col][row]->Draw("AP");
        c6->SaveAs(outfile);
        c6->SaveAs(outfile+"]");
        
        cout<<"PDF created in "<<outfile<<endl;
        
        TDirectory *cdscurve = f.mkdir(Form("SCurves_test%i",t.first));
        cdscurve->cd();
        
        for(int icol=0;icol<300;icol++){
            for(int irow=0;irow<16;irow++){
                SCurveGraph[t.first][icol][irow]->Write(Form("SCurve_%i_%i_test%i",icol,irow,t.first));
            }
        }
        
        TDirectory *cdfscurve = f.mkdir(Form("Failed_SCurves_test%i",t.first));
        cdfscurve->cd();
        
        for(int icol=0;icol<300;icol++){
            for(int irow=0;irow<15;irow++){
                if(noisemap[t.first]->GetBinContent(icol+1, irow+1) == 0){
                    SCurveGraph[t.first][icol][irow]->Write(Form("SCurve_%i_%i_test%i",icol,irow,t.first));
                }
            }
        }
        
    }
    
}

void readTest(TString filename){
    if(filename==""){
        filename = "output.txt";
    }
    
    ThresholdScan * scan = new ThresholdScan(filename.Data());
    
    TString outfile = filename;
    outfile.Remove(outfile.Last('.'),outfile.Sizeof()-outfile.Last('.'));
    outfile += ".pdf";
    
    scan->SaveHistos(outfile);
    
}
