//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Tue Nov  8 14:28:48 2016 by ROOT version 5.30/01
// from TTree Hits/Hits
// found on file: cosmic_011044.root
//////////////////////////////////////////////////////////

#ifndef DUT_h
#define DUT_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

class DUT {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

   // Declaration of leaf types
   Int_t           NHits;
   Int_t           PixX[10000];   //[NHits]
   Int_t           PixY[10000];   //[NHits]
   Int_t           Value[10000];   //[NHits]
   Int_t           Timing[10000];   //[NHits]
   Int_t           BCID[10000];   //[NHits]
   Int_t           HitInCluster[10000];   //[NHits]
   Double_t        PosX[10000];   //[NHits]
   Double_t        PosY[10000];   //[NHits]
   Double_t        PosZ[10000];   //[NHits]

   // List of branches
   TBranch        *b_NHits;   //!
   TBranch        *b_PixX;   //!
   TBranch        *b_PixY;   //!
   TBranch        *b_Value;   //!
   TBranch        *b_Timing;   //!
   TBranch        *b_BCID;   //!
   TBranch        *b_HitInCluster;   //!
   TBranch        *b_PosX;   //!
   TBranch        *b_PosY;   //!
   TBranch        *b_PosZ;   //!

   DUT(int planeID=0,TTree *tree = 0);
   virtual ~DUT();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop(){};
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef Correlator_cxx
DUT::DUT(int planeID,TTree *tree)
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("cosmic_011068_conv.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("cosmic_011068_conv.root");
      }
      TDirectory * dir = (TDirectory*)f->Get(Form("cosmic_011068_conv.root:/Plane%i",planeID));
      dir->GetObject("Hits",tree);

   }
   Init(tree);
}

DUT::~DUT()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t DUT::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t DUT::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void DUT::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("NHits", &NHits, &b_NHits);
   fChain->SetBranchAddress("PixX", PixX, &b_PixX);
   fChain->SetBranchAddress("PixY", PixY, &b_PixY);
   fChain->SetBranchAddress("Value", Value, &b_Value);
   fChain->SetBranchAddress("Timing", Timing, &b_Timing);
   fChain->SetBranchAddress("BCID", BCID, &b_BCID);
   fChain->SetBranchAddress("HitInCluster", HitInCluster, &b_HitInCluster);
   fChain->SetBranchAddress("PosX", PosX, &b_PosX);
   fChain->SetBranchAddress("PosY", PosY, &b_PosY);
   fChain->SetBranchAddress("PosZ", PosZ, &b_PosZ);
   Notify();
}

Bool_t DUT::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void DUT::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t DUT::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef DUT_cxx
