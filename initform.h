/****************************************************************************
** Authors: Stefano Terzo (stefano.terzo@cern.ch)
**
** Institut de Física d'Altes Energies (IFAE)
** Edifici CN UAB Campus
** 08193 Bellaterra (Barcelona), Spain
**
****************************************************************************/

#ifndef INITFORM_H
#define INITFORM_H

#include <QString>
#include <Qt>

#include <sstream>
#include <bitset>
#include <QLineEdit>
#include <QListWidgetItem>
#include <build/ui_initform.h>
#include <QSettings>
#include <QCompleter>
#include <QMenu>
#include <QMessageBox>

#include <QtConcurrent/QtConcurrent>
#include <QFutureWatcher>

#include "Instrument.h"
#include "pageForm.h"

namespace Ui {
class InitForm;
}

class InitForm : public PageForm
{
    Q_OBJECT

public:
    explicit InitForm(ClientWidget *client, QWidget *parent = 0);
    ~InitForm();

    QLineEdit *getAddressLineEdit(){return ui->address_lineEdit;}

    Instrument* getInstrument(int id){return instruments_[id];}

signals:
    void activeTcpSession(bool active);
    void updateInstruments(std::vector<Instrument*> *instruments);
    void loadConf(void);
    void saveConfAs();

public slots:
    void on_configure_pushButton_clicked();

    void on_initBias_pushButton_clicked();

    bool openTcpSession();

    void closeTcpSession();

    void resetCheckboxes();

private slots:
    void on_open_tcp_session_pushbutton_clicked(bool checked);

    void on_addDevice_pushButton_clicked();

    void on_removeDevice_pushButton_clicked();

    void on_loadConf_pushButton_clicked();

    void on_newConf_pushButton_clicked();

    void on_searchPort_pushButton_clicked();

    void displayDeviceMenu(const QPoint &pos);

private:
    Ui::InitForm *ui;

    ClientWidget *client_;

    std::vector<Instrument*> instruments_;
    std::vector<QCheckBox*> matrixResponse_checkBoxs_;
    QWidget *confOptList_;

    QMap<QString, QVariant> savedDevices_;

    bool abort_;

    QFutureWatcher<void> futureWatcher_;

    bool addDevice(QString devId);
    bool addDevice(Instrument::Interface interface, std::string address, int port);

    void searchPort();
    int connectDevice(int instr);
};

#endif // TESTFORM_H
