/****************************************************************************
** Created by Stefano Terzo (stefano.terzo@cern.ch)
**
** Institut de Física d'Altes Energies (IFAE)
** Edifici CN UAB Campus
** 08193 Bellaterra (Barcelona), Spain
**
****************************************************************************/

#include "mainwindow.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    //TApplication *rootapp = new TApplication("Simple Qt ROOT Application", &argc, argv);
    //rootapp->SetReturnFromRun(true);

    QApplication a(argc, argv);

    MainWindow w;
    w.show();

    return a.exec();
}
