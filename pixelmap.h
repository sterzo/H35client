/****************************************************************************
** Authors: Stefano Terzo (stefano.terzo@cern.ch)
**
** Institut de Física d'Altes Energies (IFAE)
** Edifici CN UAB Campus
** 08193 Bellaterra (Barcelona), Spain
**
****************************************************************************/

#ifndef PIXELMAP_H
#define PIXELMAP_H

#include <stdio.h>
#include <iostream>
#include <math.h>

#include <QWidget>
#include <QLabel>
#include <QMouseEvent>

#include "Client.h"

class PixelMap : public QLabel
{
    Q_OBJECT

public:
    PixelMap(int matrixID, int cols, int rows, int xsize = 1, int ysize = 1, int zsize = 1, QWidget *parent = 0);
    ~PixelMap();

    void updateMap();
    Client::nmoscode getCode(){return nmoscode_;}
    void setPixel(int c, int r, bool on);
    void setPixel(int col, int row, int val);
    bool getPixelStatus(int col, int row);
    int getPixelValue(int col, int row);
    void setAllPixels(bool on);
    void setAllPixels(int val);
    void setCol(int col, bool on);
    void setCol(int col, int val);
    void setRow(int row, bool on);
    void setRow(int col, int val);

public slots:
    void setMaskFromCode(Client::nmoscode code);

signals:
    void pixelClicked(int matrix,int col, int row);

private:
    QImage *pixMap_;

    int xp_,yp_,zp_,cols_,rows_,matrixID_;

    void mouseDoubleClickEvent (QMouseEvent *event);
    void mousePressEvent(QMouseEvent *event);

    Client::nmoscode nmoscode_;

    std::vector< std::vector<int> > map_;
};

#endif // PIXELMAP_H
