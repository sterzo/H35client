#!/bin/bash

if [[ $1 = "" ]] || [[ $1 = "1" ]]; then
    IP="172.16.15.21"
elif [[ $1 = "2" ]]; then
   IP="172.16.17.153"
else
   IP=$1
fi

echo "connecting to FPGA board: $IP"

ssh-keygen -R $IP && scp *.elf root@$IP:

echo "chmod +x *" > .profile
echo "./test_H35stdalone_universal.elf" >> .profile

#be careful with sshpass:"it makes it too easy for novice SSH users to ruin SSH's security."
sshpass -p "root" scp .profile root@$IP:
sshpass -p "root" ssh root@$IP
