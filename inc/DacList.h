/****************************************************************************
** Created by Stefano Terzo (stefano.terzo@cern.ch) on 24/03/17
**
** Institut de Física d'Altes Energies (IFAE)
** Edifici CN UAB Campus
** 08193 Bellaterra (Barcelona), Spain
**
****************************************************************************/
#ifndef H35CLIENT_DACLIST_H
#define H35CLIENT_DACLIST_H

#include <vector>

#include <QWidget>
#include <QGridLayout>
#include <QLabel>
#include <QSpinBox>
#include <QAbstractSpinBox>
#include <QCheckBox>

class DacList : public QWidget{
Q_OBJECT

public:
    DacList(const std::vector<unsigned int> defaultInternalDacs, QWidget *parent = 0);

    std::vector<QSpinBox*> getInternalDacSpinBoxes(){return internalDacSpinBoxes_;}
    std::vector<QCheckBox*> getInternalDacCheckBoxes(){return internalDacCheckBoxes_;}
//    std::vector<QSpinBox*> getExternalDacSpinBoxes(){return externalDacSpinBoxes_;}

signals:
    void valueChanged(void);

private slots:
    void emitValueChanged(){emit valueChanged();}

private:
    std::vector<QSpinBox*> internalDacSpinBoxes_;
    //std::vector<QSpinBox*> externalDacSpinBoxes_;
    std::vector<QCheckBox*> internalDacCheckBoxes_;
};

#endif //H35CLIENT_DACLIST_H
