/****************************************************************************
** Created by Stefano Terzo (stefano.terzo@cern.ch)  on 04/10/16
**
** Institut de Física d'Altes Energies (IFAE)
** Edifici CN UAB Campus
** 08193 Bellaterra (Barcelona), Spain
**
****************************************************************************/

#ifndef H35CLIENT_PULSEGENERATOR_H
#define H35CLIENT_PULSEGENERATOR_H

#include "Instrument.h"

class PulseGenerator : public Instrument {

public:
    PulseGenerator(int interface, std::string address, int port) : Instrument(interface,address,port) { }

    void setDisplayOn(bool on = true);

    void setOutputLoad(int ohms);

    void setPulse(float width_sec, float freq_hz);

    void setAmplitude(float volts);

    void setBurst(int n);

    void setOutput(bool on = true, int channel = 1);

    std::string status();

    void trigger();
};

#endif //H35CLIENT_PULSEGENERATOR_H
