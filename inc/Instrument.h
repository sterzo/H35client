/****************************************************************************
** Created by Stefano Terzo (stefano.terzo@cern.ch)  on 04/10/16
**
** Institut de Física d'Altes Energies (IFAE)
** Edifici CN UAB Campus
** 08193 Bellaterra (Barcelona), Spain
**
****************************************************************************/

#ifndef H35CLIENT_INSTRUMENT_H
#define H35CLIENT_INSTRUMENT_H

#include <stdio.h>
#include <iostream>
#include <string>
#include <sstream>
#include <map>

#ifdef GPIB_VISA
    #include "VISA/visa.h"
#endif

#include "Socket.h"

//#define ERRMSGSIZE 1024 // Maximum size of SCPI command string
#define ARRAYSIZE 1024 // Size of read buffer
//#define BDINDEX 0 // Board Index of GPIB board
#define NO_SECONDARY_ADDR 0 // PNA has no Secondary address
#define TIMEOUT T1s // Timeout value = 1 seconds
#define EOTMODE 1 // Enable the END message
#define EOSMODE 0 // Disable the EOS mode

class Instrument : public Socket {

public:
    enum Status {
        noInterface = -1, disconnected = 0, connected = 1, readError = 33, writeError = 22
    };
    enum Type {
        Unknown = 0, VoltageSource = 1, PulseGenerator, ClimateChamber, Oscilloscope
    };

    Instrument(int interface, std::string address, int port) {
        interface_ = interface;
        address_ = address;
        port_ = port;
        type_ = Unknown;
        status_ = Status::disconnected;
    }

    Instrument(int interface){
        interface_ = interface;
        address_ = "";
        port_ = 0;
        type_ = Unknown;
        status_ = Status::disconnected;
    }

    virtual ~Instrument();


    int connect(std::string address, int port);
    int connect();

    std::string identify();
    bool operationComplete();

    bool reset();

    int getInterface() { return interface_; }

    enum Interface {
        TCP, GPIB
    };

    int write(std::string &cmd);

    int read(std::string &message, unsigned int buffer_len = 1000, unsigned int timeout_s = 2);

    int sendCmd(std::string &message);

    std::string getName() { return name_; }

    inline Type getType(std::string *type = NULL) {
        if (type != NULL) {
            if (type_ == Type::VoltageSource) *type = "VoltageSource";
            else if (type_ == Type::PulseGenerator) *type = "PulseGenerator";
            else if (type_ == Type::ClimateChamber) *type = "ClimateChamber";
            else if (type_ == Type::Oscilloscope) *type = "Oscilloscope";
            else *type = "UnknownType";
        }
        return type_;
    }

    inline int getStatus(std::string *statusMessage = NULL) {
        if (statusMessage != NULL) {
            if (status_ == Status::noInterface) *statusMessage = "noInterface";
            else if (status_ == Status::disconnected) *statusMessage = "disconnected";
            else if (status_ == Status::connected) *statusMessage = "connected";
            else if (status_ == Status::readError) *statusMessage = "readError";
            else if (status_ == Status::writeError) *statusMessage = "writeError";
            else *statusMessage = "unknown";
        }
        return status_;
    }

    bool isConnected(){return this->getStatus() == Status::connected;};

    //static std::map<std::string, std::pair<int,int> > searchDevices(Instrument::Interface interface);
    //std::map<std::string, int > searchDevices(Instrument::Interface interface);

protected:

private:

    std::string address_;
    int port_;

#ifdef GPIB_VISA
    ViSession defaultRM_;
    ViStatus vi_status_;
    ViSession vi_inst_;
    ViUInt32 vi_rcount_;
#endif

    bool connectGPIB(int primary_pna_address, int bdindex);

    int GPIBWrite(std::string &cmd);

    int GPIBRead(std::string &message, unsigned int buffer_len = 1024);

    int TCPWrite(std::string &cmd);

    int TCPRead(std::string &message, unsigned int buffer_len = 1000, unsigned int timeout_s = 2);

    int pna_;
    int interface_;

    std::string name_;
    int status_;
    Type type_;

    void detectInstrument(std::string id);
};

#endif //H35CLIENT_INSTRUMENT_H
