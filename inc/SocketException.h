/****************************************************************************
** Created by Stefano Terzo (stefano.terzo@cern.ch)
**
** Institut de Física d'Altes Energies (IFAE)
** Edifici CN UAB Campus
** 08193 Bellaterra (Barcelona), Spain
**
****************************************************************************/

#ifndef H35CLIENT_SOCKETEXCEPTION_H
#define H35CLIENT_SOCKETEXCEPTION_H

#include <string>

class SocketException
{
 public:
  SocketException ( std::string s ) : m_s ( s ) {};
  ~SocketException (){};

  std::string description() { return m_s; }

 private:

  std::string m_s;

};

#endif //H35CLIENT_SOCKETEXCEPTION_H
