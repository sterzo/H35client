/****************************************************************************
** Created by Stefano Terzo (stefano.terzo@cern.ch) on 24/08/16
**
** Institut de Física d'Altes Energies (IFAE)
** Edifici CN UAB Campus
** 08193 Bellaterra (Barcelona), Spain
**
****************************************************************************/

#ifndef H35CLIENT_CLIENTWIDGET_H
#define H35CLIENT_CLIENTWIDGET_H

#include <QWidget>

#include "Client.h"

class ClientWidget : public QWidget, public Client{
Q_OBJECT

public:
    void setNMOScode(nmoscode code) {
        Client::setNMOScode(code);
        emit codeChanged(code);
    }

    void setCMOScode(nmoscode code) {
        Client::setCMOScode(code);
        emit codeChanged(code);
    }

    void setANA1code(nmoscode code) {
        Client::setANA1code(code);
        emit codeChanged(code);
    }

    void setANA2code(nmoscode code) {
        Client::setANA2code(code);
        emit codeChanged(code);
    }

    void setCode(Matrix matrix, nmoscode code) {
        Client::setCode(matrix, code);
        emit codeChanged(code);
    }

    void setExternalDACs(std::vector<unsigned int> dacs) {
        Client::setExternalDACs(dacs);
        emit dacsChanged(dacs);
    }

signals:

    void codeChanged(nmoscode code);
    void dacsChanged(std::vector<unsigned int> dacs);

private:


};


#endif //H35CLIENT_CLIENTWIDGET_H
