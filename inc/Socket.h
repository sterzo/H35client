/****************************************************************************
** Created by Stefano Terzo (stefano.terzo@cern.ch)
**
** Institut de Física d'Altes Energies (IFAE)
** Edifici CN UAB Campus
** 08193 Bellaterra (Barcelona), Spain
**
****************************************************************************/

#ifndef SOCKET_H
#define SOCKET_H

#include <stdio.h>
#include <stdlib.h>
#include<iostream>
#include "string.h"

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <unistd.h>
#include <string>
#include <arpa/inet.h>
#include <bitset>
#include <fcntl.h>

#ifdef APPLE
    #define NO_SIGNAL_PIPE SO_NOSIGPIPE
#else
    #define NO_SIGNAL_PIPE MSG_NOSIGNAL
#endif

//#include "SocketException.h"

class Socket
{
public:
    Socket();
    virtual ~Socket();

    bool createSocket();
    void closeSocket();
    bool connectTCP(const std::string address, const int port);
    bool sendMessage(std::string message, int buffer) const;
    //bool sendMessage(const char byte) const;
    //bool sendMessage(nmoscode code) const;
    template<typename T>
    inline bool sendMessage(T msg) const{
        int status = ::send(sockfd_, &msg, sizeof(T), NO_SIGNAL_PIPE);
        return status != -1;
    }

    int  recv( std::string& message, unsigned int buffer_len = 1000) const;
    int  recv(std::string &message, unsigned int buffer_len, unsigned int timeout_sec) const;

    //int  readMessage(unsigned char& byte) const;
    template<typename T>
    inline int readMessage(T &msg) const {
        //T buf;

        int status = ::recv (sockfd_ , &msg, sizeof(T), 0 );

        if ( status == -1 )
        {
            std::cout << "status == -1   errno == " << errno << "  in Socket::recv\n";
            return 0;
        }
        else if ( status == 0 )
        {
            return 0;
        }
        else
        {
            //msg = buf;
            return status;
        }
    }

    inline int readMessage(int32_t *msg, size_t buffer) const {

        int status = ::recv (sockfd_ , msg, buffer, 0 );

        if ( status == -1 )
        {
            std::cout << "status == -1   errno == " << errno << "  in Socket::recv\n";
            return 0;
        }
        else if ( status == 0 )
        {
            return 0;
        }
        else
        {
            return status;
        }
    }

    bool bind ( const int port );

    bool is_valid() const { return sockfd_ != -1; }
    bool is_connected() const {return connected_;}

    bool setSocketBlocking(bool blocking = true);

    int find_port(std::string address,int min = 2000,int max = 65535);

    void connectSocket2(std::string address, int port);

private:
    int sockfd_;
    bool connected_,created_;
    struct sockaddr_in serv_addr_;
};

#endif // CLIENT_H
