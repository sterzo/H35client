/****************************************************************************
** Created by Stefano Terzo (stefano.terzo@cern.ch)  on 07/10/16
**
** Institut de Física d'Altes Energies (IFAE)
** Edifici CN UAB Campus
** 08193 Bellaterra (Barcelona), Spain
**
****************************************************************************/

#ifndef H35CLIENT_VOLTAGESOURCE_H
#define H35CLIENT_VOLTAGESOURCE_H

#include "Instrument.h"

class VoltageSource : public Instrument {
public:
    VoltageSource(int interface, std::string address, int port) : Instrument(interface,address,port) { }

    int setCurrentLimit(double Ilim);

    int setVoltage(double V);

    void setOutput(bool on = true);

    double readCurrent();

    double readVoltage();

    int setCurrentRange(double range=0.01, int resolution=6);

private:

};


#endif //H35CLIENT_VOLTAGESOURCE_H
