/****************************************************************************
** Created by Stefano Terzo (stefano.terzo@cern.ch)  on 04/10/16
**
** Institut de Física d'Altes Energies (IFAE)
** Edifici CN UAB Campus
** 08193 Bellaterra (Barcelona), Spain
**
****************************************************************************/

#ifndef H35CLIENT_CLIMATECHAMBER_H
#define H35CLIENT_CLIMATECHAMBER_H

#include "Instrument.h"

#include <vector>

class ClimateChamber : public Instrument {

public:
    ClimateChamber(int interface, std::string address, int port) : Instrument(interface,address,port) { }

    std::vector<std::string> readStatus();

    bool setTemperature(double temp);
};

#endif //H35CLIENT_CLIMATECHAMBER_H
