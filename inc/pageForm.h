/****************************************************************************
** Created by Stefano Terzo (stefano.terzo@cern.ch) on 14/07/16
**
** Institut de Física d'Altes Energies (IFAE)
** Edifici CN UAB Campus
** 08193 Bellaterra (Barcelona), Spain
**
****************************************************************************/

#ifndef H35CLIENT_PAGEFORM_H
#define H35CLIENT_PAGEFORM_H

#include <QWidget>
//#include "mainwindow.h"

#include "ClientWidget.h"

class PageForm : public QWidget
{
    Q_OBJECT

public:
    explicit PageForm(ClientWidget *client, QWidget *parent = 0);
    virtual ~PageForm(){}

public slots:

signals:

private slots:

private:
//    MainWindow *mw_ = (MainWindow*)parent;

};

#endif //H35CLIENT_PAGEFORM_H
