/****************************************************************************
** Created by Stefano Terzo (stefano.terzo@cern.ch) on 21/06/16
**
** Institut de Física d'Altes Energies (IFAE)
** Edifici CN UAB Campus
** 08193 Bellaterra (Barcelona), Spain
**
****************************************************************************/

#ifndef H35CLIENT_CLIENT_H
#define H35CLIENT_CLIENT_H

#define OPC_PROG_NMOS       0x80 //PROGRAM H35 NMOS CONFIGURATION REGISTER
#define OPC_PROG_CMOS       0x90 //PROGRAM H35 CMOS CONFIGURATION REGISTER
#define OPC_PROG_ANA1       0x91 //PROGRAM H35 ANALOG1 CONFIGURATION REGISTER
#define OPC_PROG_ANA2       0x92 //PROGRAM H35 ANALOG2 CONFIGURATION REGISTER
#define OPC_ACQ_NMOS        0xb0 //H35 NMOS ACQUISITION
#define OPC_ACQ_NMOS_DMA    0xb8 //H35 NMOS ACQUISITION DMA
#define OPC_ACQ_CMOS        0xb3 //H35 CMOS ACQUISITION
#define OPC_ACQ_CMOS_DMA    0xb9 //H35 CMOS ACQUISITION DMA
#define OPC_TLU_NMOS        0xb4 //H35 TLU NMOS ACQUISITION
#define OPC_TLU_NMOS_DMA    0xbc //H35 TLU NMOS ACQUISITION DMA
#define OPC_TLU_CMOS        0xb5 //H35 TLU CMOS ACQUISITION
#define OPC_TLU_CMOS_DMA    0xbd //H35 TLU CMOS ACQUISITION DMA
#define OPC_TLUS_NMOS       0xb6 //H35 TLUS NMOS ACQUISITION
#define OPC_TLUS_NMOS_DMA   0xba //H35 TLUS NMOS ACQUISITION DMA
#define OPC_TLUS_CMOS       0xb7 //H35 TLUS CMOS ACQUISITION
#define OPC_TLUS_CMOS_DMA   0xbb //H35 TLUS CMOS ACQUISITION DMA
#define OPC_PROG_DACS       0xa0 //PROGRAM DAC7568
#define OPC_EN_BPADS        0xb1 //H35 ENABLE BIAS PADS
#define OPC_DIS_BPADS       0xb2 //H35 DISABLE BIAS PADS

#define MAX_ROWS 16
#define MAX_COLS 300
#define MAX_ROCS 40
#define MAX_ROC_COLS 120
#define NMOSBITS 1472
#define PROGRAMBITS 1504

#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include <sstream>
#include <vector>
#include <map>

#include "Socket.h"
#include "SocketException.h"


class Client : public Socket {

public:

    enum Matrix {NMOS, analog1, analog2, CMOS};

    typedef unsigned char opcode;
    typedef std::bitset<NMOSBITS> nmoscode;
    typedef std::bitset<PROGRAMBITS> programcode;
    typedef std::bitset<32> wirebondcode;
    typedef std::bitset<32> DAC7568;

    typedef struct Opt {
        std::string name;
        std::pair<double,double> range;
        int value;
        void (*funk)(int,Client*);
    } Option;

    Client();

    virtual ~Client(){}

    unsigned int configure();

    bool connectSocket(std::string address, int port);

    bool decodeData(Client::DAC7568 data, int &col, int &row, int &timestamp, unsigned long int &fpgaTS);

    nmoscode getNMOScode() { return code_[NMOS]; }

    nmoscode getCMOScode() { return code_[CMOS]; }

    nmoscode getANA1code() { return code_[analog1]; }

    nmoscode getANA2code() { return code_[analog2]; }

    nmoscode getCode(Matrix n){return code_[n];}

    unsigned int getNmatrices(){return matrixNum_;}

    const std::vector<Option> getConfigOptions(){return configOptions_;}

    std::string getMatrixName(Matrix n){return matrixNames_.at(n);}

    std::vector<unsigned int> getExternalDACs() { return externalDACs_; }

    unsigned int initExtBias();

    bool loadConfig(std::string fileName);

    bool sendOpcode(Client::opcode &opcode);

    bool sendProgramCode(Client::nmoscode code);

    bool programMatrix(Client::Matrix matrix, Client::nmoscode code);

    void setNMOScode(nmoscode code);

    void setCMOScode(nmoscode code);

    void setANA1code(nmoscode code);

    void setANA2code(nmoscode code);

    void setCode(Matrix matrix, nmoscode code);

    void setExternalDACs(std::vector<unsigned int> dacsVector) { externalDACs_ = dacsVector; }

    void setLoadDAC(int row, bool enable, nmoscode &code);

    void setLoadDACCell(int roc, bool enable, nmoscode &code);

    void setAnaThCol(int col, short val, nmoscode &code);

    void setAnaThCol(int col, std::bitset<2> val, nmoscode &code);

    void setChipPower(bool on, nmoscode &code);

    void setDigThCol(int roc, short val, nmoscode &code);

    void setDigThCol(int roc, std::bitset<3> val, nmoscode &code);

    void setEnHitbus(int roc, bool enable, nmoscode &code);

    void setEnHitbusCol(int roc, bool enable, nmoscode &code);

    void setEnDigCol(int roc_col, bool val, nmoscode &code);

    void setEnInj(int col, int row, bool enable, nmoscode &code);

    void setEnInjCol(int col, bool enable, nmoscode &code);

    void setEnInjRow(int row, bool enable, nmoscode &code);

    void setEnMeasRow(int row, bool enable, nmoscode &code);

    bool setOption(unsigned int opt_n, int value);

    static void convertROCtoPixel(int &col, int &row);

    void setFullWirebond(int wirebondscheme){wirebond_ = wirebondcode(wirebondscheme-1);}

    void setPowerCycle(int doPowerCycle){doPowerCycle_ = (bool)doPowerCycle;}

    void setResetDACs(int reset){resetDACs_ = (bool)reset;}


private:
    static const unsigned int matrixNum_ = 4;

    nmoscode code_[matrixNum_];
    std::bitset<119> configCode_;
    std::bitset<1344> loadDACcode_;
    std::bitset<1344> loadDACcellCode_;
    std::bitset<1344> enableCode_;
    wirebondcode wirebond_;
    bool resetDACs_;
    bool doPowerCycle_;

    std::vector<std::bitset<6> > dac_;
    std::vector<std::bitset<1> > dacbit_;

    const std::map<Matrix,std::string> matrixNames_ = {
        {Matrix::NMOS,"NMOS"},
        {Matrix::analog1,"analog1"},
        {Matrix::analog2,"analog2"},
        {Matrix::CMOS,"CMOS"}
    };

    static void setFullWirebond(int wirebondscheme, Client* cl){cl->setFullWirebond(wirebondscheme);}

    static void setResetDACs(int reset, Client *cl) {cl->setResetDACs(reset);}

    static void setPowerCycle(int doPowerCycle, Client *cl) {cl->setPowerCycle(doPowerCycle);}

    void resetDACs();

    void powerCycle();

    std::vector<Option> configOptions_;

    std::vector<unsigned int> externalDACs_;

    static void convertROCtoPixel(int &col, int &row, int roc_matrix);

    nmoscode removeZeros(nmoscode b);
};

#endif //H35CLIENT_CLIENT_H
