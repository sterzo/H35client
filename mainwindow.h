/****************************************************************************
** Authors: Stefano Terzo (stefano.terzo@cern.ch)
**
** Institut de Física d'Altes Energies (IFAE)
** Edifici CN UAB Campus
** 08193 Bellaterra (Barcelona), Spain
**
****************************************************************************/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <stdio.h>
#include <iostream>
#include <map>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>

#include <QMainWindow>
#include <QStackedWidget>
#include <QSettings>
#include <QStringList>

#include "advancedform.h"
#include "maskform.h"
#include "initform.h"
#include "dacsform.h"
#include "scanform.h"
#include "ClientWidget.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:

    void on_actionAdvanced_triggered();

    void on_actionInit_triggered();

    void on_actionDACs_triggered();

    void on_actionOpen_TCP_Session_triggered();

    void on_actionClose_TCP_Session_triggered();

    void on_actionMasks_triggered();

    void on_actionScan_triggered();

    void on_actionInitialise_bias_triggered();

    void on_actionConfigure_chip_triggered();

    void on_actionConfig_all_triggered();

    void on_actionSave_conf_triggered();

    void on_actionLoad_Conf_triggered();

    void on_actionSave_Conf_As_triggered();

    void dacConfModified();

    void updateTcpStatus(bool connected);

    void on_actionTest_triggered();

    void on_actionReset_UI_triggered();

private:
    Ui::MainWindow *ui;

    ClientWidget *client_;

    bool connected_;

    enum Page {advanced, init, mask, dacs, scan};
    AdvancedForm *af_;
    InitForm *if_;
    MaskForm *mf_;
    DACsForm *df_;
    ScanForm *sf_;
    QStackedWidget *currentWindow_;

    QString cfgFileName_;

    void setCurrentCfgFile(QString fileName);

};

#endif // MAINWINDOW_H
