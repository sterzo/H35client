/****************************************************************************
** Authors: Stefano Terzo (stefano.terzo@cern.ch)
**
** Institut de Física d'Altes Energies (IFAE)
** Edifici CN UAB Campus
** 08193 Bellaterra (Barcelona), Spain
**
****************************************************************************/

#include "initform.h"
#include "ui_initform.h"

InitForm::InitForm(ClientWidget *client, QWidget *parent) :
    PageForm(client,parent),
    ui(new Ui::InitForm) {
    client_ = client;
    ui->setupUi(this);

    ui->device_comboBox->addItem("TCP");
    ui->device_comboBox->addItem("GPIB");
    //ui->device_comboBox->addItem("VXI");

    QSettings settings;//("ifaepix","H35client");
    ui->address_lineEdit->setText(settings.value("initform/address","172.16.15.21").toString());
    ui->port_spinBox->setValue(settings.value("initform/port",30200).toInt());
    ui->deviceAddress_lineEdit->setText(settings.value("initform/deviceAddress","172.16.15.253").toString());
    ui->devicePort_spinBox->setValue(settings.value("initform/devicePort",5025).toInt());
    ui->device_comboBox->setCurrentIndex(settings.value("initform/deviceInterface",0).toInt());
    savedDevices_ = settings.value("initform/savedDevices").toMap();

    ui->device_tableWidget->setColumnCount(4);
    ui->device_tableWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);
    ui->device_tableWidget->horizontalHeader()->setVisible(false);
    ui->device_tableWidget->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->device_tableWidget->setSelectionMode(QAbstractItemView::SingleSelection);
    ui->device_tableWidget->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

    ui->device_tableWidget->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(ui->device_tableWidget, SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(displayDeviceMenu(QPoint)));

    QMapIterator<QString,QVariant> it1(savedDevices_);
    while(it1.hasNext()){
        it1.next();
        std::cout << "Load instruments:" << "\n";
        std::cout << it1.key().toStdString() << " - " << it1.value().toString().toStdString() << "\n";

        this->addDevice(it1.key());
        QStringList list = it1.value().toString().split(',');
        for(unsigned int j=2; j<ui->device_tableWidget->columnCount(); ++j) {
            ui->device_tableWidget->setItem(0, j, new QTableWidgetItem(list.at(j-2)));
        }
    }

    ui->device_tableWidget->resizeColumnsToContents();

    ui->configMatrices_horizonatalLayout->setAlignment(Qt::AlignLeft);

    ui->configure_pushButton->setEnabled(false);
    ui->initBias_pushButton->setEnabled(false);
}

InitForm::~InitForm()
{
    abort_ = true;

    QSettings settings;//("ifaepix","H35client");
    settings.setValue("initform/address",ui->address_lineEdit->text());
    settings.setValue("initform/port",ui->port_spinBox->value());
    settings.setValue("initform/deviceAddress",ui->deviceAddress_lineEdit->text());
    settings.setValue("initform/devicePort",ui->devicePort_spinBox->value());
    settings.setValue("initform/deviceInterface",ui->device_comboBox->currentIndex());
    settings.setValue("initform/savedDevices",savedDevices_);

    delete ui;

    for(auto it : instruments_){
        delete it;
    }
}

void InitForm::displayDeviceMenu(const QPoint &pos)
{
    QMenu *menu = new QMenu(this);
    if(ui->device_tableWidget->itemAt(pos) != nullptr) {
        QAction *connect = menu->addAction("Connect");
        QAction *save = menu->addAction("Save");
        QAction *remove = menu->addAction("Remove");
        QAction *action = menu->exec(ui->device_tableWidget->viewport()->mapToGlobal(pos));
        if (action == remove) {
            savedDevices_.remove(ui->device_tableWidget->item(ui->device_tableWidget->itemAt(pos)->row(),0)->text());
            this->on_removeDevice_pushButton_clicked();
        }
        else if(action == connect){
            QString devId = ui->device_tableWidget->item(ui->device_tableWidget->itemAt(pos)->row(),0)->text();
            QStringList list = devId.split(':');
            this->connectDevice(ui->device_tableWidget->itemAt(pos)->row());
        }
        else if(action == save){
            QString devId = ui->device_tableWidget->item(ui->device_tableWidget->itemAt(pos)->row(),0)->text();
            QString devInfo = "";
            for(int i=2; i<ui->device_tableWidget->columnCount(); ++i) {
                QTableWidgetItem *item = ui->device_tableWidget->item(ui->device_tableWidget->itemAt(pos)->row(), i);
                if(item != nullptr){
                    devInfo += item->text();
                }
                if(i!=ui->device_tableWidget->columnCount()-1) devInfo+= ",";
            }
            savedDevices_.insert(devId,devInfo);
            //std::cout << "Saved: "<< devId.toStdString() << savedDevices_.value(devId).toString().toStdString() << std::endl;
            std::cout << "Saved instruments:" << "\n";
            QMapIterator<QString,QVariant> it(savedDevices_);
            int i =0;
            while(it.hasNext()){
                it.next();
                std::cout << i << ") " << it.key().toStdString() << " - " << it.value().toString().toStdString() << "\n";
                i++;
            }
        }
    }
    else{
        //QAction *connect = menu->addAction("Connect All");
        //QAction *remove = menu->addAction("Remove All");
        //QAction *save = menu->addAction("Save All");
    }
    delete menu;
}

void InitForm::on_configure_pushButton_clicked() {
    if(!ui->configure_pushButton->isEnabled()) return;

    std::cout << "configuring...\n";
    unsigned int output = client_->configure();

    for(unsigned int i=0; i<client_->getNmatrices(); ++i){
        if((output >> i) & 1) matrixResponse_checkBoxs_[i]->setChecked(true);
        else matrixResponse_checkBoxs_[i]->setCheckState(Qt::CheckState::PartiallyChecked);
    }

    std::cout << "configured\n";
}

void InitForm::on_initBias_pushButton_clicked() {
    std::cout << "initializing bias voltages...\n";

    std::bitset<8> channels = std::bitset<8>(client_->initExtBias());
    for(unsigned int addr=0; addr<channels.size(); addr++){
        QCheckBox *checkBox = (QCheckBox*)ui->initBias_horizontalLayout->itemAt(addr+1)->widget();
        if (channels[addr]) checkBox->setChecked(true);
        else checkBox->setCheckState(Qt::CheckState::PartiallyChecked);
    }
}

void InitForm::on_open_tcp_session_pushbutton_clicked(bool checked)
{
    if(checked) {
        std::cout << "not connected, connecting...\n";
        this->openTcpSession();
    }
    else{
        std::cout << "connected, disconnecting...\n";
        this->closeTcpSession();
    }
}

bool InitForm::openTcpSession(){
    bool created = client_->createSocket();
    bool connected = false;
    if (created) {
        std::string address = ui->address_lineEdit->text().toStdString();
        int port = ui->port_spinBox->value();
        connected = client_->connectSocket(address.c_str(), port);
        ui->address_lineEdit->setDisabled(connected);
        ui->port_spinBox->setDisabled(connected);
        if (connected) {
            ui->open_tcp_session_pushbutton->setText("Close TCP Session");

            for(unsigned int i=0; i<client_->getNmatrices(); ++i){
                matrixResponse_checkBoxs_.push_back(new QCheckBox(this));
                ui->configMatrices_horizonatalLayout->addWidget(matrixResponse_checkBoxs_.back());
                matrixResponse_checkBoxs_.back()->setToolTip(QString::fromStdString(client_->getMatrixName((Client::Matrix)i)));
            }

            confOptList_ = new QWidget(this);
            QGridLayout *confOptList_gridLayout = new QGridLayout(confOptList_);
            QSpinBox *spinBox;
            const std::vector<Client::Option> options = client_->getConfigOptions();
            for (int i = 0; i < options.size(); i++) {
                QLabel *label = new QLabel(QString::fromStdString(options[i].name), confOptList_);
                confOptList_gridLayout->addWidget(label, i, 0, Qt::AlignLeft);
                spinBox = new QSpinBox(confOptList_);
                //SpinBox->setButtonSymbols(QAbstractSpinBox::NoButtons);
                //spinBox->setFixedSize(50, 24);
                spinBox->setMinimum((int)options[i].range.first);
                spinBox->setMaximum((int)options[i].range.second);
                confOptList_gridLayout->addWidget(spinBox, i, 1, Qt::AlignLeft);
                QObject::connect(spinBox, static_cast<void (QSpinBox::*)(int)>(&QSpinBox::valueChanged),
                                 [=](int value) {client_->setOption(i,value);});
                spinBox->setValue(options[i].value);
            }
            confOptList_gridLayout->setAlignment(Qt::AlignTop);
            ui->configOpt_scrollArea->setWidget(confOptList_);
        }
        else {
            std::cout << "Failed to connect to: " << address << " - port: " << port << std::endl;
            client_->closeSocket();
        }
    }
    else std::cout << "Failed to create socket\n";

    ui->open_tcp_session_pushbutton->setChecked(connected);
    ui->configure_pushButton->setEnabled(connected);
    ui->initBias_pushButton->setEnabled(connected);
    emit activeTcpSession(connected);

    return connected;
}

void InitForm::closeTcpSession(){
    std::string str;
    client_->sendMessage('Q');
    client_->recv(str);
    std::cout << str << "\n";
    client_->closeSocket();
    ui->address_lineEdit->setEnabled(true);
    ui->port_spinBox->setEnabled(true);
    ui->open_tcp_session_pushbutton->setText("Open TCP Session");
    ui->open_tcp_session_pushbutton->setChecked(false);
    emit activeTcpSession(false);

    for(auto it : matrixResponse_checkBoxs_){
        if(it!= nullptr) delete it;
    }
    matrixResponse_checkBoxs_.clear();

    if(confOptList_!= nullptr) delete confOptList_;
}
void InitForm::on_addDevice_pushButton_clicked()
{
    if(this->addDevice((Instrument::Interface)ui->device_comboBox->currentIndex(),ui->deviceAddress_lineEdit->text().toStdString(),ui->devicePort_spinBox->value()))
        this->connectDevice(0);
}

bool InitForm::addDevice(Instrument::Interface interface, std::string address, int port){
    QString devId = ui->device_comboBox->itemText(interface) + ":" + QString::fromStdString(address) + ":" + QString::number(port);

    if(ui->device_tableWidget->findItems(devId,Qt::MatchFlag::MatchExactly).size()!=0){
        std::cout << "WARNING: " << devId.toStdString() << " is already in the device list.\n";
        return false;
    }

    std::cout << devId.toStdString() << std::endl;

    ui->device_tableWidget->insertRow(0);
    ui->device_tableWidget->setItem(0,0,new QTableWidgetItem(devId));

    instruments_.push_back(new Instrument(interface,address,port));

//    std::cout << "Added instruments:" << "\n";
//    for(int i = 0; i<instruments_.size(); ++i){
//        std::cout << i << ") " << ui->device_tableWidget->item(i,0)->text().toStdString() << "\n";
//    }

    ui->device_tableWidget->resizeColumnsToContents();

    return true;
}

bool InitForm::addDevice(QString devId){
    QStringList list = devId.split(':');
    for(int i=0; i<ui->device_comboBox->count(); ++i) {
        if(list.at(0) == ui->device_comboBox->itemText(i)) {
            std::cout << list.at(0).toStdString() << " - " << list.at(1).toStdString() << " - " << list.at(2).toStdString() << std::endl;
            return this->addDevice((Instrument::Interface) i, list.at(1).toStdString(), list.at(2).toInt());
        }
    }
    return false;
}

int InitForm::connectDevice(int instr){
    std::cout << "Connecting device: " << instr << "\n";

    std::vector<Instrument*>::reverse_iterator rit = instruments_.rbegin() + instr;
    int status = (*rit)->connect();

    if(status==Instrument::Status::connected) {
        std::string type;
        (*rit)->getType(&type);
        std::vector<QString> text = {"connected",QString::fromStdString(type),QString::fromStdString((*rit)->getName())};
        for(unsigned int i=1; i<4; ++i) {
            QTableWidgetItem *item = ui->device_tableWidget->item(instr, i);
            if (item != nullptr) {
                item->setText(text[i-1]);
            }
            else ui->device_tableWidget->setItem(instr, i, new QTableWidgetItem(text[i-1]));
        }
    }
    else{
        std::string statusMessage;
        (*rit)->getStatus(&statusMessage);
        QTableWidgetItem *item = ui->device_tableWidget->item(instr, 1);
        if(item != nullptr){
            item->setText(QString::fromStdString(statusMessage));
        }
        else ui->device_tableWidget->setItem(instr,1,new QTableWidgetItem(QString::fromStdString(statusMessage)));
    }
    ui->device_tableWidget->resizeColumnsToContents();

//    std::cout << "Connected instruments:" << "\n";
//    for(int i=0; i<instruments_.size(); ++i){
//        std::cout << i << ") " << instruments_[i]->getName() << "\n";
//    }

    emit updateInstruments(&instruments_);

    return status;
}

void InitForm::on_removeDevice_pushButton_clicked()
{
    int element = ui->device_tableWidget->currentRow();
    if(element > -1) {
        ui->device_tableWidget->removeRow(element);
        std::vector<Instrument*>::iterator it = instruments_.end() - element - 1;
        delete (*it);
        instruments_.erase(it);
        ui->device_tableWidget->resizeColumnsToContents();
        emit updateInstruments(&instruments_);
    }
}

void InitForm::on_loadConf_pushButton_clicked()
{
    emit loadConf();
}

void InitForm::on_newConf_pushButton_clicked()
{
    emit saveConfAs();
}

void InitForm::resetCheckboxes(){
    for(unsigned int i=0; i<8; i++){
        QCheckBox *checkBox = (QCheckBox*)ui->initBias_horizontalLayout->itemAt(i+1)->widget();
        checkBox->setChecked(false);
    }
    for(unsigned int i=0; i<matrixResponse_checkBoxs_.size(); i++){
        matrixResponse_checkBoxs_[i]->setChecked(false);
    }
}


void InitForm::on_searchPort_pushButton_clicked() {

    abort_ = false;
    if(!futureWatcher_.isRunning()) {
        ui->searchPort_pushButton->setText("Abort");

        futureWatcher_.setFuture(QtConcurrent::run(this, &InitForm::searchPort));
    }
    else {
        std::cout << "abort search...\n";
        abort_ = true;
    }
}

void InitForm::searchPort() {
    std::cout << "Searching port: " << ui->devicePort_spinBox->value() << "\n";

    Instrument *inst = new Instrument(ui->device_comboBox->currentIndex());

    int port = inst->find_port(ui->deviceAddress_lineEdit->text().toStdString(), ui->devicePort_spinBox->value());

    if (port > 0) {
        std::cout << "\t-->Found port: " << port << std::endl;
        delete inst;

        this->addDevice((Instrument::Interface)ui->device_comboBox->currentIndex(),ui->deviceAddress_lineEdit->text().toStdString(),port);
        if (!abort_){
            ui->devicePort_spinBox->setValue(port+1);
            this->searchPort();
        }
    }
    else delete inst;

    ui->searchPort_pushButton->setText("Search");
}

//void InitForm::searchPort() {
////    std::cout << "Searching port: " << ui->devicePort_spinBox->value() << "\n";
//
//    Instrument *inst = new Instrument(ui->device_comboBox->currentIndex());
//    std::map<std::string, int > addrMap =  inst->searchDevices((Instrument::Interface) ui->device_comboBox->currentIndex());
//    delete inst;
//    for(auto it : addrMap){
//        std::cout << "Address: " << it.first << " - Ports: TPC) " << it.second <<  std::endl;
//    }
//
//    ui->searchPort_pushButton->setText("Search");
//}