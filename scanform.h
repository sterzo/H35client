/****************************************************************************
** Authors: Stefano Terzo (stefano.terzo@cern.ch)
**
** Institut de Física d'Altes Energies (IFAE)
** Edifici CN UAB Campus
** 08193 Bellaterra (Barcelona), Spain
**
****************************************************************************/

#ifndef SCANFORM_H
#define SCANFORM_H

#include <QString>

#include <sstream>
#include <bitset>
#include <QLineEdit>
#include <QFile>
#include <QFileInfo>
#include <QString>
#include <QDate>
#include <QtConcurrent/QtConcurrent>
#include <QTimer>
#include <iomanip>
#include <cmath>
#include <unistd.h>

#include <sys/stat.h>
#include <QStackedWidget>
#include <QDoubleSpinBox>

#include "pageForm.h"
#include "Instrument.h"
#include "PulseGenerator.h"
#include "VoltageSource.h"
#include "ClimateChamber.h"
#include "pixelmap.h"

namespace Ui {
class ScanForm;
}

class ScanForm : public PageForm
{
    Q_OBJECT

public:
    enum DataFormat{
        ASCII=0, RAW
    };

    explicit ScanForm(ClientWidget *client, QWidget *parent = 0);
    ~ScanForm();

    bool setupPulser();
    bool setupChamber();
    bool setupVoltage();

    bool readPulser();
    bool readChamber();
    bool readVoltage();

    void setCmosPixMaps(PixelMap *analogCmosMap, PixelMap *digitalCmosMap){analogCmosMap_ = analogCmosMap; digitalCmosMap_=digitalCmosMap;}
    void setNmosPixMaps(PixelMap *analogNmosMap, PixelMap *digitalNmosMap){analogNmosMap_ = analogNmosMap; digitalNmosMap_=digitalNmosMap;}

signals:
    void scanCanceled();

public slots:

    void refreshInstruments(std::vector<Instrument*> *instruments);

private slots:
    void on_startScan_pushButton_clicked();

    void on_stopScan_pushButton_clicked();

    void updateOutfileName(QString scanName);

    void on_selectScan_comboBox_currentIndexChanged(int index);

    void on_setupInstruments_pushButton_clicked();

    void scanFinished();

    void updateProgressBar();

    void on_readInstruments_pushButton_clicked();

private:

    Ui::ScanForm *ui;

    ClientWidget *client_;

    bool stopScan_,timeout_;

    int events_,triggers_,injections_,*counter_,headerIndex_;

    double currentAmp_;

    Client::opcode opcode_;

    bool acquireData();

    void startDAQ(bool dma = false);

    void startTrgDAQ();

    bool readData(std::fstream &outfile, int outputFreq = 1000);

    void setDefaultEnabled(bool maxEvents, bool injections, bool timeout,bool clocks,bool buffer, bool colRowInterval, bool voltageMinMax, bool steps, bool eventBlock, bool deadTime, bool delay, bool threshold );

    void analogScan();

    void trgResponceScan();

    void pixelLoop(void (ScanForm::*fn)());
    void injLoop(void (ScanForm::*fn)(void (ScanForm::*)()), void (ScanForm::*arg)());
    void injLoop(void (ScanForm::*fn)());
    void temperatureLoop(void (ScanForm::*fn)(void (ScanForm::*)()), void (ScanForm::*arg)());
    void temperatureLoop(void (ScanForm::*fn)());

    enum Scan {analog,source,threshold,gpix,gdac,tpix,tpix_fit,tdac,tdac_fit,trigger,triggerSimple,delay,test,triggerResponse,noise,xray,thrLoop};

    bool startPulses();

    PulseGenerator *pulseGenerator_;
    VoltageSource *voltageSource_;
    ClimateChamber *climateChamber_;

    std::vector<QDoubleSpinBox*> pulseGeneratorPars_,climateChamberPars_,voltageSourcePars_;

    QStackedWidget *instrument_stackedWidget_;
    std::vector<QWidget*> instrumentWidgetList_;
    std::vector<Instrument*> instrumentList_;

    bool setAmplitude(float volts);
    bool setVoltage(double volts, bool checkLim=true);

    void stopDAQ();

    void writeHeader(int test = 0);

    unsigned int pixelMap_[300][16];
    PixelMap *analogCmosMap_,*analogNmosMap_,*digitalCmosMap_,*digitalNmosMap_;

    QFutureWatcher<void> futureWatcher_;

    void sourceScan();

    void xrayScan();

    void thrLoopScan();

    void triggerScan();

    void delayScan();

    void gdacPixelScan();

    void tdacPixelScan();

    void gdacScan();

    void tdacFitScan();

    void tpixFitScan();

    void tdacScan();

    void testScan();

    bool openOutfile(std::fstream &outfile, ScanForm::DataFormat dataFormart = ScanForm::DataFormat::ASCII);

    QTimer *timer_;
};

#endif // SCANFORM_H
