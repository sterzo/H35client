#!/bin/bash
rm build/ui_*.h
uic -o build/ui_dacsform.h dacsform.ui 
uic -o build/ui_advancedform.h advancedform.ui 
uic -o build/ui_initform.h initform.ui 
uic -o build/ui_mainwindow.h mainwindow.ui 
uic -o build/ui_maskform.h maskform.ui 
uic -o build/ui_scanform.h scanform.ui 
uic -o build/ui_pixelmap.h pixelmap.ui 
