/****************************************************************************
** Authors: Stefano Terzo (stefano.terzo@cern.ch)
**
** Institut de Física d'Altes Energies (IFAE)
** Edifici CN UAB Campus
** 08193 Bellaterra (Barcelona), Spain
**
****************************************************************************/

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QtConcurrent/QtConcurrentRun>
#include <QTimer>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow) {
    client_ = new ClientWidget();

    connected_ = false;

    cfgFileName_ = "";

    ui->setupUi(this);

    QCoreApplication::setOrganizationName("ifaepix");
    QCoreApplication::setApplicationName("H35client");

    QSettings settings;
    settings.setFallbacksEnabled(false);
    QStringList settingList = settings.allKeys();

    for (int i = 0; i < settingList.size(); ++i)
        std::cout << settingList.at(i).toLocal8Bit().constData() << std::endl;

    this->resize(settings.value("mainwindow/size",QSize(1000,610)).toSize());

//    ui->menuBar->setNativeMenuBar(false);
    mf_ = new MaskForm(client_, this);
    //connect(client_, &ClientWidget::codeChanged, mf_, &MaskForm::updateMasks);

    if_ = new InitForm(client_, this);

    af_ = new AdvancedForm(client_, this);

    df_ = new DACsForm(client_, this);

    sf_ = new ScanForm(client_, this);
    sf_->setCmosPixMaps(df_->getAnalogPixMaps()[3],df_->getDigitalPixMaps()[3]);
    sf_->setNmosPixMaps(df_->getAnalogPixMaps()[3],df_->getDigitalPixMaps()[3]);

    connect(client_, &ClientWidget::dacsChanged, df_, &DACsForm::updateDACs);

    currentWindow_ = new QStackedWidget(this);
    currentWindow_->addWidget(af_);
    currentWindow_->addWidget(if_);
    currentWindow_->addWidget(mf_);
    currentWindow_->addWidget(df_);
    currentWindow_->addWidget(sf_);
    currentWindow_->setCurrentIndex(Page::init);
    ui->scrollArea->setWidget(currentWindow_);
    this->setCentralWidget(ui->scrollArea);

    QActionGroup *selectWindowGroup = new QActionGroup(this);
    selectWindowGroup->addAction(ui->actionAdvanced);
    selectWindowGroup->addAction(ui->actionInit);
    selectWindowGroup->addAction(ui->actionMasks);
    selectWindowGroup->addAction(ui->actionDACs);
    selectWindowGroup->addAction(ui->actionScan);
    ui->actionInit->setChecked(true);

    //connect(selectWindowGroup,SIGNAL(triggered(QAction* action)),currentWindow_,SLOT(setCurrentIndex(action));
    this->setWindowTitle("H35Client");
    this->setWindowModified(true);
    connect(df_, &DACsForm::dacChanged, this, &MainWindow::dacConfModified);
    connect(df_, &DACsForm::dacChanged, if_, &InitForm::resetCheckboxes);
    connect(if_, SIGNAL(activeTcpSession(bool)), this, SLOT(updateTcpStatus(bool)));
    connect(if_, &InitForm::updateInstruments, sf_, &ScanForm::refreshInstruments);
    connect(if_, &InitForm::loadConf, this, &MainWindow::on_actionLoad_Conf_triggered);
    connect(if_, &InitForm::saveConfAs, this, &MainWindow::on_actionSave_Conf_As_triggered);
}

MainWindow::~MainWindow()
{
    if(connected_) this->on_actionClose_TCP_Session_triggered();
    QSettings settings;
    settings.setValue("mainwindow/size", this->size());

    delete client_;
    delete ui;
}

void MainWindow::updateTcpStatus(bool connected){
    connected_ = connected;
    ui->actionClose_TCP_Session->setEnabled(connected);
    ui->actionOpen_TCP_Session->setDisabled(connected);
}

void MainWindow::on_actionOpen_TCP_Session_triggered()
{
    bool connected = if_->openTcpSession();
}

void MainWindow::on_actionClose_TCP_Session_triggered()
{
    if_->closeTcpSession();
}

void MainWindow::on_actionAdvanced_triggered()
{
    currentWindow_->setCurrentIndex(Page::advanced);
}

void MainWindow::on_actionInit_triggered()
{
    currentWindow_->setCurrentIndex(Page::init);
}

void MainWindow::on_actionMasks_triggered()
{
    currentWindow_->setCurrentIndex(Page::mask);
}

void MainWindow::on_actionDACs_triggered()
{
    currentWindow_->setCurrentIndex(Page::dacs);
}

void MainWindow::on_actionScan_triggered()
{
     currentWindow_->setCurrentIndex(Page::scan);
}


void MainWindow::on_actionInitialise_bias_triggered()
{
    if_->on_initBias_pushButton_clicked();
}

void MainWindow::on_actionConfigure_chip_triggered()
{
    if_->on_configure_pushButton_clicked();
}

void MainWindow::on_actionConfig_all_triggered()
{
    if_->on_initBias_pushButton_clicked();
    if_->on_configure_pushButton_clicked();
}

void MainWindow::on_actionSave_conf_triggered() {
    if(cfgFileName_ == "") this->on_actionSave_Conf_As_triggered();
    else {
        bool success = df_->save(cfgFileName_);
        if (success) this->setWindowModified(false);
    }
}

void MainWindow::on_actionLoad_Conf_triggered()
{
    QString fileName = QFileDialog::getOpenFileName(this, tr("Load configuration file"), cfgFileName_, tr("Config Files (*.cfg)"));
    bool success = df_->load(fileName);
    if(success) this->setCurrentCfgFile(fileName);
}

void MainWindow::on_actionSave_Conf_As_triggered()
{
    QString fileName = QFileDialog::getSaveFileName(this, tr("Save configuration file"), cfgFileName_, tr("Config Files (*.cfg)"));
    bool success = df_->save(fileName);
    if(success) this->setCurrentCfgFile(fileName);
}

void MainWindow::setCurrentCfgFile(QString fileName)
{
        cfgFileName_ = fileName;
        this->setWindowTitle("H35Client: " + cfgFileName_);
        this->setWindowModified(false);
}

void MainWindow::dacConfModified(){
    this->setWindowModified(true);
}

void MainWindow::on_actionTest_triggered()
{
    //sf_->setupPulser();
    Client::nmoscode code = client_->getNMOScode();
    std::cout << code.to_string() << "\n\n";

    client_->setDigThCol(119,15,code);
    //client_->setAnaThCol(0,3,code);
    std::cout << code.to_string() << "\n\n";
}


void MainWindow::on_actionReset_UI_triggered()
{
    QSettings settings;
    settings.clear();
}
