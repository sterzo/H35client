/****************************************************************************
** Authors: Stefano Terzo (stefano.terzo@cern.ch)
**          Emanuele Cavallaro (ecavallaro@ifae.es)
**
** Institut de Física d'Altes Energies (IFAE)
** Edifici CN UAB Campus
** 08193 Bellaterra (Barcelona), Spain
**
****************************************************************************/

#include "dacsform.h"
#include "ui_dacsform.h"

DACsForm::DACsForm(ClientWidget *client, QWidget *parent) :
    PageForm(client,parent),
    ui(new Ui::DACsForm) {
    ui->setupUi(this);

    ui->selectMask_comboBox->addItem("CMOS Analog");
    ui->selectMask_comboBox->addItem("CMOS Digital");
    ui->selectMask_comboBox->addItem("NMOS Analog");
    ui->selectMask_comboBox->addItem("NMOS Digital");


    client_ = client;

    currentMatrixDacs_ = new QStackedWidget(this);
    //currentMatrixDacs_ = new QTabWidget(this);
    for(unsigned int i=0; i < client_->getNmatrices(); i++) {
        daclists_.push_back(new DacList(defaultInternalDacs_,this));
        currentMatrixDacs_->addWidget(daclists_.back());
        //currentMatrixDacs_->addTab(daclists_.back(),"Matrix" + QString::number(i));
        ui->dacsForm_comboBox->addItem(QString::fromStdString(client_->getMatrixName((Client::Matrix)i)));
        connect(daclists_.back(), SIGNAL(valueChanged()), this, SLOT(updateDACconf()));
    }
    ui->dacsForm_scrollArea->setWidget((QWidget*)currentMatrixDacs_);
    connect(ui->matrix_spinBox,SIGNAL(valueChanged(int)),ui->dacsForm_comboBox,SLOT(setCurrentIndex(int)));
    connect(ui->dacsForm_comboBox,SIGNAL(currentIndexChanged(int)),currentMatrixDacs_,SLOT(setCurrentIndex(int)));
    connect(ui->dacsForm_comboBox,SIGNAL(currentIndexChanged(int)),ui->matrix_spinBox,SLOT(setValue(int)));
    ui->matrix_spinBox->setValue(Client::Matrix::CMOS);

    QWidget *extDaclist = new QWidget(this);
    QGridLayout *extDaclist_gridLayout = new QGridLayout(extDaclist);
    std::vector<QString> labels = {"nVPlusTwPix", "V_A", "nThTwPix", "V_B", "nVGatePix", "V_C", "nThPix", "V_D", "nBLPix", "V_E", "nTh", "V_F", "nBL", "V_G", "VGATE", "V_H"};

    QSpinBox *spinBox;
    QDoubleSpinBox *doubleSpinBox;
    for (int i = 0; i < labels.size(); i++) {
        QLabel *label = new QLabel(labels[i], extDaclist);
        extDaclist_gridLayout->addWidget(label, i, 0, Qt::AlignLeft);
        if (i % 2 == 0) {
            spinBox = new QSpinBox(this);
            spinBox->setButtonSymbols(QAbstractSpinBox::NoButtons);
            spinBox->setMaximum(4095);
            spinBox->setFixedSize(50, 24);
            externalDacSpinBoxes_.push_back(spinBox);
            extDaclist_gridLayout->addWidget(spinBox, i, 1, Qt::AlignLeft);
        }
        else {
            doubleSpinBox = new QDoubleSpinBox(extDaclist);
            doubleSpinBox->setButtonSymbols(QAbstractSpinBox::NoButtons);
            doubleSpinBox->setSuffix(" V");
            doubleSpinBox->setDecimals(2);
            doubleSpinBox->setFixedSize(50, 24);
            doubleSpinBox->setMaximum(5.00);
            extDaclist_gridLayout->addWidget(doubleSpinBox, i, 1, Qt::AlignLeft);
            QObject::connect(spinBox, static_cast<void (QSpinBox::*)(int)>(&QSpinBox::valueChanged),
                             [=](int value) { doubleSpinBox->setValue(value * 5. / 4095.); });
//QObject::connect(doubleSpinBox, static_cast<void (QDoubleSpinBox::*)(double)>(&QDoubleSpinBox::valueChanged), [=](double value){spinBox->setValue( value * 4095 / 5);});
            QObject::connect(doubleSpinBox, &QDoubleSpinBox::editingFinished,
                             [=](void) { spinBox->setValue(doubleSpinBox->value() * 4095 / 5); });
            spinBox->setValue(defaultExternalDacs_[i / 2]);
            connect(spinBox, SIGNAL(valueChanged(int)), this, SLOT(updateDACconf()));
        }
    }
    ui->extDacsForm_scrollArea->setWidget(extDaclist);

    QWidget *analogTDACmaskMatrix = new QWidget(this);
    QGridLayout *analogTDACmaskMatrix_gridLayout = new QGridLayout(analogTDACmaskMatrix);
    //H35 Matrix =============================================
    analogPixMaps_.push_back(new PixelMap(Client::Matrix::NMOS,MAX_COLS, MAX_ROWS, 2, 10, 3, analogTDACmaskMatrix));
    analogPixMaps_.back()->setAllPixels(true);
    analogPixMaps_.back()->updateMap();
    analogTDACmaskMatrix_gridLayout->addWidget(analogPixMaps_.back(),0,0);
    analogPixMaps_.push_back(new PixelMap(Client::Matrix::analog1,300, 23, 2, 10, 3, analogTDACmaskMatrix));
    analogTDACmaskMatrix_gridLayout->addWidget(analogPixMaps_.back(),1,0);
    analogPixMaps_.push_back(new PixelMap(Client::Matrix::analog2,300, 23, 2, 10, 3, analogTDACmaskMatrix));
    analogTDACmaskMatrix_gridLayout->addWidget(analogPixMaps_.back(),2,0);
    analogPixMaps_.push_back(new PixelMap(Client::Matrix::CMOS,MAX_COLS, MAX_ROWS, 2, 10, 3, analogTDACmaskMatrix));
    analogPixMaps_.back()->setAllPixels(true);
    analogPixMaps_.back()->updateMap();
    analogTDACmaskMatrix_gridLayout->addWidget(analogPixMaps_.back(),3,0);
    analogTDACmaskMatrix->setLayout(analogTDACmaskMatrix_gridLayout);
    ui->AnalogTDAC_scrollArea->setWidget(analogTDACmaskMatrix);
    //=========================================================
    analogTDACmaskMatrix_gridLayout->setHorizontalSpacing(-1);
    analogTDACmaskMatrix_gridLayout->setVerticalSpacing(-1);
    ui->matrix_spinBox->setMaximum(analogPixMaps_.size()-1);

    connect(ui->row_spinBox,SIGNAL(valueChanged(int)),this,SLOT(updatePixelValue()));
    connect(ui->col_spinBox,SIGNAL(valueChanged(int)),this,SLOT(updatePixelValue()));
    connect(ui->matrix_spinBox,SIGNAL(valueChanged(int)),this,SLOT(updatePixelValue()));
    for(int m=0; m<analogPixMaps_.size(); ++m)
    {
        connect(analogPixMaps_[m],&PixelMap::pixelClicked,this,&DACsForm::mapClick);
    }

    QWidget *digitalTDACmaskMatrix = new QWidget(this);
    QGridLayout *digitalTDACmaskMatrix_gridLayout = new QGridLayout(digitalTDACmaskMatrix);
    //H35 Matrix =============================================
    digitalPixMaps_.push_back(new PixelMap(Client::Matrix::NMOS,MAX_COLS, MAX_ROWS, 2, 10, 7, digitalTDACmaskMatrix));
    digitalPixMaps_.back()->setAllPixels(true);
    digitalPixMaps_.back()->updateMap();
    digitalTDACmaskMatrix_gridLayout->addWidget(digitalPixMaps_.back(),0,0);
    digitalPixMaps_.push_back(new PixelMap(Client::Matrix::analog1,300, 23, 2, 10, 7, digitalTDACmaskMatrix));
    digitalTDACmaskMatrix_gridLayout->addWidget(digitalPixMaps_.back(),1,0);
    digitalPixMaps_.push_back(new PixelMap(Client::Matrix::analog2,300, 23, 2, 10, 7, digitalTDACmaskMatrix));
    digitalTDACmaskMatrix_gridLayout->addWidget(digitalPixMaps_.back(),2,0);
    digitalPixMaps_.push_back(new PixelMap(Client::Matrix::CMOS,MAX_COLS, MAX_ROWS, 2, 10, 7, digitalTDACmaskMatrix));
    digitalPixMaps_.back()->setAllPixels(true);
    digitalPixMaps_.back()->updateMap();
    digitalTDACmaskMatrix_gridLayout->addWidget(digitalPixMaps_.back(),3,0);
    digitalTDACmaskMatrix->setLayout(digitalTDACmaskMatrix_gridLayout);
    ui->DigitalTDAC_scrollArea->setWidget(digitalTDACmaskMatrix);
    //=========================================================
    digitalTDACmaskMatrix_gridLayout->setHorizontalSpacing(-1);
    digitalTDACmaskMatrix_gridLayout->setVerticalSpacing(-1);

    connect(ui->row_spinBox,SIGNAL(valueChanged(int)),this,SLOT(updatePixelValue()));
    connect(ui->col_spinBox,SIGNAL(valueChanged(int)),this,SLOT(updatePixelValue()));
    connect(ui->matrix_spinBox,SIGNAL(valueChanged(int)),this,SLOT(updatePixelValue()));
    for(int m=0; m<digitalPixMaps_.size(); ++m)
    {
        connect(digitalPixMaps_[m],&PixelMap::pixelClicked,this,&DACsForm::mapClick);
    }

    connect(ui->dacForm_tabWidget,&QTabWidget::currentChanged,this,&DACsForm::updatePixelValue);

    this->updateDACconf();
}

DACsForm::~DACsForm()
{
    delete ui;
}

void DACsForm::updateDACconf(){
    std::cout << "updating DAC conf\n";

    for (int matrix = 0; matrix < client_->getNmatrices(); matrix++) {
        std::vector<QSpinBox *> internalDacSpinBoxes = daclists_[matrix]->getInternalDacSpinBoxes();
        std::vector<QCheckBox *> internalDacCheckBoxes = daclists_[matrix]->getInternalDacCheckBoxes();
//        std::vector<QSpinBox *> externalDacSpinBoxes = daclists_[matrix]->getExternalDacSpinBoxes();

        std::stringstream msg;
        msg << std::bitset<9>(0).to_string();
        for (int i = 0; i < internalDacSpinBoxes.size(); ++i) {
            msg << std::bitset<6>(internalDacSpinBoxes[i]->value()).to_string();
            msg << std::bitset<1>(internalDacCheckBoxes[i]->isChecked()).to_string();
        }
        msg << std::bitset<1344>(0).to_string();
        Client::nmoscode code(msg.str());
        client_->setCode((Client::Matrix) matrix, code);
//        std::vector<unsigned int> dacs;
//        for (int i = 0; i < externalDacSpinBoxes.size(); i++) {
//            dacs.push_back(externalDacSpinBoxes[i]->value());
//        }
//        client_->setExternalDACs((Client::Matrix) matrix, dacs);
    }
    std::vector<unsigned int> dacs;
    for (int i = 0; i < externalDacSpinBoxes_.size(); i++) {
        dacs.push_back(externalDacSpinBoxes_[i]->value());
    }
    client_->setExternalDACs(dacs);
    emit dacChanged();
}

void DACsForm::updateDACs(){

        std::vector<unsigned int> exdacs = client_->getExternalDACs();
        for (int i = 0; i < exdacs.size(); i++) {
            externalDacSpinBoxes_[i]->setValue(exdacs[i]);
        }
//    }
}

void DACsForm::updatePixelValue(){
    if(ui->dacForm_tabWidget->currentIndex()==TDAC::analog) {
        ui->val_spinBox->setValue(
                    analogPixMaps_[ui->matrix_spinBox->value()]->getPixelValue(ui->col_spinBox->value(),
                                                                               ui->row_spinBox->value()));
    }
    else if(ui->dacForm_tabWidget->currentIndex()==TDAC::digital){
        ui->val_spinBox->setValue(
                    digitalPixMaps_[ui->matrix_spinBox->value()]->getPixelValue(ui->col_spinBox->value(),
                                                                                ui->row_spinBox->value()));
    }
}

void DACsForm::mapClick(int matrix, int col, int row){
    ui->matrix_spinBox->setValue(matrix);
    ui->col_spinBox->setValue(col);
    ui->row_spinBox->setValue(row);
    this->updatePixelValue();
}
void DACsForm::on_setPixel_pushButton_clicked()
{
    if(ui->dacForm_tabWidget->currentIndex()==TDAC::analog) {
        analogPixMaps_[ui->matrix_spinBox->value()]->setPixel(ui->col_spinBox->value(), ui->row_spinBox->value(), ui->val_spinBox->value());
        analogPixMaps_[ui->matrix_spinBox->value()]->updateMap();
    }
    else if(ui->dacForm_tabWidget->currentIndex()==TDAC::digital){
        digitalPixMaps_[ui->matrix_spinBox->value()]->setPixel(ui->col_spinBox->value(), ui->row_spinBox->value(), ui->val_spinBox->value());
        digitalPixMaps_[ui->matrix_spinBox->value()]->updateMap();
    }
}

void DACsForm::on_setCol_pushButton_clicked()
{
    if(ui->dacForm_tabWidget->currentIndex()==TDAC::analog) {
        analogPixMaps_[ui->matrix_spinBox->value()]->setCol(ui->col_spinBox->value(), ui->val_spinBox->value());
        analogPixMaps_[ui->matrix_spinBox->value()]->updateMap();
    }
    else if(ui->dacForm_tabWidget->currentIndex()==TDAC::digital){
        digitalPixMaps_[ui->matrix_spinBox->value()]->setCol(ui->col_spinBox->value(), ui->val_spinBox->value());
        digitalPixMaps_[ui->matrix_spinBox->value()]->updateMap();
    }
}

void DACsForm::on_setRow_pushButton_clicked()
{
    if(ui->dacForm_tabWidget->currentIndex()==TDAC::analog) {
        analogPixMaps_[ui->matrix_spinBox->value()]->setRow(ui->row_spinBox->value(), ui->val_spinBox->value());
        analogPixMaps_[ui->matrix_spinBox->value()]->updateMap();
    }
    else if(ui->dacForm_tabWidget->currentIndex()==TDAC::digital){
        digitalPixMaps_[ui->matrix_spinBox->value()]->setRow(ui->row_spinBox->value(), ui->val_spinBox->value());
        digitalPixMaps_[ui->matrix_spinBox->value()]->updateMap();
    }
}
void DACsForm::on_setMatrix_pushButton_clicked()
{
    if(ui->dacForm_tabWidget->currentIndex()==TDAC::analog) {
        analogPixMaps_[ui->matrix_spinBox->value()]->setAllPixels(ui->val_spinBox->value());
        analogPixMaps_[ui->matrix_spinBox->value()]->updateMap();
    }
    else if(ui->dacForm_tabWidget->currentIndex()==TDAC::digital){
        digitalPixMaps_[ui->matrix_spinBox->value()]->setAllPixels(ui->val_spinBox->value());
        digitalPixMaps_[ui->matrix_spinBox->value()]->updateMap();
    }
}

bool DACsForm::save(QString fileName) {
    std::fstream outfile;

    outfile.open(fileName.toStdString(), std::fstream::out | std::fstream::binary);

    if (outfile.is_open()) {
        for (int matrix = 0; matrix < client_->getNmatrices(); matrix++) {
            std::vector<QSpinBox *> internalDacSpinBoxes = daclists_[matrix]->getInternalDacSpinBoxes();
            std::vector<QCheckBox *> internalDacCheckBoxes = daclists_[matrix]->getInternalDacCheckBoxes();
//            std::vector<QSpinBox *> externalDacSpinBoxes = daclists_[matrix]->getExternalDacSpinBoxes();

            for (int i = 0; i < internalDacSpinBoxes.size(); ++i) {
                outfile << internalDacSpinBoxes[i]->value() << "\n";
                outfile << internalDacCheckBoxes[i]->isChecked() << "\n";
            }
        }
        for (int i = 0; i < externalDacSpinBoxes_.size(); i++) {
            outfile << externalDacSpinBoxes_[i]->value() << "\n";
        }
    }
    else {
        std::cout << "Unable to open file\n";
        return false;
    }

    outfile.close();
    return true;
};

bool DACsForm::load(QString fileName) {
    std::fstream infile;

    infile.open(fileName.toStdString(), std::fstream::in | std::fstream::binary);

    if (infile.is_open()) {
        for (int matrix = 0; matrix < client_->getNmatrices(); matrix++) {
            std::vector<QSpinBox *> internalDacSpinBoxes = daclists_[matrix]->getInternalDacSpinBoxes();
            std::vector<QCheckBox *> internalDacCheckBoxes = daclists_[matrix]->getInternalDacCheckBoxes();
//            std::vector<QSpinBox *> externalDacSpinBoxes = daclists_[matrix]->getExternalDacSpinBoxes();

            for (int i = 0; i < internalDacSpinBoxes.size(); ++i) {
                int val;
                bool checked;
                infile >> val;
                internalDacSpinBoxes[i]->setValue(val);
                infile >> checked;
                internalDacCheckBoxes[i]->setChecked(checked);
            }
        }
        for (int i = 0; i < externalDacSpinBoxes_.size(); i++) {
            int val;
            infile >> val;
            externalDacSpinBoxes_[i]->setValue(val);
        }
        while(infile.good()){
            std::string str("");
            infile >> str;
            if(str == "NMOS_A"){infile >> str; this->load_TDAC(QString::fromStdString(str),analogPixMaps_.at(Client::Matrix::NMOS));}
            else if(str == "NMOS_D"){infile >> str; this->load_TDAC(QString::fromStdString(str),digitalPixMaps_.at(Client::Matrix::NMOS));}
            else if(str == "CMOS_A"){infile >> str; this->load_TDAC(QString::fromStdString(str),analogPixMaps_.at(Client::Matrix::CMOS));}
            else if(str == "CMOS_D"){infile >> str; this->load_TDAC(QString::fromStdString(str),digitalPixMaps_.at(Client::Matrix::CMOS));}
        }
    }
    else{
        std::cout << "Unable to open file\n";
        return false;
    }

    infile.close();
    return true;
};

bool DACsForm::load_TDAC(QString fileName, PixelMap *pixelmap) {

    std::fstream infile;
    infile.open(fileName.toStdString(), std::fstream::in | std::fstream::binary);

    if (infile.is_open()) {
        char c_val;
        int val;
        infile.ignore(1024,'\n');//ignore first line

        for(int icol=0;icol<MAX_COLS;icol++){
            infile.ignore(256,'\t');
            for(int irow=0;irow<MAX_ROWS;irow++){
                infile>>c_val;
                val = strtol(&c_val,NULL,16);
                pixelmap->setPixel(icol,irow,val);
            }
        }
        pixelmap->updateMap();
    }else{
        std::cout << "Unable to open file\n";
        return false;
    }

    infile.close();
    return true;

};

void DACsForm::on_loadMask_pushButton_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this, tr("Load TDAC Mask"),"./", tr("Mask(*.msk);;Text(*.txt);;All files(*)"));

    PixelMap *thismap;
    int current_index  =  ui->selectMask_comboBox->currentIndex();
    if(current_index == TDACmask::CMOSanalog){
        thismap = analogPixMaps_.at(Client::Matrix::CMOS);
    } else if(current_index == TDACmask::CMOSdigital){
        thismap = digitalPixMaps_.at(Client::Matrix::CMOS);
    } else if(current_index == TDACmask::NMOSanalog){
        thismap = analogPixMaps_.at(Client::Matrix::NMOS);
    } else if(current_index == TDACmask::NMOSdigital){
        thismap = digitalPixMaps_.at(Client::Matrix::NMOS);
    }

    bool test = this->load_TDAC(fileName,thismap);
    if (!test) std::cout<<"Error!"<<std::endl;

}

void DACsForm::on_applyMask_pushButton_clicked()
{
    Client::Matrix matrix;
    PixelMap *thismap;
    int current_index  =  ui->selectMask_comboBox->currentIndex();
    if(current_index == TDACmask::CMOSanalog){
        thismap = analogPixMaps_.at(Client::Matrix::CMOS);
        matrix = Client::Matrix::CMOS;
    } else if(current_index == TDACmask::CMOSdigital){
        thismap = digitalPixMaps_.at(Client::Matrix::CMOS);
        matrix = Client::Matrix::CMOS;
    } else if(current_index == TDACmask::NMOSanalog){
        thismap = analogPixMaps_.at(Client::Matrix::NMOS);
        matrix = Client::Matrix::NMOS;
    } else if(current_index == TDACmask::NMOSdigital){
        thismap = digitalPixMaps_.at(Client::Matrix::NMOS);
        matrix = Client::Matrix::NMOS;
    }

    Client::nmoscode code,old_code;

    old_code = client_->getCode(matrix);

    if(current_index == TDACmask::CMOSdigital || current_index == TDACmask::NMOSdigital){
        for(int roc_row = 0; roc_row<MAX_ROCS; ++roc_row){
            code = old_code;
            client_->setLoadDACCell(roc_row, true, code);
            //client_->setEnHitbus(roc_row,false,code);
            for(int roc_col = 0; roc_col < MAX_ROC_COLS; ++roc_col) {
                int col = roc_col;
                int row = roc_row;
                Client::convertROCtoPixel(col,row);
                client_->setDigThCol(roc_col, thismap->getPixelValue(col,row), code);
                bool enable = false;
                if(digitalPixMaps_.at(2)->getPixelValue(col,row)>0) enable = true;
                client_->setEnDigCol(roc_col,enable,code);
            }
            client_->programMatrix(matrix,code);
            //sleep(1);
            client_->setLoadDACCell(roc_row, false, code);
            client_->programMatrix(matrix,code);
            //sleep(1);
        }

    } else if (current_index == TDACmask::CMOSanalog || current_index == TDACmask::NMOSanalog){

        for (int row = 0; row < MAX_ROWS; ++row) {
            code = old_code;
            client_->setLoadDAC(row,true,code);
            for (int col = 0; col < MAX_COLS; ++col) {
                client_->setAnaThCol(col,thismap->getPixelValue(col,row),code);
            }
            client_->programMatrix(matrix,code);
        }
    }

    client_->programMatrix(matrix,old_code);
}

