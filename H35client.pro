#-------------------------------------------------
#
# Project created by QtCreator 2016-06-15T15:09:58
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = H35client
#TEMPLATE = app

win32 {
   QMAKE_CXXFLAGS += -FIw32pragma.h
}

CONFIG += qt warn_on thread

INCLUDEPATH += /Applications/root_v6.06.02/include/
INCLUDEPATH += inc/

win32:LIBS += -L/Applications/root_v6.06.02/lib/ -llibCore -llibRIO -llibNet \
        -llibHist -llibGraf -llibGraf3d -llibGpad -llibTree \
        -llibRint -llibPostscript -llibMatrix -llibPhysics \
        -llibGui -llibRGL
else:LIBS += -L/Applications/root_v6.06.02/lib/ -lCore -lRIO -lNet \
        -lHist -lGraf -lGraf3d -lGpad -lTree \
        -lRint -lPostscript -lMatrix -lPhysics \
        -lGui -lRGL

SOURCES += main.cpp\
        mainwindow.cpp \
    src/Socket.cpp \
    advancedform.cpp \
    src/Client.cpp \
src/Instrument.cpp \
    pixelmap.cpp \
    maskform.cpp \
    initform.cpp \
    dacsform.cpp \
    scanform.cpp \
    src/PulseGenerator.cpp

HEADERS  += mainwindow.h\
        inc/Socket.h \
    advancedform.h \
    inc/Client.h \
inc/Instrument.h \
    pixelmap.h \
    maskform.h \
    initform.h \
    inc/pageForm.h \
    dacsform.h \
    scanform.h

FORMS    += mainwindow.ui \
    advancedform.ui \
    maskform.ui \
    dacsform.ui \
    scanform.ui \
    initform.ui

DISTFILES +=
