/****************************************************************************
** Authors: Stefano Terzo (stefano.terzo@cern.ch)
**
** Institut de Física d'Altes Energies (IFAE)
** Edifici CN UAB Campus
** 08193 Bellaterra (Barcelona), Spain
**
****************************************************************************/

#include "advancedform.h"
#include "ui_advancedform.h"

AdvancedForm::AdvancedForm(ClientWidget *client, QWidget *parent) :
    PageForm(client,parent),
    ui(new Ui::AdvancedForm)
{
    client_= client;

    ui->setupUi(this);

    ui->enum_comboBox->addItem("Send single command");
    ui->enum_comboBox->addItem("Send OPCODE");
    ui->enum_comboBox->addItem("Initialise external bias voltages");
    ui->enum_comboBox->addItem("Program H35 NMOS");
    ui->enum_comboBox->addItem("Program H35 CMOS");
    ui->enum_comboBox->addItem("Program H35 ANA1");
    ui->enum_comboBox->addItem("Program H35 ANA2");
    ui->enum_comboBox->addItem("Program DAC7568");
    ui->enum_comboBox->addItem("H35 Enable bias pads");
    ui->enum_comboBox->addItem("H35 Disable bias pads");
    ui->enum_comboBox->addItem("NMOS Data acquisition");
    ui->enum_comboBox->addItem("NMOS Data acquisition DMA");
    ui->enum_comboBox->addItem("CMOS Data acquisition");
    ui->enum_comboBox->addItem("CMOS Data acquisition DMA");
    ui->enum_comboBox->addItem("NMOS TLU acquisition");
    ui->enum_comboBox->addItem("NMOS TLU acquisition DMA");
    ui->enum_comboBox->addItem("CMOS TLU acquisition");
    ui->enum_comboBox->addItem("CMOS TLU acquisition DMA");
    ui->enum_comboBox->addItem("NMOS TLUS acquisition");
    ui->enum_comboBox->addItem("NMOS TLUS acquisition DMA");
    ui->enum_comboBox->addItem("CMOS TLUS acquisition");
    ui->enum_comboBox->addItem("CMOS TLUS acquisition DMA");
    ui->enum_comboBox->addItem("Read debug");

    ui->pcbVersion_comboBox->addItem("H35demo: only monolithic wirebond");
    ui->pcbVersion_comboBox->addItem("H35demo: full wirebond");
    ui->pcbVersion_comboBox->setCurrentIndex(1);

    //DACS H35 CONFIG WIDGET
    QGridLayout *configControl_gridLayout = new QGridLayout(ui->control_tabWidget->widget(0));
    for(int r=0; r<5; ++r) {
        QLabel *label = new QLabel(QString("DAC%1").arg(r*4),this);
        label->setFixedSize(41,21);
        configControl_gridLayout->addWidget(label,r,0,Qt::AlignVCenter);
        for (int c = 0; c < 4; ++c) {
            if(r==4 && c==1) break;
            QWidget *sevenbits = new QWidget(this);
            QHBoxLayout *sevenbits_HLayout = new QHBoxLayout(this);
            QLineEdit *lineEdit = new QLineEdit(this);
            configControl_widgets_.push_back(lineEdit);
            lineEdit->setFixedSize(37,21);
            lineEdit->setInputMask("HH");
            sevenbits_HLayout->addWidget(lineEdit,Qt::AlignCenter);
            configControl_widgets_.push_back(new QCheckBox(this));
            configControl_widgets_.back()->setFixedSize(20,21);
            sevenbits_HLayout->addWidget(configControl_widgets_.back(),Qt::AlignCenter);
            sevenbits->setLayout(sevenbits_HLayout);
            configControl_gridLayout->addWidget(sevenbits, r, c + 1,Qt::AlignCenter);
        }
    }
    configControl_gridLayout->setHorizontalSpacing(0);
    configControl_gridLayout->setVerticalSpacing(0);

    std::vector<unsigned int> dacs = {0x1,1,0xa,0,0xa,0,0xa,0,
                                               0xa,0,0xa,0,0x1,0,0x1,0,
                                               0x3c,0,0xa,0,0x1e,0,0xf,0,
                                               0x14,0,0x5,0,0x32,0,0x5,1,
                                               0xA,0};
    for(int i=0; i<configControl_widgets_.size(); ++i){
        if(i%2==0) {
            QLineEdit *lineEdit = (QLineEdit *) configControl_widgets_[i];
            lineEdit->setText(QString::number(dacs[i],16));
        }
        else{
            QCheckBox *checkBox = (QCheckBox*) configControl_widgets_[i];
            checkBox->setChecked(dacs[i]);
        }
    }

    //ROW CONTROL WIDGET
    QWidget *rowContol = new QWidget(this);
    QGridLayout *rowControl_gridLayout = new QGridLayout(this);
    std::vector<QString> labels = {"+","EN_INJ","EN_TEST","EN_MEAS","LOAD DAC"};
    for(int i=0; i<5; i++) {
        QPushButton *pushButton = new QPushButton(labels[i], this);
        pushButton->setCheckable(true);
        pushButton->setFlat(true);
        pushButton->setMaximumSize(70,20);
        rowControl_gridLayout->addWidget(pushButton, 0, i, Qt::AlignCenter);
    }
    for(int r=0; r<MAX_ROWS; ++r) {
        QPushButton *label = new QPushButton(QString("ROW%1").arg(r),this);
        label->setCheckable(true);
        label->setFlat(true);
        label->setFixedSize(50,17);
        rowControl_gridLayout->addWidget(label,r+1,0,Qt::AlignCenter);
        for (int c = 0; c < 4; ++c) {
            rowControl_checkBoxes_.push_back(new QCheckBox(this));
            //rowControl_checkBoxes_.back()->setFixedSize(40, 15);
            rowControl_gridLayout->addWidget(rowControl_checkBoxes_.back(), r + 1, c + 1,Qt::AlignCenter);
            connect((QPushButton*)rowControl_gridLayout->itemAtPosition(0,c+1)->widget(),SIGNAL(clicked(bool)),rowControl_checkBoxes_.back(),SLOT(setChecked(bool)));
            connect((QPushButton*)rowControl_gridLayout->itemAtPosition(r+1,0)->widget(),SIGNAL(clicked(bool)),rowControl_checkBoxes_.back(),SLOT(setChecked(bool)));
            connect((QPushButton*)rowControl_gridLayout->itemAtPosition(0,0)->widget(),SIGNAL(clicked(bool)),rowControl_checkBoxes_.back(),SLOT(setChecked(bool)));
        }
    }
    rowContol->setLayout(rowControl_gridLayout);
    ui->rowControl_scrollArea->setWidget(rowContol);
    rowControl_gridLayout->setHorizontalSpacing(0);
    rowControl_gridLayout->setVerticalSpacing(0);

    //rowControl_gridLayout->setVerticalSpacing(0);

    //COLUMN CONTROL WIDGET
    QWidget *columnContol = new QWidget(this);
    QGridLayout *columnControl_gridLayout = new QGridLayout(this);
    labels = {"+","EN_SIG","EN_MEAS","EN_INJ","TRIMDAC[1]"};
    for(int i=0; i<5; i++) {
        QPushButton *pushButton = new QPushButton(labels[i], this);
        pushButton->setCheckable(true);
        pushButton->setFlat(true);
        pushButton->setMaximumSize(72,20);
        columnControl_gridLayout->addWidget(pushButton, 0, i, Qt::AlignCenter);
    }
    for(int r=0; r<MAX_COLS; ++r) {
        QPushButton *label = new QPushButton(QString("COL%1").arg(r),this);
        label->setCheckable(true);
        label->setFlat(true);
        label->setFixedSize(55,17);
        columnControl_gridLayout->addWidget(label,r+1,0,Qt::AlignCenter);
        for (int c = 0; c < 4; ++c) {
            columnControl_checkBoxes_.push_back(new QCheckBox(this));
            //rowControl_checkBoxes_.back()->setFixedSize(40, 15);
            columnControl_gridLayout->addWidget(columnControl_checkBoxes_.back(), r + 1, c + 1,Qt::AlignCenter);
            connect((QPushButton*)columnControl_gridLayout->itemAtPosition(0,c+1)->widget(),SIGNAL(clicked(bool)),columnControl_checkBoxes_.back(),SLOT(setChecked(bool)));
            connect((QPushButton*)columnControl_gridLayout->itemAtPosition(r+1,0)->widget(),SIGNAL(clicked(bool)),columnControl_checkBoxes_.back(),SLOT(setChecked(bool)));
            connect((QPushButton*)columnControl_gridLayout->itemAtPosition(0,0)->widget(),SIGNAL(clicked(bool)),columnControl_checkBoxes_.back(),SLOT(setChecked(bool)));
        }
    }
    columnContol->setLayout(columnControl_gridLayout);
    ui->columnControl_scrollArea->setWidget(columnContol);
    columnControl_gridLayout->setHorizontalSpacing(0);
    columnControl_gridLayout->setVerticalSpacing(0);

    //ROC CONTROL WIDGET
    QWidget *rocContol = new QWidget(this);
    QGridLayout *rocControl_gridLayout = new QGridLayout(this);
    labels = {"+","LOAD DAC CELL","EN_HB"};
    for(int i=0; i<3; i++) {
        QPushButton *pushButton = new QPushButton(labels[i], this);
        pushButton->setCheckable(true);
        pushButton->setFlat(true);
        pushButton->setMaximumSize(120,20);
        rocControl_gridLayout->addWidget(pushButton, 0, i, Qt::AlignCenter);
    }
    for(int r=0; r<MAX_ROCS; ++r) {
        QPushButton *label = new QPushButton(QString("ROC%1").arg(r),this);
        label->setCheckable(true);
        label->setFlat(true);
        label->setFixedSize(50,17);
        rocControl_gridLayout->addWidget(label,r+1,0,Qt::AlignCenter);
        for (int c = 0; c < 2; ++c) {
            rocControl_checkBoxes_.push_back(new QCheckBox(this));
            //rowControl_checkBoxes_.back()->setFixedSize(40, 15);
            rocControl_gridLayout->addWidget(rocControl_checkBoxes_.back(), r + 1, c + 1,Qt::AlignCenter);
            connect((QPushButton*)rocControl_gridLayout->itemAtPosition(0,c+1)->widget(),SIGNAL(clicked(bool)),rocControl_checkBoxes_.back(),SLOT(setChecked(bool)));
            connect((QPushButton*)rocControl_gridLayout->itemAtPosition(r+1,0)->widget(),SIGNAL(clicked(bool)),rocControl_checkBoxes_.back(),SLOT(setChecked(bool)));
            connect((QPushButton*)rocControl_gridLayout->itemAtPosition(0,0)->widget(),SIGNAL(clicked(bool)),rocControl_checkBoxes_.back(),SLOT(setChecked(bool)));
        }
    }
    rocContol->setLayout(rocControl_gridLayout);
    ui->rocControl_scrollArea->setWidget(rocContol);
    rocControl_gridLayout->setHorizontalSpacing(0);
    rocControl_gridLayout->setVerticalSpacing(0);

    //RECEIVE SENT WIDGET
    QWidget *dataCheck = new QWidget(this);
    QGridLayout *dataCheck_gridLayout = new QGridLayout(this);
    for(int r=0; r<NMOSBITS/LINEBITS; ++r) {
        for (int c = 0; c < 2; ++c) {
            QLineEdit *lineEdit = new QLineEdit(this);
            lineEdit->setReadOnly(true);
            if(c==0) dataSent_lineEdits_.push_back(lineEdit);
            else     {
                dataRecv_lineEdits_.push_back(lineEdit);
            }
            connect(lineEdit,SIGNAL(textChanged(QString)),this,SLOT(compareData_lineEdit()));
            //rowControl_checkBoxes_.back()->setFixedSize(40, 15);
            dataCheck_gridLayout->addWidget(lineEdit,r,c);
        }
    }
    dataCheck->setLayout(dataCheck_gridLayout);
    ui->dataCheck_scrollArea->setWidget(dataCheck);
    dataCheck_gridLayout->setVerticalSpacing(0);
}

AdvancedForm::~AdvancedForm()
{
    delete ui;
}

void AdvancedForm::on_send_command_pushbutton_clicked() {
    Client::opcode byte;

    switch(ui->enum_comboBox->currentIndex()) {
        case Action::sendSingleCommand: {
            if (opcode_ == OPC_PROG_DACS) {
                this->sendDAC7568();
            }
            else if (opcode_ == OPC_PROG_NMOS || opcode_ == OPC_PROG_CMOS) {
                //msg << std::bitset<9>(0).to_string();
                //std::string msg = get_dac_sent_verticalLayout();
                std::string msg = this->get_dac_control_tabWidget();
                Client::nmoscode code(msg);
                this->sendMessage(code);
                this->readMessage(code);
            }
            break;
        }
        case Action::sendOPCODE: {
            std::stringstream message;
            //message << "0x" << ui->OPCODE_lineEdit->text().toStdString();
            //Client::opcode byte = std::stoul(message.str(), nullptr, 16);
            byte = (Client::opcode) ui->OPCODE_lineEdit->text().toShort(nullptr, 16);
            std::cout << "Sent message: " << std::hex << (int) byte << "\n";
            this->sendMessage(byte);
            break;
        }
        case Action::initialiseExternalBias: {
            this->initExtBias();
            break;
        }
        case Action::programNMOS: {
            for (int i = 0; i < 2; i++) {
                byte = OPC_PROG_NMOS;
                this->sendMessage(byte);

                std::string msg = this->get_dac_control_tabWidget();
                Client::nmoscode code(msg);
                client_->setNMOScode(code);
                this->sendMessage(code);
                this->readMessage(code);
            }
            break;
        }
        case Action::programCMOS: {
            for (int i = 0; i < 2; i++) {
                byte = OPC_PROG_CMOS;
                this->sendMessage(byte);

                std::string msg = this->get_dac_control_tabWidget();
                Client::nmoscode code(msg);
                client_->setCMOScode(code);
                this->sendMessage(code);
                this->readMessage(code);
            }
            break;
        }
        case Action::programANA1: {
            for (int i = 0; i < 2; i++) {
                byte = OPC_PROG_ANA1;
                this->sendMessage(byte);

                std::string msg = this->get_dac_control_tabWidget();
                Client::nmoscode code(msg);
                client_->setANA1code(code);
                this->sendMessage(code);
                this->readMessage(code);
            }
            break;
        }
        case Action::programANA2: {
            for (int i = 0; i < 2; i++) {
                byte = OPC_PROG_ANA2;
                this->sendMessage(byte);

                std::string msg = this->get_dac_control_tabWidget();
                Client::nmoscode code(msg);
                client_->setANA2code(code);
                this->sendMessage(code);
                this->readMessage(code);
            }
            break;
        }
        case Action::programDAC7568: {
            byte = OPC_PROG_DACS;
            this->sendMessage(byte);
            this->sendDAC7568();
            break;
        }
        case Action::enableBiasPads: {
            byte = OPC_EN_BPADS;
            this->sendMessage(byte);
            break;
        }
        case Action::disableBiasPads: {
            byte = OPC_DIS_BPADS;
            this->sendMessage(byte);
            break;
        }
        case Action::NMOSdataAcquisition: {
            byte = OPC_ACQ_NMOS;
            this->dataAcquisition(byte, (unsigned long)ui->timeout_doubleSpinBox->value()*320000);
            break;
        }
        case Action::NMOSdataAcquisitionDMA: {
            byte = OPC_ACQ_NMOS_DMA;
            this->dataAcquisition(byte, (unsigned long)ui->timeout_doubleSpinBox->value());
            break;
        }
        case Action::CMOSdataAcquisition: {
            byte = OPC_ACQ_CMOS;
            this->dataAcquisition(byte, (unsigned long)ui->timeout_doubleSpinBox->value()*320000);
            break;
        }
        case Action::CMOSdataAcquisitionDMA: {
            byte = OPC_ACQ_CMOS_DMA;
            this->dataAcquisition(byte, (unsigned long)ui->timeout_doubleSpinBox->value());
            break;
        }
        case Action::NMOStluAcquisition: {
            byte = OPC_TLU_NMOS;
            this->triggeredAcquisition(byte, (unsigned long)ui->timeout_doubleSpinBox->value()*320000);
            break;
        }
        case Action::NMOStluAcquisitionDMA: {
            byte = OPC_TLU_NMOS_DMA;
            this->triggeredAcquisition(byte, (unsigned long)ui->timeout_doubleSpinBox->value());
            break;
        }
        case Action::CMOStluAcquisition: {
            byte = OPC_TLU_CMOS;
            this->triggeredAcquisition(byte, (unsigned long)ui->timeout_doubleSpinBox->value()*320000);
        }
        case Action::CMOStluAcquisitionDMA: {
            byte = OPC_TLU_CMOS_DMA;
            this->triggeredAcquisition(byte, (unsigned long)ui->timeout_doubleSpinBox->value());
        }
        case Action::NMOStlusAcquisition: {
            byte = OPC_TLUS_NMOS;
            this->triggeredAcquisition(byte, (unsigned long)ui->timeout_doubleSpinBox->value()*320000);
            break;
        }
        case Action::NMOStlusAcquisitionDMA: {
            byte = OPC_TLUS_NMOS_DMA;
            this->triggeredAcquisition(byte, (unsigned long)ui->timeout_doubleSpinBox->value());
            break;
        }
        case Action::CMOStlusAcquisition: {
            byte = OPC_TLUS_CMOS;
            this->triggeredAcquisition(byte, (unsigned long)ui->timeout_doubleSpinBox->value()*320000);
            break;
        }
        case Action::CMOStlusAcquisitionDMA: {
            byte = OPC_TLUS_CMOS_DMA;
            this->triggeredAcquisition(byte, (unsigned long)ui->timeout_doubleSpinBox->value());
            break;
        }
        case Action::readDebug: {
            byte = 0x0;
            std::cout << "reading...\n";
            while (byte != ui->OPCODE_lineEdit->text().toUShort(nullptr, 16)) {
                client_->readMessage(byte);
                std::cout << "byte: 0x" << std::hex << (int) byte << "\n";
            }
            break;
        }
        default:
            std::cout << "no command selected.\n";
    }
}

void AdvancedForm::triggeredAcquisition(unsigned char opcode, unsigned long int timeout)
{
    std::cout << "\nStarting triggered DAQ...\n";
    unsigned char byte = opcode;
    this->sendMessage(byte);

    std::stringstream msg;
    int eventBlock = ui->eventBlock_spinBox->value();
    int deadTime = ui->deadTime_spinBox->value();
    int trgDelay = ui->trgDelay_spinBox->value();
    msg << std::bitset<32>(ui->samplingEdge_checkBox->isChecked()).to_string() << std::bitset<32>(trgDelay).to_string()
        << std::bitset<32>(deadTime).to_string() << std::bitset<32>(eventBlock).to_string()
        << std::bitset<32>(timeout).to_string() << std::bitset<32>(ui->eventBuffer_spinBox->value()).to_string()
        << std::bitset<32>(ui->clocks_spinBox->value()).to_string();
    std::bitset<224> code(msg.str());
    client_->sendMessage(code);
    this->acquireData();

    std::cout << "Stopping triggered DAQ...\n";
    client_->sendMessage(std::bitset<32>(1));
    //Client::DAC7568 bb;
    //client_->readMessage(bb);
    byte = 0x0;
    //client_->sendMessage(byte);
    std::cout << "reading...\n";
    while(byte!=opcode) {
        client_->readMessage(byte);
        std::cout << "byte: 0x" << std::hex << (int) byte << "\n";
    }
}

void AdvancedForm::dataAcquisition(unsigned char opcode, unsigned long int timeout)
{
    unsigned char byte = opcode;
    this->sendMessage(byte);
//    std::cout << "reading message..\n";
//    this->readMessage(byte );
//    std::cout << "message read..\n";

    std::stringstream msg;
    msg << std::bitset<32>(ui->samplingEdge_checkBox->isChecked()).to_string() << std::bitset<32>(timeout).to_string()
        << std::bitset<32>(ui->eventBuffer_spinBox->value()).to_string()    << std::bitset<32>(ui->clocks_spinBox->value()).to_string();
    std::bitset<128> code(msg.str());
    client_->sendMessage(code);
    this->acquireData();
    client_->sendMessage(std::bitset<32>(1));
    //Client::DAC7568 bb;
    //client_->readMessage(bb);
    byte = 0x0;
    //client_->sendMessage(byte);
    std::cout << "reading...\n";
    while(byte!=opcode) {
        client_->readMessage(byte);
        std::cout << "byte: 0x" << std::hex << (int) byte << "\n";
    }
}

void AdvancedForm::sendDAC7568()
{
    std::stringstream msg;
    msg << std::bitset<4>(0).to_string() << std::bitset<4>(ui->dac_control_bits_spinBox->value()).to_string() <<
    std::bitset<4>(ui->dac_address_bits_spinBox->value()).to_string() <<
    std::bitset<12>(ui->dac_data_bits_spinBox->value()).to_string() << std::bitset<8>(0).to_string();
    Client::DAC7568 code = std::stoul(msg.str(), nullptr, 2);
    client_->sendMessage(code);
    this->readMessage(code);
}

void AdvancedForm::readMessage(std::string &message) {
        client_->recv(message);
        std::cout << "Received message:\n"<< message << "\n";
        ui->data_out_textEdit->append(QString::fromStdString(message));
}

void AdvancedForm::readMessage(Client::DAC7568 &code) {
    code.reset();
    client_->readMessage(code);
    std::cout << "Received code: "<<  Client::DAC7568(code).to_string() << "\n";
    ui->data_out_textEdit->append(QString::number(code.to_ulong()));
}

void AdvancedForm::readMessage(Client::nmoscode code) {
    code.reset();
    client_->readMessage(code);
    std::cout << "Received nmoscode: "<< Client::nmoscode(code).to_string() << "\n";
    this->set_data_lineEdit(dataRecv_lineEdits_,code);
}

void AdvancedForm::sendMessage(Client::nmoscode code) {
    this->set_data_lineEdit(dataSent_lineEdits_, code);
    Client::programcode pcode(std::bitset<32>(ui->pcbVersion_comboBox->currentIndex()).to_string() + code.to_string());
    client_->sendMessage(pcode);
}

void AdvancedForm::sendMessage(unsigned char byte) {

    std::stringstream ss;
    ss << "0x" << std::hex << (int)byte;
    std::cout << "Sent opcode: " << ss.str() << "\n";

    QPalette palette;
    if( !client_->sendOpcode(byte) ){
        palette.setColor(QPalette::Text, Qt::red);
    }
    else{
        palette.setColor(QPalette::Text, Qt::black);
    }
    ui->ACK_lineEdit->setPalette(palette);

    ss.str("");
    ss << "0x" << std::hex << (int) byte;
    ui->data_out_textEdit->append(QString::fromStdString(ss.str()));
    std::cout << "Received opcode: " << ss.str() << "\n";
    ui->ACK_lineEdit->setText(QString::fromStdString(ss.str()));
}

void AdvancedForm::initExtBias(){
    unsigned char byte = OPC_PROG_DACS;

    int ctrl = 3;
    //int addr = 0;
    std::vector<unsigned int> data = client_->getExternalDACs();
    for (unsigned int addr = 0; addr < data.size(); ++addr) {
        this->sendMessage(byte);

        std::stringstream msg;
        msg.str("");
        msg << std::bitset<4>(0).to_string() << std::bitset<4>(ctrl).to_string() <<
        std::bitset<4>(addr).to_string() << std::bitset<12>(data[addr]).to_string() <<
        std::bitset<8>(0).to_string();
        Client::DAC7568 code = std::stoul(msg.str(), nullptr, 2);
        client_->sendMessage(code);
        this->readMessage(code);
    }
}

std::string AdvancedForm::get_dac_sent_verticalLayout() {
    std::stringstream msg;
    for (int i = 0; i < dataSent_lineEdits_.size(); ++i) {
        msg << std::bitset<LINEBITS>(dataSent_lineEdits_[i]->text().toULongLong(nullptr, 16)).to_string();
    }
    return msg.str();
}

void AdvancedForm::set_data_lineEdit(std::vector<QLineEdit*> lineEdits,Client::nmoscode code) {
    for (int i = 0; i < lineEdits.size(); ++i) {
        std::stringstream ss;
        ss.str("");
        for (int bit = 0; bit < LINEBITS; ++bit) {
            ss << code[LINEBITS * (lineEdits.size() - i) - bit - 1];
        }
        long long int num = std::stoull(ss.str(), nullptr, 2);
        ss.str("");
        ss << std::hex << num;
        lineEdits[i]->setText(QString::fromStdString(ss.str()));
    }
}

void AdvancedForm::compareData_lineEdit() {
    for (int i = 0; i < dataRecv_lineEdits_.size(); ++i) {
        QPalette palette;
        if (dataRecv_lineEdits_[i]->text() != dataSent_lineEdits_[i]->text()) {
            palette.setColor(QPalette::Text, Qt::red);
        }
        else {
            palette.setColor(QPalette::Text, Qt::black);
        }
        dataRecv_lineEdits_[i]->setPalette(palette);
    }
}

std::string AdvancedForm::get_dac_control_tabWidget() {
    std::stringstream msg;

    msg << std::bitset<9>(0).to_string();
    for(int i=0; i<configControl_widgets_.size(); ++i){
        if(i%2==0) {
            QLineEdit *lineEdit = (QLineEdit *) configControl_widgets_[i];
            msg << std::bitset<6>(lineEdit->text().toULong(nullptr, 16)).to_string();
        }
        else{
            QCheckBox *checkBox = (QCheckBox*) configControl_widgets_[i];
            msg << std::bitset<1>(checkBox->isChecked()).to_string();
        }
    }
    for(int i=0; i<rowControl_checkBoxes_.size(); ++i){
        msg << std::bitset<1>(rowControl_checkBoxes_[i]->isChecked()).to_string();
    }
    for(int i=0; i<columnControl_checkBoxes_.size(); ++i){
        msg << std::bitset<1>(columnControl_checkBoxes_[i]->isChecked()).to_string();
    }
    for(int i=0; i<rocControl_checkBoxes_.size(); ++i){
        msg << std::bitset<1>(rocControl_checkBoxes_[i]->isChecked()).to_string();
    }

    std::cout << "message: " << msg.str() << std::endl;
    return msg.str();
}

bool AdvancedForm::acquireData() {
    bool isData;
    int events=0;
    int triggers=0;
    //bool stop = false;
    std::fstream outfile;
    outfile.open ("output.txt", std::fstream::out | std::fstream::app);

    if (outfile.is_open())
    {
        std::cout << "Writing out file\n";
    }
    else std::cout << "Unable to open file\n";

    stopScan_ = false;
    while(!stopScan_) {
        int32_t data_size = 0;
        client_->readMessage(&data_size, sizeof(int32_t)); //number of bytes that are going to be trasmitted
        std::cout << "received " << data_size << " bytes of data\n";
        std::cout << "received " << data_size/4 << " events\n";
        if(data_size==1) return false;
        //int32_t data[ui->eventBuffer_spinBox->value()];
        //client_->readMessage(data, sizeof(int32_t) * ui->eventBuffer_spinBox->value());
        //int32_t data[data_size/4];
        int32_t data[data_size/4];
        client_->readMessage(data, sizeof(int32_t) * data_size/4);


        int row=-1, col=-1, timestamp = -1;
        unsigned long fpgaTS;
        //for(int i=0; i<ui->eventBuffer_spinBox->value(); i++){
        for(int i=0; i<data_size/4; i++){
            isData = client_->decodeData(data[i],col, row, timestamp, fpgaTS);
            if(isData) {
                events++;
                std::cout << "col:\t\t" << std::dec << col << "\ttime_stamp:\t" << timestamp << "\trow:\t\t" << row << "\tfpga_time_stamp:\t\t" << fpgaTS << std::endl;
                outfile << col << " " << row << " " << timestamp << " " << fpgaTS << "\n";
            }
            else {
                std::cout << std::dec << "Trigger:\t" << triggers << "\tfpga_time_stamp:\t" << timestamp << std::endl;
                triggers++;
            }
        }
        if(events>=ui->maxEvents_spinBox->value() || triggers>=ui->maxEvents_spinBox->value()) {
            //if(stop) break;
            //stop = true;
            break;
        }
        else {
            client_->sendMessage(std::bitset<32>(0));
        }
        QCoreApplication::processEvents();
    }

//    client_->sendMessage(std::bitset<32>(1));
//    int32_t data[ui->eventBuffer_spinBox->value()];
//    client_->readMessage(data, sizeof(int32_t) * ui->eventBuffer_spinBox->value());
//    int row=-1, col=-1;
//    long long int timestamp;
//    for(int i=0; i<ui->eventBuffer_spinBox->value(); i++){
//        timeout = !client_->decodeData(data[i],col, row, timestamp);
//        if(timeout) {
//            std::cout << "TIMEOUT \n";
//        }
//        else {
//            events++;
//            std::cout << "col:\t\t" << col << "\ttime_stamp:\t" << timestamp << "\trow:\t\t" << row << std::endl;
//            outfile << col << " " << row << " " << timestamp << "\n";
//        }
//    }

    outfile.close();

    return true;
}
void AdvancedForm::on_stop_command_pushbutton_clicked()
{
    stopScan_ = true;
}
