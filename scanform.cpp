/****************************************************************************
** Authors: Stefano Terzo (stefano.terzo@cern.ch)
**
** Institut de Física d'Altes Energies (IFAE)
** Edifici CN UAB Campus
** 08193 Bellaterra (Barcelona), Spain
**
****************************************************************************/

#include <QFileInfo>
#include "scanform.h"
#include "ui_scanform.h"

ScanForm::ScanForm(ClientWidget *client, QWidget *parent) :
        PageForm(client,parent),
    ui(new Ui::ScanForm) {
    pulseGenerator_ = nullptr;

    client_ = client;
    ui->setupUi(this);

    ui->selectScan_comboBox->addItem("ANALOG_SCAN");
    ui->selectScan_comboBox->addItem("SOURCE_SCAN");
    ui->selectScan_comboBox->addItem("THRESHOLD_SCAN");
    ui->selectScan_comboBox->addItem("GPIX_TUNE");
    ui->selectScan_comboBox->addItem("GDAC_TUNE");
    ui->selectScan_comboBox->addItem("TPIX_TUNE");
    ui->selectScan_comboBox->addItem("TPIX_FIT_TUNE");
    ui->selectScan_comboBox->addItem("TDAC_TUNE");
    ui->selectScan_comboBox->addItem("TDAC_FIT_TUNE");
    ui->selectScan_comboBox->addItem("TRIGGER_SCAN");
    ui->selectScan_comboBox->addItem("TRIGGERSIMPLE_SCAN");
    ui->selectScan_comboBox->addItem("DELAY_SCAN");
    ui->selectScan_comboBox->addItem("TEST");
    ui->selectScan_comboBox->addItem("TRIGGER_RESPONSE");
    ui->selectScan_comboBox->addItem("NOISE_SCAN");
    ui->selectScan_comboBox->addItem("XRAY-SCAN");
    ui->selectScan_comboBox->addItem("THRESHOLD-LOOP");


    ui->dataFormat_comboBox->addItem("ASCII");
    ui->dataFormat_comboBox->addItem("RAW");

    for (unsigned int m = 0; m < client_->getNmatrices(); ++m) {
        ui->matrix_comboBox->addItem(QString::fromStdString(client_->getMatrixName((Client::Matrix) m)));
    }

    connect(ui->selectScan_comboBox, SIGNAL(currentIndexChanged(QString)), this, SLOT(updateOutfileName(QString)));

    //QObject::connect(&futureWatcher_, SIGNAL(finished()), ui->progressBar, SLOT(reset()));
    QObject::connect(&futureWatcher_, SIGNAL(finished()), this, SLOT(scanFinished()));
    QObject::connect(this, SIGNAL(scanCanceled()), &futureWatcher_, SLOT(cancel()));

    instrument_stackedWidget_ = new QStackedWidget(this);
    ui->instrument_scrollArea->setWidget(instrument_stackedWidget_);

    ui->progressBar->reset();
    timer_ = new QTimer(this);
    QObject::connect(timer_, SIGNAL(timeout()), this, SLOT(updateProgressBar()));
    QObject::connect(ui->instruments_comboBox, SIGNAL(currentIndexChanged(int)), instrument_stackedWidget_,
                     SLOT(setCurrentIndex(int)));
    QObject::connect(ui->dma_checkBox, &QCheckBox::clicked,
                     [=](void) {
                         if (ui->dma_checkBox->isChecked()){
                             ui->timeout_doubleSpinBox->setMaximum(pow(2,32)-1);
                             ui->timeout_doubleSpinBox->setDecimals(0);
                         }
                         else{
                             ui->timeout_doubleSpinBox->setMaximum(13421);
                             ui->timeout_doubleSpinBox->setDecimals(2);
                         }
                     });
}

ScanForm::~ScanForm()
{
    delete ui;
    delete timer_;
}

void ScanForm::updateOutfileName(QString scanName){
    QString outFileName = ui->outputFile_lineEdit->text();
    outFileName = outFileName.section("/",0,-2) + "/" + scanName + ".dat";

    int num =0;
    while(QFile::exists(outFileName)){
        outFileName = outFileName.section("/",0,-2) + "/" + scanName + "_" + QString::number(num,10) + ".dat";
        num++;
    }
    ui->outputFile_lineEdit->setText(outFileName);
}

void ScanForm::writeHeader(int test){
    QDateTime dateTime = QDateTime::currentDateTime();

    std::fstream outfile;
    if(!this->openOutfile(outfile,(ScanForm::DataFormat) ui->dataFormat_comboBox->currentIndex())) return;

    //outfile << std::hex << 0xcafe << " ";
    if(ui->dataFormat_comboBox->currentIndex() == DataFormat::ASCII) {
        outfile << ui->selectScan_comboBox->currentText().toStdString() << " " <<
        dateTime.toString("dd MM yyyy HH mm ss").toStdString() << "\n";
        outfile << std::dec << "Inj: " << ui->inj_spinBox->value()
        << " Timeout: " << ui->timeout_doubleSpinBox->value()
        << " Amp: " << currentAmp_
        << " Test: " << test << "\n";
    }
    else{
        int32_t h = 0xcafe;
//        std::stringstream msg;
//        msg << std::bitset<1>(0).to_string() << std::bitset<16>(h).to_string()
//        << std::bitset<32>(deadTime).to_string() << std::bitset<32>(eventBlock).to_string()
//        << std::bitset<32>(timeout).to_string() << std::bitset<32>(ui->eventBuffer_spinBox->value()).to_string()
//        << std::bitset<32>(ui->clocks_spinBox->value()).to_string();
//        std::bitset<32> head(msg.str());

        outfile.write((char*)&h,sizeof(h));
    }

    outfile.close();
}

void ScanForm::on_startScan_pushButton_clicked() {
    std::cout << ui->selectScan_comboBox->currentText().toStdString() << " started...\n";
    stopScan_ = false;
    timeout_ = false;
    ui->startScan_pushButton->setDisabled(true);
    if (!ui->continueScan_checkBox->isChecked()) {
        this->updateOutfileName(ui->selectScan_comboBox->currentText());
        events_ = 0;
    }
    ui->progressBar->reset();
    triggers_ = 0;
    injections_ = 0;
    headerIndex_ = 0;
    timer_->start(100);

    for (int col = 0; col < MAX_COLS; ++col) {
        for (int row = 0; row < MAX_ROWS; ++row) {
            pixelMap_[col][row] = 0;
        }
    }

    switch (ui->selectScan_comboBox->currentIndex()) {
        case Scan::analog: {
            if (this->setupPulser()) {
                ui->progressBar->setRange(0,
                                          (ui->rowInterval_spinBox->value() + 1) *
                                          (ui->colInterval_spinBox->value() + 1)-1);
                counter_ = &injections_;
                futureWatcher_.setFuture(QtConcurrent::run(this, &ScanForm::pixelLoop, &ScanForm::analogScan));
            }
            else this->scanFinished();
            break;
        }
        case Scan::source: {
            ui->progressBar->setRange(0, ui->maxEvents_spinBox->value());
            counter_ = &events_;
            futureWatcher_.setFuture(QtConcurrent::run(this, &ScanForm::sourceScan));
            break;
        }
        case Scan::trigger: {
            if (ui->matrix_comboBox->currentIndex() == Client::Matrix::NMOS){
                if(ui->dma_checkBox->isChecked()) opcode_ = OPC_TLU_NMOS_DMA;
                else opcode_ = OPC_TLU_NMOS;
            }
            else if (ui->matrix_comboBox->currentIndex() == Client::Matrix::CMOS){
                if(ui->dma_checkBox->isChecked()) opcode_ = OPC_TLU_CMOS_DMA;
                else opcode_ = OPC_TLU_CMOS;
            }
            ui->progressBar->setRange(0, ui->maxEvents_spinBox->value());
            counter_ = &triggers_;
            futureWatcher_.setFuture(QtConcurrent::run(this, &ScanForm::triggerScan));
            break;
        }
        case Scan::triggerSimple: {
            if (ui->matrix_comboBox->currentIndex() == Client::Matrix::NMOS){
                if(ui->dma_checkBox->isChecked()) opcode_ = OPC_TLUS_NMOS_DMA;
                else opcode_ = OPC_TLUS_NMOS;
            }
            else if (ui->matrix_comboBox->currentIndex() == Client::Matrix::CMOS){
                if(ui->dma_checkBox->isChecked()) opcode_ = OPC_TLUS_CMOS_DMA;
                else opcode_ = OPC_TLUS_CMOS;
            }
            ui->progressBar->setRange(0, ui->maxEvents_spinBox->value());
            counter_ = &triggers_;
            futureWatcher_.setFuture(QtConcurrent::run(this, &ScanForm::triggerScan));
            break;
        }
        case Scan::delay: {
            ui->progressBar->setRange(0, ui->maxEvents_spinBox->value() * 1000 / ui->vsteps_spinBox->value());
            counter_ = &triggers_;
            futureWatcher_.setFuture(QtConcurrent::run(this, &ScanForm::delayScan));
            break;
        }
        case Scan::threshold: {
            ui->progressBar->setRange(0,
                                      (ui->rowInterval_spinBox->value() + 1) * (ui->colInterval_spinBox->value() + 1) *
                                      ui->vsteps_spinBox->value()-1);
            counter_ = &injections_;
            futureWatcher_.setFuture(
                    QtConcurrent::run(this, &ScanForm::injLoop, &ScanForm::pixelLoop, &ScanForm::analogScan));
            break;
        }
        case Scan::gpix: {
            ui->progressBar->setRange(0,
                                      (ui->rowInterval_spinBox->value() + 1) * (ui->colInterval_spinBox->value() + 1) *
                                      ui->vsteps_spinBox->value()-1);
            counter_ = &injections_;
            futureWatcher_.setFuture(QtConcurrent::run(this, &ScanForm::gdacPixelScan));
            break;
        }
        case Scan::tpix: {
            ui->progressBar->setRange(0,
                                      (ui->rowInterval_spinBox->value() + 1) * (ui->colInterval_spinBox->value() + 1) *
                                      4-1);
            counter_ = &injections_;
            futureWatcher_.setFuture(QtConcurrent::run(this, &ScanForm::tdacPixelScan));
            break;
        }
        case Scan::tpix_fit: {
            ui->progressBar->setRange(0,
                                      (ui->rowInterval_spinBox->value() + 1) * (ui->colInterval_spinBox->value() + 1) *
                                      ui->vsteps_spinBox->value() * 4-1);
            counter_ = &injections_;
            futureWatcher_.setFuture(QtConcurrent::run(this, &ScanForm::tpixFitScan));
            break;
        }
        case Scan::gdac: {
            ui->progressBar->setRange(0,
                                      (ui->rowInterval_spinBox->value() + 1) * (ui->colInterval_spinBox->value() + 1) *
                                      ui->vsteps_spinBox->value()-1);
            counter_ = &injections_;
            futureWatcher_.setFuture(QtConcurrent::run(this, &ScanForm::gdacScan));
            break;
        }
        case Scan::tdac_fit: {
            ui->progressBar->setRange(0,
                                      (ui->rowInterval_spinBox->value() + 1) * (ui->colInterval_spinBox->value() + 1) *
                                      ui->vsteps_spinBox->value() * 8-1);
            counter_ = &injections_;
            futureWatcher_.setFuture(QtConcurrent::run(this, &ScanForm::tdacFitScan));
            break;
        }
        case Scan::tdac: {
            ui->progressBar->setRange(0,
                                      (ui->rowInterval_spinBox->value() + 1) * (ui->colInterval_spinBox->value() + 1) *
                                      16-1);
            counter_ = &injections_;
            futureWatcher_.setFuture(QtConcurrent::run(this, &ScanForm::tdacScan));
            break;
        }
        case Scan::triggerResponse: {
            if (this->setupPulser()) {
                ui->progressBar->setRange(0,
                                          (ui->colInterval_spinBox->value() + 1) *
                                          (ui->rowInterval_spinBox->value() + 1)-1);
                counter_ = &injections_;
                futureWatcher_.setFuture(QtConcurrent::run(this, &ScanForm::pixelLoop, &ScanForm::trgResponceScan));
            }
            else this->scanFinished();
            break;
        }
        case Scan::noise: {
            if (this->setupPulser()) {
                if (ui->matrix_comboBox->currentIndex() == Client::Matrix::NMOS) {
                    if (ui->dma_checkBox->isChecked()) opcode_ = OPC_TLUS_NMOS_DMA;
                    else opcode_ = OPC_TLUS_NMOS;
                }
                else if (ui->matrix_comboBox->currentIndex() == Client::Matrix::CMOS) {
                    if (ui->dma_checkBox->isChecked()) opcode_ = OPC_TLUS_CMOS_DMA;
                    else opcode_ = OPC_TLUS_CMOS;
                }
                ui->progressBar->setRange(0, ui->maxEvents_spinBox->value());
                counter_ = &triggers_;
                futureWatcher_.setFuture(QtConcurrent::run(this, &ScanForm::triggerScan));
            }
            else this->scanFinished();
            break;
        }
        case Scan::xray:{
            if (this->setupPulser()) {
                if (ui->matrix_comboBox->currentIndex() == Client::Matrix::NMOS) {
                    if (ui->dma_checkBox->isChecked()) opcode_ = OPC_TLUS_NMOS_DMA;
                    else opcode_ = OPC_TLUS_NMOS;
                }
                else if (ui->matrix_comboBox->currentIndex() == Client::Matrix::CMOS) {
                    if (ui->dma_checkBox->isChecked()) opcode_ = OPC_TLUS_CMOS_DMA;
                    else opcode_ = OPC_TLUS_CMOS;
                }
                ui->progressBar->setRange(0, ui->maxEvents_spinBox->value()*ui->vsteps_spinBox->value());
                counter_ = &triggers_;
                futureWatcher_.setFuture(QtConcurrent::run(this, &ScanForm::xrayScan));
            }
            else this->scanFinished();
            break;
        }
        case Scan::thrLoop: {
            ui->progressBar->setRange(0,
                                      (ui->rowInterval_spinBox->value() + 1) * (ui->colInterval_spinBox->value() + 1) *
                                      ui->vsteps_spinBox->value()*ui->vsteps_spinBox->value());
            counter_ = &injections_;
            futureWatcher_.setFuture(QtConcurrent::run(this, &ScanForm::thrLoopScan));
            break;
        }
        case Scan::test: {
            counter_ = &injections_;
            futureWatcher_.setFuture(QtConcurrent::run(this, &ScanForm::testScan));
            break;
        }
        default:
            std::cout << "WARNING: this scan is not implemented.\n";
    }
}

void ScanForm::scanFinished(){
    ui->startScan_pushButton->setDisabled(false);
    std::cout << "Scan finished\n";
    timer_->stop();
    ui->continueScan_checkBox->setChecked(false);
    //if(!stopScan_ && !timeout_) ui->progressBar->setValue(ui->progressBar->value()+1);
}

void ScanForm::sourceScan(){
    do {
        this->startDAQ(ui->dma_checkBox->isChecked());
        timeout_ = !this->acquireData();
        this->stopDAQ();
    }while((timeout_ && !stopScan_ && ui->continueScan_checkBox->isChecked()));
    std::cout << "Total events: " << events_ << std::endl;
}

bool ScanForm::acquireData() {
    std::fstream outfile;
    if(!this->openOutfile(outfile,(ScanForm::DataFormat) ui->dataFormat_comboBox->currentIndex())) return false;

    bool notimeout = true;
    while (!stopScan_) {
        if(!(notimeout = this->readData(outfile))) break;

        if ((*counter_) < ui->maxEvents_spinBox->value())
            client_->sendMessage(std::bitset<32>(0));
        else break;
    }
    outfile.close();

    return notimeout;
}

void ScanForm::xrayScan(){
    if(!this->setupPulser()) return;
<<<<<<< HEAD
        // was 1709
    int hi=1400;
    int lo=1244;
=======

    unsigned int hi=1709;
    unsigned int lo=1244;
>>>>>>> 1ee0a1c4cecd7f894676f0ae3035fdf0c1a71ea2


    std::vector<unsigned int> dacs = client_->getExternalDACs();
    std::cout<<"dacs vector" << dacs.size() << "\n";
    unsigned int step = (hi-lo)/ui->scanSteps_spinBox->value();
    std::cout<<"makeing steps: " << step << "\n";

    for(unsigned int nThPix=hi; nThPix>=lo+step; nThPix-=step) {
        std::cout<<"loop on ThPix: " << nThPix << "\n";
        dacs[3] = nThPix;

        std::cout<<"set dacs \n";
        client_->setExternalDACs(dacs);
        std::cout<<"init bias \n";
        client_->initExtBias();

        ui->outputFile_lineEdit->setText("xray_nThPix_"+QString::number(nThPix)+".dat");
        std::cout<<"Filename: " << ui->outputFile_lineEdit->text().toStdString() << "\n";

        triggers_=0;
        injections_=0;
        events_=0;
        this->triggerScan();
        if(stopScan_) break;
    }
}

void ScanForm::thrLoopScan(){
    if(!this->setupPulser()) return;

<<<<<<< HEAD

        //156 steps!!
    int hi=1400;
    int lo=1244;
=======
    unsigned int hi=1709;
    unsigned int lo=1244;
>>>>>>> 1ee0a1c4cecd7f894676f0ae3035fdf0c1a71ea2

    std::vector<unsigned int> dacs = client_->getExternalDACs();
    unsigned int step = (hi-lo)/ui->scanSteps_spinBox->value();
    for(unsigned int nThPix=hi; nThPix>=lo+step; nThPix-=step) {
        dacs[3] = nThPix;
        client_->setExternalDACs(dacs);
        client_->initExtBias();

        ui->outputFile_lineEdit->setText("thr_nThPix_"+QString::number(nThPix)+".dat");
        this->injLoop(&ScanForm::pixelLoop,&ScanForm::analogScan);
         if(stopScan_) break;
    }
}

void ScanForm::triggerScan(){
    do{
        this->startTrgDAQ();
        if(ui->selectScan_comboBox->currentIndex()==Scan::noise || ui->selectScan_comboBox->currentIndex()==Scan::xray){
            this->startPulses();
            std::cout << "start pulses for noise scan\n";
        }
        timeout_ = !this->acquireData();
        this->stopDAQ();
    }while((timeout_ && !stopScan_ && (ui->continueScan_checkBox->isChecked() || ui->selectScan_comboBox->currentIndex()==Scan::noise ||ui->selectScan_comboBox->currentIndex()==Scan::xray )));
    std::cout << "Total hits/triggers: " << events_ << "/" << triggers_ << " = " << 1.*events_/triggers_<< std::endl;
    std::cout << "Event rate: " << events_ << "/" << ui->eventBlock_spinBox->value()*25e-9*triggers_ << " = " << 1.*events_/(ui->eventBlock_spinBox->value()*25e-9*triggers_) << " Hz" << std::endl;
}

void ScanForm::delayScan(){
    double rate,best_rate=0;
    int best_delay=0;
    int step = ui->vsteps_spinBox->value();
    for(int i=ui->triggerDelay_spinBox->value(); i<1001; i+=step) {
        if (ui->matrix_comboBox->currentIndex() == Client::Matrix::NMOS) opcode_ = OPC_TLU_NMOS;
        else if (ui->matrix_comboBox->currentIndex() == Client::Matrix::CMOS) opcode_ = OPC_TLU_CMOS;
        std::cout << "trgdelay: " << i << std::endl;
        ui->triggerDelay_spinBox->setValue(i);

        this->startTrgDAQ();
        this->acquireData();
        this->stopDAQ();
        rate = 1.*events_/triggers_;
        if(rate > best_rate) {
            best_rate = rate;
            best_delay = i;
        }
        if(stopScan_) break;
    }
    std::cout << "=======================================================\n";
    std::cout << std::dec << "Best delay: " << best_delay << "  -  at rate: " << rate << std::endl;
    std::cout << "=======================================================\n";
}

void ScanForm::gdacPixelScan(){
    if(!this->setupPulser()) return;
    float targetThr = (float) ui->threshold_doubleSpinBox->value();
    this->setAmplitude(targetThr);
    std::vector<unsigned int> dacs = client_->getExternalDACs();
    unsigned int step = (1500-dacs[4])/ui->vsteps_spinBox->value();
    for(unsigned int nThPix=1500; nThPix>=dacs[4]+step; nThPix-=step) {
        dacs[3] = nThPix;
        client_->setExternalDACs(dacs);
        client_->initExtBias();
        this->pixelLoop(&ScanForm::analogScan);
        unsigned int injTot = 0;
        for (int col = 0; col < MAX_COLS; ++col) {
            for (int row = 0; row < MAX_ROWS; ++row) {
                injTot += pixelMap_[col][row];
                pixelMap_[col][row] = 0;
            }
        }
        double meanOcc = 1. * injTot / (ui->inj_spinBox->value() * 300. * 16.);
        std::cout << "Mean occupancy at " << nThPix << " is " << meanOcc << "\n";
        if (meanOcc>0.5) {
            std::cout << "meanOcc>0.5\n";
            break;
        }
    }
}

void ScanForm::tdacPixelScan(){
    Client::Matrix matrix = (Client::Matrix)ui->matrix_comboBox->currentIndex();
    Client::nmoscode code, old_code;

    old_code = client_->getCode(matrix);

    short resultMap[MAX_COLS][MAX_ROWS];

    double best_deviation[MAX_COLS][MAX_ROWS];
    for (int col = 0; col < MAX_COLS; ++col) {
        for (int row = 0; row < MAX_ROWS; ++row) {
            resultMap[col][row] = 0;
            best_deviation[col][row] = 1000000;
        }
    }
    if(!this->setupPulser()) return;
    float targetThr = (float) ui->threshold_doubleSpinBox->value();
    this->setAmplitude(targetThr);

    for(unsigned short n=0; n<4; ++n) {
        for(int row = 0; row<MAX_ROWS; ++row) {
            code = old_code;
            client_->setLoadDAC(row, true, code);
            for (int col = 0; col < MAX_COLS; ++col) {
                client_->setAnaThCol(col, n, code);
            }
            client_->programMatrix(matrix,code);
        }
        client_->programMatrix(matrix,old_code);

        this->pixelLoop(&ScanForm::analogScan);

        for (int col = 0; col < MAX_COLS; ++col) {
            for (int row = 0; row < MAX_ROWS; ++row) {
                double deviation = std::abs((1.*pixelMap_[col][row])/(1.*ui->inj_spinBox->value()) - 0.50);
                if(deviation < best_deviation[col][row]){
                    best_deviation[col][row] = deviation;
                    resultMap[col][row] = n;
                }
                pixelMap_[col][row] = 0;
            }
        }
    }

    std::fstream outfile;
    outfile.open("./tpix_mask.msk", std::fstream::out | std::fstream::app);

    if (outfile.is_open()) {
        //std::cout << "Writing out file\n";
    }
    else std::cout << "Unable to open file\n";

    outfile << "###";
    for(int col = 0; col < MAX_COLS; ++col) {
        outfile << col << "\t";
    }
    for(int row = 0; row<MAX_ROWS; ++row){
        code = old_code;
        client_->setLoadDAC(row, true, code);
        outfile << "\n" << row;
        for(int col = 0; col < MAX_COLS; ++col) {
            client_->setAnaThCol(col, resultMap[col][row], code);
            std::cout << "["<<col<<","<<row<<"]="<<best_deviation[col][row]<<"\n";
            if (ui->matrix_comboBox->currentIndex() == Client::Matrix::NMOS){analogNmosMap_->setPixel(col,row,resultMap[col][row]);}
            else if (ui->matrix_comboBox->currentIndex() == Client::Matrix::CMOS){analogCmosMap_->setPixel(col,row,resultMap[col][row]);}
            outfile << "\t" << resultMap[col][row];
        }
        client_->programMatrix(matrix,code);
    }

    outfile.close();

    client_->programMatrix(matrix,old_code);
}

void ScanForm::gdacScan(){
    if(!this->setupPulser()) return;
    float targetThr = (float) ui->threshold_doubleSpinBox->value();
    this->setAmplitude(targetThr);
    std::vector<unsigned int> dacs = client_->getExternalDACs();
    unsigned int step = (1500-dacs[6])/ui->vsteps_spinBox->value();
    for(unsigned int nTh=1500; nTh>=dacs[6]+step; nTh-=step) {
        dacs[5] = nTh;
        client_->setExternalDACs(dacs);
        client_->initExtBias();
        this->pixelLoop(&ScanForm::analogScan);
        unsigned int injTot = 0;
        for (int col = 0; col < MAX_COLS; ++col) {
            for (int row = 0; row < MAX_ROWS; ++row) {
                injTot += pixelMap_[col][row];
                pixelMap_[col][row] = 0;
            }
        }
        double meanOcc = 1. * injTot / (ui->inj_spinBox->value() * 300. * 16.);
        std::cout << "Mean occupancy at " << nTh << " is " << meanOcc << "\n";
        if (meanOcc>0.5) {
            std::cout << "meanOcc>0.5\n";
            break;
        }
    }
}

void ScanForm::tpixFitScan(){
    Client::Matrix matrix = (Client::Matrix)ui->matrix_comboBox->currentIndex();
    Client::nmoscode code, old_code;

    old_code = client_->getCode(matrix);

    if(!this->setupPulser()) return;

    for (unsigned int n = 0; n < 4; ++n) {
        std::cout << "Main Loop " << n << " of 3\n";
        headerIndex_ = n;
        for (int row = 0; row < MAX_ROWS; ++row) {
            code = old_code;
            client_->setLoadDAC(row,true,code);
            for (int col = 0; col < MAX_COLS; ++col) {
                client_->setAnaThCol(col,n,code);
            }
            client_->programMatrix(matrix,code);
        }
        client_->programMatrix(matrix,old_code);

        std::cout << "Start Injection Loop\n";
        this->injLoop(&ScanForm::pixelLoop, &ScanForm::analogScan);
    }
    std::cout << "End of Main Loop\n";
}

void ScanForm::tdacFitScan(){
    Client::Matrix matrix = (Client::Matrix)ui->matrix_comboBox->currentIndex();
    Client::nmoscode code, old_code;
    old_code = client_->getCode(matrix);

    short resultMap[MAX_COLS][MAX_ROWS];

    double best_deviation[MAX_COLS][MAX_ROWS];
    for (int col = 0; col < MAX_COLS; ++col) {
        for (int row = 0; row < MAX_ROWS; ++row) {
            resultMap[col][row] = 0;
            best_deviation[col][row] = 1000000;
        }
    }
    if(!this->setupPulser()) return;
    float targetThr = (float) ui->threshold_doubleSpinBox->value();

    for (unsigned int n = 0; n < 8; ++n) {
        std::cout << "Main Loop " << n << " of 7\n";
        headerIndex_ = n;
        for (int roc_row = 0; roc_row < MAX_ROCS; ++roc_row) {
            code = old_code;
            client_->setLoadDACCell(roc_row,true,code);
            //client_->setEnHitbus(roc_row, true, code);
            for (int roc_col = 0; roc_col < MAX_ROC_COLS; ++roc_col) {
                client_->setDigThCol(roc_col, n, code);
                client_->setEnHitbusCol(roc_col,true,code);
                client_->setEnDigCol(roc_col,true,code);
            }
            for(int i=0; i<10; ++i) {
                //client_->setLoadDACCell(roc_row,true,code);
                client_->programMatrix(matrix, code);
                usleep(ui->confDelay_spinBox->value());
                client_->setLoadDACCell(roc_row, false, code);
                client_->programMatrix(matrix, code);
                usleep(ui->confDelay_spinBox->value());
            }
        }
        client_->programMatrix(matrix,old_code);

        std::cout << "Start Injection Loop\n";
        this->injLoop(&ScanForm::pixelLoop, &ScanForm::analogScan);
    }
    std::cout << "End of Main Loop\n";
}

void ScanForm::tdacScan(){
    Client::Matrix matrix = (Client::Matrix)ui->matrix_comboBox->currentIndex();
    Client::nmoscode code, old_code;
    old_code = client_->getCode(matrix);

    short resultMap[MAX_COLS][MAX_ROWS];

    double best_deviation[MAX_COLS][MAX_ROWS];
    for (int col = 0; col < MAX_COLS; ++col) {
        for (int row = 0; row < MAX_ROWS; ++row) {
            resultMap[col][row] = 0;
            best_deviation[col][row] = 1000000;
        }
    }
    if(!this->setupPulser()) return;

    for (unsigned int n = 0; n < 8; ++n) {
        std::cout << "Main Loop " << n << " of 7\n";
        headerIndex_ = n;
        for (int roc_row = 0; roc_row < MAX_ROCS; ++roc_row) {
            code = old_code;
            client_->setLoadDACCell(roc_row,true,code);
            //client_->setEnHitbus(roc_row, true, code);
            for (int roc_col = 0; roc_col < MAX_ROC_COLS; ++roc_col) {
                client_->setEnHitbusCol(roc_col,true,code);
                client_->setEnDigCol(roc_col,true,code);
                client_->setDigThCol(roc_col, n, code);
            }
            for(int i=0; i<10; ++i) {
                //client_->setLoadDACCell(roc_row,true,code);
                client_->programMatrix(matrix, code);
                usleep(ui->confDelay_spinBox->value());
                client_->setLoadDACCell(roc_row, false, code);
                client_->programMatrix(matrix, code);
                usleep(ui->confDelay_spinBox->value());
            }
        }
        client_->programMatrix(matrix,old_code);
        usleep(ui->confDelay_spinBox->value());

        std::cout << "Start Injection Loop\n";
        this->writeHeader(n);
        this->pixelLoop(&ScanForm::analogScan);
    }

    client_->programMatrix(matrix,old_code);
}

void ScanForm::testScan(){
    Client::Matrix matrix = (Client::Matrix)ui->matrix_comboBox->currentIndex();
    Client::nmoscode code, old_code;
    old_code = client_->getCode(matrix);

    if(!this->setupPulser()) return;

    for (unsigned int n = 0; n < 16; ++n) {
        this->writeHeader(n);
        std::cout << "Main Loop " << n << " of 16\n";
        headerIndex_ = n;
        for (int roc_row = 0; roc_row < MAX_ROCS; ++roc_row) {
            code = old_code;
            client_->setLoadDACCell(roc_row,true,code);
            client_->setEnHitbus(roc_row, false, code);
            for (int roc_col = 0; roc_col < MAX_ROC_COLS; ++roc_col) {
                client_->setEnHitbusCol(roc_col,true,code);
                client_->setDigThCol(roc_col, n, code);
            }
            client_->programMatrix(matrix,code);
        }
        client_->programMatrix(matrix,old_code);

        std::cout << "Start Injection Loop\n";
        this->pixelLoop(&ScanForm::analogScan);
    }
    std::cout << "End of Main Loop\n";
}

void ScanForm::temperatureLoop(void (ScanForm::*fn)(void (ScanForm::*)()), void (ScanForm::*arg)()){
    if(!this->setupChamber()) return;
    float min = (float) -20;
    float max = (float) 20;
    float step = (float) (max - min) / ui->vsteps_spinBox->value();

    for (float temp = min; temp < max ; temp += step) {
        climateChamber_->setTemperature(temp);
        this->writeHeader(headerIndex_);

        (this->*fn)(arg);
        if (stopScan_) break;
        //this->updateOutfileName(ui->selectScan_comboBox->currentText());
        headerIndex_++;
    }
}

void ScanForm::temperatureLoop(void (ScanForm::*fn)()){

}

void ScanForm::injLoop(void (ScanForm::*fn)(void (ScanForm::*)()), void (ScanForm::*arg)()){
    if(!this->setupPulser()) return;
    float min = (float) ui->vmin_doubleSpinBox->value();
    float max = (float) ui->vmax_doubleSpinBox->value();
    float step = (float) (ui->vmax_doubleSpinBox->value() - ui->vmin_doubleSpinBox->value()) /
                 ui->vsteps_spinBox->value();

    for (float amp = min; amp < max; amp += step) {
        this->setAmplitude(amp);
        this->writeHeader(headerIndex_);

        (this->*fn)(arg);
        if (stopScan_) break;
        //this->updateOutfileName(ui->selectScan_comboBox->currentText());
    }
}

void ScanForm::injLoop(void (ScanForm::*fn)()){
    if(!this->setupPulser()) return;
    float min = (float) ui->vmin_doubleSpinBox->value();
    float max = (float) ui->vmax_doubleSpinBox->value();
    float step = (float) (ui->vmax_doubleSpinBox->value() - ui->vmin_doubleSpinBox->value()) /
                 ui->vsteps_spinBox->value();

    for (float amp = min; amp < max; amp += step) {
        this->setAmplitude(amp);
        this->writeHeader(headerIndex_);

        (this->*fn)();

        if (stopScan_) break;
        //this->updateOutfileName(ui->selectScan_comboBox->currentText());
    }
}

void ScanForm::pixelLoop(void (ScanForm::*fn)()){
    Client::Matrix matrix = (Client::Matrix)ui->matrix_comboBox->currentIndex();
    Client::nmoscode code, old_code;

    old_code = client_->getCode(matrix);

    Client::nmoscode reply;
    int colInt = ui->colInterval_spinBox->value()+1;
    int rowInt = ui->rowInterval_spinBox->value()+1;

    for(int c=0; c<colInt; c++) {
        for (int r = 0; r < rowInt; r++) {
            //std::cout << "C" << c << "R" << r << "\n";
            //std::cout << code.to_string() << "\n";
            code = old_code;
            for (int ci = c; ci < MAX_COLS; ci += colInt) {
                for (int ri = r; ri < MAX_ROWS; ri += rowInt) {
                    client_->setEnInj(ci, ri, true, code);
                }
            }

            client_->programMatrix(matrix,code);

            (this->*fn)();

            client_->programMatrix(matrix,old_code);
            injections_++;
            if (stopScan_) break;
        }
        if (stopScan_) break;
    }
}

void ScanForm::analogScan() {
    std::fstream outfile;
    if(!this->openOutfile(outfile,(ScanForm::DataFormat) ui->dataFormat_comboBox->currentIndex())) return;

    this->startDAQ(ui->dma_checkBox->isChecked());
    this->startPulses();
    this->readData(outfile, 0);
    this->stopDAQ();
    outfile.close();
}

bool ScanForm::setAmplitude(float volts){
    pulseGenerator_->setAmplitude(volts);
    if(pulseGenerator_->operationComplete()) std::cout << "Device ready\n";
    currentAmp_ = volts;
    return true;
};

bool ScanForm::startPulses() {
    pulseGenerator_->trigger();
    return true;
};

bool ScanForm::setupPulser() {
    if (pulseGenerator_ == nullptr){
        std::cout << "ERROR: no pulse generator found.\n";
        return false;
    }

    std::stringstream ss;
    std::string msg;

    std::cout << "Instrument: " << pulseGenerator_->identify() << "\n";
    if(pulseGenerator_->reset())
        std::cout << "Device successfully resetted\n";
    if(pulseGenerator_->operationComplete())
        std::cout << "Device ready\n";

    pulseGenerator_->setDisplayOn(true);
    pulseGenerator_->setOutputLoad(50);
    pulseGenerator_->setPulse(pulseGeneratorPars_[1]->value()*1e-9,pulseGeneratorPars_[3]->value());
    //pulseGenerator_->setAmplitude(pulseGeneratorPars_[0]);
    this->setAmplitude(pulseGeneratorPars_[0]->value());
    pulseGenerator_->setBurst(ui->inj_spinBox->value());
    std::cout << pulseGenerator_->status() << "\n";
    pulseGenerator_->setOutput();

    if(pulseGenerator_->operationComplete()) std::cout << "Device ready\n";

    return true;
}

bool ScanForm::readPulser() {
    if (pulseGenerator_ == nullptr){
        std::cout << "ERROR: no pulse generator found.\n";
        return false;
    }

    return true;
}

bool ScanForm::setupChamber() {
    if(climateChamber_ == nullptr) {
        std::cout << "ERROR: no climate chamber found.\n";
        return false;
    }

    if(!climateChamber_->setTemperature(climateChamberPars_[0]->value())) return false;

    return this->readChamber();
}

bool ScanForm::readChamber() {
    if(climateChamber_ == nullptr) {
        std::cout << "ERROR: no climate chamber found.\n";
        return false;
    }

    std::vector<std::string> tokens = climateChamber_->readStatus();
    double setpoint = QString::fromStdString(tokens[0]).toDouble();
    if(setpoint != climateChamberPars_[0]->value()) return false;

    climateChamberPars_[0]->setValue(setpoint);
    climateChamberPars_[2]->setValue(QString::fromStdString(tokens[1]).toDouble());
    return true;
}

bool ScanForm::setupVoltage() {
    if (voltageSource_ == nullptr) {
        std::cout << "ERROR: no voltage source found.\n";
        return false;
    }

    voltageSource_->setCurrentLimit(voltageSourcePars_[4]->value()*1e-3);
    voltageSource_->setOutput();
    return this->setVoltage(voltageSourcePars_[0]->value(), true);
}

bool ScanForm::readVoltage() {
    if (voltageSource_ == nullptr) {
        std::cout << "ERROR: no voltage source found.\n";
        return false;
    }
    return true;
}

bool ScanForm::setVoltage(double volts, bool checkLim) {
    /*
    double svolt = voltageSource_->readVoltage();
    double vstep = voltageSourcePars_[2]->value();
    int sign = 1;
    if(volts < svolt){
        sign =-1;
    }

    for (double v = svolt; sign*v < volts; v+=sign*vstep ) {
        voltageSource_->setVoltage(v);

        //voltageSourcePars_[1]->setValue(v);
        double curr = voltageSource_->readCurrent();
        //std::cout<< curr << std::endl;
        voltageSourcePars_[5]->setValue(curr);
        //if(checkLim && fabs(curr*1e3)>voltageSourcePars_[4]->value()){
        //    std::cout << "WARNING: current limit reached, ramping down.\n";
        //    this->setVoltage(0,false);
        //    return false;
        //}
        QCoreApplication::processEvents();
        sleep((unsigned int)voltageSourcePars_[3]->value());
        voltageSourcePars_[1]->setValue(voltageSource_->readVoltage());
    }
    */
    voltageSource_->setVoltage(volts);
    voltageSourcePars_[1]->setValue(voltageSource_->readVoltage());
    double curr = voltageSource_->readCurrent();
    voltageSourcePars_[5]->setValue(curr);

    return true;
}

void ScanForm::startTrgDAQ(){
    unsigned char byte = opcode_;
    client_->sendOpcode(byte);

    unsigned long int timeout = (unsigned long) ui->timeout_doubleSpinBox->value();
    if(!ui->dma_checkBox->isChecked()) timeout*=320000;

    std::bitset<32> timeout_bitset(timeout);
    if(ui->maxTimeout_checkBox->isChecked())  timeout_bitset.set();

    int eventBlock = ui->eventBlock_spinBox->value();
    int deadTime = ui->deadTime_spinBox->value();
    int trgDelay = ui->triggerDelay_spinBox->value();
    std::stringstream msg;
    msg << std::bitset<32>(ui->clockDelay_spinBox->value()).to_string() << std::bitset<32>(ui->samplingEdge_checkBox->isChecked()).to_string()
        << std::bitset<32>(trgDelay).to_string() << std::bitset<32>(deadTime).to_string() << std::bitset<32>(eventBlock).to_string()
        << timeout_bitset.to_string() << std::bitset<32>(ui->eventBuffer_spinBox->value()).to_string()
        << std::bitset<32>(ui->clocks_spinBox->value()).to_string();
    std::bitset<256> code(msg.str());
    client_->sendMessage(code);
}

void ScanForm::trgResponceScan(){
    if (ui->matrix_comboBox->currentIndex() == Client::Matrix::NMOS) opcode_ = OPC_TLUS_NMOS;
    else if (ui->matrix_comboBox->currentIndex() == Client::Matrix::CMOS) opcode_ = OPC_TLUS_CMOS;

    this->startTrgDAQ();

    std::string msg = "*WAI";
    pulseGenerator_->write(msg);
    this->startPulses();

    std::fstream outfile;
    outfile.open(ui->outputFile_lineEdit->text().toStdString().c_str(), std::fstream::out | std::fstream::app);

    if (!outfile.is_open()) {
        std::cout << "ERROR: unable to open file: " << ui->outputFile_lineEdit->text().toStdString() << "\n";
    }
    else this->readData(outfile);

    outfile.close();

    this->stopDAQ();
}

void ScanForm::startDAQ(bool dma) {
    opcode_ = 0x0;
    if (ui->matrix_comboBox->currentIndex() == Client::Matrix::NMOS){
        if(dma) opcode_ = OPC_ACQ_NMOS_DMA;
        else opcode_ = OPC_ACQ_NMOS;
    }
    else if (ui->matrix_comboBox->currentIndex() == Client::Matrix::CMOS){
        if(dma) opcode_ = OPC_ACQ_CMOS_DMA;
        else opcode_ = OPC_ACQ_CMOS;
    }
    unsigned char byte = opcode_;
    client_->sendOpcode(byte);
    unsigned long int timeout = (unsigned long) ui->timeout_doubleSpinBox->value();
    if(!ui->dma_checkBox->isChecked()) timeout*=320000;

    std::bitset<32> timeout_bitset(timeout);
    if(ui->maxTimeout_checkBox->isChecked())  timeout_bitset.set();

    std::stringstream msg;
    msg << std::bitset<32>(ui->clockDelay_spinBox->value()).to_string() << std::bitset<32>(ui->samplingEdge_checkBox->isChecked()).to_string() << timeout_bitset.to_string()
        << std::bitset<32>(ui->eventBuffer_spinBox->value()).to_string() << std::bitset<32>(ui->clocks_spinBox->value()).to_string();
    std::bitset<160> code(msg.str());
    client_->sendMessage(code);
}

bool ScanForm::readData(std::fstream &outfile, int outputFreq) {

    int32_t data_size = 0;
    client_->readMessage(&data_size, sizeof(int32_t)); //number of bytes that are going to be trasmitted
    //std::cout << "received " << std::dec << data_size << " bytes of data\n";
    //std::cout << "received " << std::dec << data_size/4 << " events\n";
    if(data_size==1) return false;

    int row=-1, col=-1, timestamp=-1;
    unsigned long int fpgaTS;
    for (int i = 0; i < data_size/4; i++) {

        int32_t data;
        client_->readMessage(&data, sizeof(int32_t));

        if(ui->dataFormat_comboBox->currentIndex() == DataFormat::RAW){
            outfile.write((char*)&data,sizeof(data));

            if(Client::DAC7568(data)[31]) triggers_++;
            else events_++;

            continue;
        }

        bool isData = client_->decodeData(data, col, row, timestamp, fpgaTS);

        if (isData) {
            outfile << col << " " << row << " " << timestamp << " " << fpgaTS << "\n";
            if (col > MAX_COLS || col < 0 || row > MAX_ROWS || row < 0) {
                std::cout << "WARNING: bad data event:\n";
                std::cout << "col:\t\t" << std::dec << col << "\ttime_stamp:\t" << timestamp << "\trow:\t\t" << row <<
                "\tfpga_time_stamp:\t\t" << fpgaTS << std::endl;
            }
            else {
                pixelMap_[col][row]++;
                events_++;
            }
            if (outputFreq>0 && (*counter_) % outputFreq == 0)
                std::cout << "col:\t\t" << std::dec << col << "\ttime_stamp:\t" << timestamp << "\trow:\t\t" << row <<
                "\tfpga_time_stamp:\t" << fpgaTS << std::endl;
        }
        else {
            if (outputFreq>0 && (*counter_) % outputFreq == 0)
                std::cout << std::dec << "Trigger:\t" << triggers_ << "\tfpga_time_stamp:\t" << fpgaTS << std::endl;
            outfile << std::dec << 1905 << " " << triggers_ << " " << fpgaTS << "\n";
            triggers_++;
        }
    }

    return true;
}

void ScanForm::on_stopScan_pushButton_clicked()
{
    stopScan_ = true;
    emit scanCanceled();
    std::cout << "Stopping scan...\n";
}

void ScanForm::on_selectScan_comboBox_currentIndexChanged(int index)
{
    if(index == Scan::analog){
        this->setDefaultEnabled(0,1,1,1,1,1,0,0,0,0,0,0);
    }
    else if(index == Scan::source){
        this->setDefaultEnabled(1,0,1,1,1,0,0,0,0,0,0,0);
    }
    else if(index == Scan::trigger || index == Scan::triggerSimple){
        this->setDefaultEnabled(1,0,0,1,1,0,0,0,1,1,1,0);
        ui->maxTimeout_checkBox->setChecked(true);
    }
    else if(index == Scan::threshold || index == Scan::test || index == Scan::thrLoop){
        this->setDefaultEnabled(0,1,1,1,1,1,1,1,0,0,0,0);
    }
    else if(index == Scan::tdac || index == Scan::tpix){
        this->setDefaultEnabled(0,1,1,1,1,1,0,0,0,0,0,1);
    }
    else if(index == Scan::tdac_fit || index == Scan::tpix_fit){
        this->setDefaultEnabled(0,1,1,1,1,1,1,1,0,0,0,0);
    }
    else if(index == Scan::gdac || index == Scan::gpix){
        this->setDefaultEnabled(0,1,1,1,1,1,0,1,0,0,0,1);
    }
    else if(index == Scan::delay){
        this->setDefaultEnabled(1,0,0,1,1,0,0,1,1,1,1,0);
    }
    else if(index == Scan::triggerResponse){
        this->setDefaultEnabled(1,1,1,1,1,1,0,0,1,1,1,0);
    }
    else if(index == Scan::noise || index == Scan::xray){
        this->setDefaultEnabled(1,0,1,1,1,0,0,1,1,1,1,0);
        ui->inj_spinBox->setValue(1);
    }
}

void ScanForm::setDefaultEnabled(bool maxEvents, bool injections, bool timeout,bool clocks,bool buffer, bool colRowInterval, bool voltageMinMax, bool steps, bool eventBlock, bool deadTime, bool delay, bool threshold ){
    ui->maxEvents_spinBox->setEnabled(maxEvents);
    ui->inj_spinBox->setEnabled(injections);
    ui->timeout_doubleSpinBox->setEnabled(timeout);
    ui->clocks_spinBox->setEnabled(clocks);
    ui->eventBuffer_spinBox->setEnabled(buffer);
    ui->colInterval_spinBox->setEnabled(colRowInterval);
    ui->rowInterval_spinBox->setEnabled(colRowInterval);
    ui->vmin_doubleSpinBox->setEnabled(voltageMinMax);
    ui->vmax_doubleSpinBox->setEnabled(voltageMinMax);
    ui->vsteps_spinBox->setEnabled(steps);
    ui->eventBlock_spinBox->setEnabled(eventBlock);
    ui->deadTime_spinBox->setEnabled(deadTime);
    ui->triggerDelay_spinBox->setEnabled(delay);
    ui->threshold_doubleSpinBox->setEnabled(threshold);
}

void ScanForm::refreshInstruments(std::vector<Instrument *> *instruments) {

    for(int i = instrument_stackedWidget_->count(); i >= 0; i--)
    {
        QWidget* widget = instrument_stackedWidget_->widget(i);
        instrument_stackedWidget_->removeWidget(widget);
    }
    ui->instruments_comboBox->clear();

    pulseGeneratorPars_.clear();
    voltageSourcePars_.clear();
    climateChamberPars_.clear();

    for(unsigned int i=0; i<instrumentWidgetList_.size(); ++i) {
        if (instrumentWidgetList_[i]!= nullptr) delete instrumentWidgetList_[i];
    }
    instrumentWidgetList_.clear();

    instrumentList_.clear();

    for(auto inst : *instruments){
        //std::cout << inst->getInterface() << ":" << inst->getType() << " - " <<  inst->getName() << std::endl;
        if(!inst->isConnected()) continue;
        if(inst->getType()==Instrument::Type::VoltageSource){
            ui->instruments_comboBox->addItem(QString::fromStdString(inst->getName()));
            voltageSource_ = (VoltageSource*) inst;
            instrumentWidgetList_.push_back(new QWidget(this));
            instrumentList_.push_back(inst);

            QGridLayout *voltageSource_gridLayout = new QGridLayout(instrumentWidgetList_.back());
            voltageSource_gridLayout->setAlignment(Qt::AlignTop);
            std::vector<QString> labels = {"Voltage SetPoint","Voltage","Ramp Step","Settle Time","Current Limit","Current"};

            double curr = voltageSource_->readCurrent();
            double volt = voltageSource_->readVoltage();
            std::vector<double> def = {volt,volt,5.,1.,1.,curr};
            std::vector<std::pair<double,double> > limits = {std::make_pair(0., 1100.),
                                                             std::make_pair(0., 1100.),
                                                             std::make_pair(0., 1100.),
                                                             std::make_pair(0., 5.),
                                                             std::make_pair(0.001, 3000.),
                                                             std::make_pair(-5000., 5000.)
            };
            std::vector<int> decimals = {2,2,2,0,6,6};
            std::vector<QString> suff = {" V"," V"," V"," s"," mA"," mA"};
            for(int i=0; i<labels.size(); i++) {
                voltageSourcePars_.push_back( new QDoubleSpinBox(instrumentWidgetList_.back()));
                if(i==1 || i==5){
                    voltageSourcePars_.back()->setButtonSymbols(QAbstractSpinBox::NoButtons);
                    voltageSourcePars_.back()->setDisabled(true);
                }
                QLabel *label = new QLabel(instrumentWidgetList_.back());
                label->setText(labels[i]);
                voltageSourcePars_.back()->setMaximum(limits[i].second);
                voltageSourcePars_.back()->setMinimum(limits[i].first);
                voltageSourcePars_.back()->setValue(def[i]);
                voltageSourcePars_.back()->setDecimals(decimals[i]);
                voltageSourcePars_.back()->setSuffix(suff[i]);
                voltageSource_gridLayout->addWidget(label, i/2, 0+2*(i%2), Qt::AlignTop);
                voltageSource_gridLayout->addWidget(voltageSourcePars_.back(), i/2, 1+2*(i%2), Qt::AlignTop);
            }
            //instrument_stackedWidget_->addWidget(instrumentWidgetList_.back());
        }
        else if(inst->getType()==Instrument::Type::PulseGenerator){
            ui->instruments_comboBox->addItem(QString::fromStdString(inst->getName()));
            instrumentWidgetList_.push_back(new QWidget(this));
            pulseGenerator_ = (PulseGenerator*) inst;
            instrumentList_.push_back(inst);

            QGridLayout *pluseGenerator_gridLayout = new QGridLayout(instrumentWidgetList_.back());
            pluseGenerator_gridLayout->setAlignment(Qt::AlignTop);
            std::vector<QString> labels = {"Amplitude","Width","Period","Frequency"};

            std::vector<double> def = {1.5,100,5E-4,2000};
            std::vector<std::pair<double,double> > limits = {std::make_pair(0.1,5.0),std::make_pair(20.0,1000.0),std::make_pair(1e-5,1.0),std::make_pair(1.0,100000.0)};
            std::vector<int> decimals = {1,0,6,0};
            std::vector<QString> suff = {" V"," ns"," s"," Hz"};

            for(unsigned int i=0; i<labels.size(); i++) {
                pulseGeneratorPars_.push_back( new QDoubleSpinBox(instrumentWidgetList_.back()));
                QLabel *label = new QLabel(instrumentWidgetList_.back());
                label->setText(labels[i]);
                pulseGeneratorPars_.back()->setMaximum(limits[i].second);
                pulseGeneratorPars_.back()->setMinimum(limits[i].first);
                pulseGeneratorPars_.back()->setValue(def[i]);
                pulseGeneratorPars_.back()->setDecimals(decimals[i]);
                pulseGeneratorPars_.back()->setSuffix(suff[i]);
                pluseGenerator_gridLayout->addWidget(label, i/2, 0+2*(i%2), Qt::AlignTop);
                pluseGenerator_gridLayout->addWidget(pulseGeneratorPars_.back(), i/2, 1+2*(i%2), Qt::AlignTop);
            }
            QObject::connect(pulseGeneratorPars_[2], &QDoubleSpinBox::editingFinished,[=](void) { pulseGeneratorPars_[3]->setValue(1./pulseGeneratorPars_[2]->value()); });
            QObject::connect(pulseGeneratorPars_[3], &QDoubleSpinBox::editingFinished,[=](void) { pulseGeneratorPars_[2]->setValue(1./pulseGeneratorPars_[3]->value()); });
            //instrument_stackedWidget_->addWidget(instrumentWidgetList_.back());
        }
        else if(inst->getType()==Instrument::Type::ClimateChamber){
            ui->instruments_comboBox->addItem(QString::fromStdString(inst->getName()));
            climateChamber_ = (ClimateChamber*) inst;
            instrumentWidgetList_.push_back(new QWidget(this));
            instrumentList_.push_back(inst);

            QGridLayout *climateChamber_gridLayout = new QGridLayout(instrumentWidgetList_.back());
            climateChamber_gridLayout->setAlignment(Qt::AlignTop);
            std::vector<QString> labels = {"Setpoint","Settle time","Temperature","Min T","Steps","Max T"};

            std::vector<std::string> tokens = climateChamber_->readStatus();
            std::vector<double> def = {QString::fromStdString(tokens[0]).toDouble(),10,QString::fromStdString(tokens[1]).toDouble(),-35.,5.,20.};
            std::vector<std::pair<double,double> > limits = {std::make_pair(-40.,30.),std::make_pair(1.,1000.),std::make_pair(-50.,50.),std::make_pair(-50.,50.),std::make_pair(0.,50.),std::make_pair(-50.,50.)};
            std::vector<int> decimals = {0,0,1,0,0,0};
            std::vector<QString> suff = {" C"," s"," C"," C"," C"," C"};
            for(int i=0; i<labels.size(); i++) {
                climateChamberPars_.push_back( new QDoubleSpinBox(instrumentWidgetList_.back()));
                if(i==2){
                    climateChamberPars_.back()->setButtonSymbols(QAbstractSpinBox::NoButtons);
                    climateChamberPars_.back()->setDisabled(true);
                }
                QLabel *label = new QLabel(instrumentWidgetList_.back());
                label->setText(labels[i]);
                climateChamberPars_.back()->setMaximum(limits[i].second);
                climateChamberPars_.back()->setMinimum(limits[i].first);
                climateChamberPars_.back()->setValue(def[i]);
                climateChamberPars_.back()->setDecimals(decimals[i]);
                climateChamberPars_.back()->setSuffix(suff[i]);
                climateChamber_gridLayout->addWidget(label, i/2, 0+2*(i%2), Qt::AlignTop);
                climateChamber_gridLayout->addWidget(climateChamberPars_.back(), i/2, 1+2*(i%2), Qt::AlignTop);
            }
        }
        else continue;

        instrument_stackedWidget_->addWidget(instrumentWidgetList_.back());
    }
    std::cout << "Passed instruments:" << "\n";
    for(int i = 0; i<(*instruments).size(); ++i){
        std::cout << i << ") " << (*instruments)[i]->getName() << "\n";
    }
}

void ScanForm::on_setupInstruments_pushButton_clicked()
{
    Instrument::Type type = instrumentList_.at(ui->instruments_comboBox->currentIndex())->getType();
    if(type == Instrument::Type::ClimateChamber) this->setupChamber();
    else if(type == Instrument::Type::PulseGenerator) this->setupPulser();
    else if(type == Instrument::Type::VoltageSource) this->setupVoltage();
}

void ScanForm::on_readInstruments_pushButton_clicked()
{
    Instrument::Type type = instrumentList_.at(ui->instruments_comboBox->currentIndex())->getType();
    if(type == Instrument::Type::ClimateChamber) this->readChamber();
    else if(type == Instrument::Type::PulseGenerator) this->readPulser();
    else if(type == Instrument::Type::VoltageSource) this->readVoltage();
}

void ScanForm::stopDAQ() {
    client_->sendMessage(std::bitset<32>(1));
    //check that the FPGA reply with the right opcode = it doesn't have data left to send
    unsigned char byte = 0x0;
    while (byte != opcode_) {
        client_->readMessage(byte);
        //std::cout << std::hex << (int) byte << "\n";
    }
}

void ScanForm::updateProgressBar() {
    ui->progressBar->setValue(*counter_);
}

bool ScanForm::openOutfile(std::fstream &outfile, ScanForm::DataFormat dataFormart){
    if(dataFormart == DataFormat::ASCII) {
        outfile.open(ui->outputFile_lineEdit->text().toStdString().c_str(), std::fstream::out | std::fstream::app);
    }
    else if(dataFormart == DataFormat::RAW){
        outfile.open(ui->outputFile_lineEdit->text().toStdString().c_str(), std::fstream::out | std::fstream::app | std::fstream::binary);
    }
    else{
        std::cout << "ERROR: unkown output data format\n";
        return false;
    }

    if(!outfile.is_open()){
        std::cout << "ERROR: unable to open file: " << ui->outputFile_lineEdit->text().toStdString() << "\n";
        return false;
    }
    else return true;
}
