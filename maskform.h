/****************************************************************************
** Authors: Stefano Terzo (stefano.terzo@cern.ch)
**
** Institut de Física d'Altes Energies (IFAE)
** Edifici CN UAB Campus
** 08193 Bellaterra (Barcelona), Spain
**
****************************************************************************/

#ifndef MASKFORM_H
#define MASKFORM_H

#include "pageForm.h"
#include "pixelmap.h"

namespace Ui {
class MaskForm;
}

class MaskForm : public PageForm
{
    Q_OBJECT

public:
    explicit MaskForm(ClientWidget *client, QWidget *parent = 0);
    ~MaskForm();

public slots:
    void mapClick(int matrix,int col, int row);
    void updateMasks(Client::nmoscode code);

private slots:
    void on_test_pushButton_clicked();

    void on_pixelStatus_checkBox_clicked(bool checked);

    void updatePixelValue();

private:
    Ui::MaskForm *ui;

    std::vector<PixelMap*> pixMaps_;
};

#endif // MASKFORM_H
