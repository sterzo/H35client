/****************************************************************************
** Created by Stefano Terzo (stefano.terzo@cern.ch)
**
** Institut de Física d'Altes Energies (IFAE)
** Edifici CN UAB Campus
** 08193 Bellaterra (Barcelona), Spain
**
****************************************************************************/

#ifndef EUDAQ_INCLUDED_ExampleHardware
#define EUDAQ_INCLUDED_ExampleHardware

#include "eudaq/Timer.hh"
#include "Client.h"
#include <vector>

// A name to identify the raw data format of the events generated
// Modify this to something appropriate for your producer.
static const std::string EVENT_TYPE = "H35client";

namespace eudaq {

    class H35clientHardware {
    public:
        H35clientHardware(Client *client);

        void Setup(int);

        void PrepareForRun(int buffer, int clocks);

        bool EventsPending() const;

        unsigned NumSensors() const;

        std::vector<unsigned char> ReadSensor(int sensorid);

        void CompletedEvent();

        //void Start();

        void Stop(){}//{m_timer.Stop();}

    private:
        unsigned short m_numsensors, m_triggerid;
        eudaq::Timer m_timer;
        double m_nextevent;
        Client *m_client;
        int m_buffer;
        bool m_events_pending;
    };

} // namespace eudaq

#endif // EUDAQ_INCLUDED_ExampleHardware
