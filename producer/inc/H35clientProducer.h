/****************************************************************************
** Created by Stefano Terzo (stefano.terzo@cern.ch) on 22/08/16
**
** Institut de Física d'Altes Energies (IFAE)
** Edifici CN UAB Campus
** 08193 Bellaterra (Barcelona), Spain
**
****************************************************************************/

#ifndef H35CLIENTPRODUCER_H35CLIENTPRODUCER_H
#define H35CLIENTPRODUCER_H35CLIENTPRODUCER_H

#include "eudaq/Configuration.hh"
#include "eudaq/Producer.hh"
#include "eudaq/Logger.hh"
#include "eudaq/RawDataEvent.hh"
#include "eudaq/Timer.hh"
#include "eudaq/Utils.hh"
#include "eudaq/OptionParser.hh"
#include "H35clientHardware.hh"
#include "Client.h"
#include <iostream>
#include <ostream>
#include <vector>

#ifdef _DEBUG
#define DEBUG_OUT(x)  std::cout<<x<<std::endl
#else
#define DEBUG_OUT(x)
#endif


// Declare a new class that inherits from eudaq::Producer
class H35clientProducer : public eudaq::Producer {
public:
    enum status_enum {
        unconfigured,
        configured,
        starting,
        started,
        stopping,
        stopped,
        doTerminat
    };

    // The constructor must call the eudaq::Producer constructor with the name
    // and the runcontrol connection string, and initialize any member variables.
    H35clientProducer(const std::string &name, const std::string &runcontrol);

    // This gets called whenever the DAQ is configured
    virtual void OnConfigure(const eudaq::Configuration &config);

    // This gets called whenever a new run is started
    // It receives the new run number as a parameter
    virtual void OnStartRun(unsigned param);

    // This gets called whenever a run is stopped
    virtual void OnStopRun();

    // This gets called when the Run Control is terminating,
    // we should also exit.
    virtual void OnTerminate();

    bool makeAndSendEvents();

    // This is just an example, adapt it to your hardware
    void ReadoutLoop();

private:
    // This is just a dummy class representing the hardware
    // It here basically that the example code will compile
    // but it also generates example raw data to help illustrate the decoder
    eudaq::H35clientHardware *hardware;
    unsigned m_run, m_ev, m_exampleparam, m_send_bore_delay, m_ID, m_maxEventNR, m_Skip;

    std::string m_address, m_confFileName;
    int m_port, m_clocks, m_buffer;

    bool m_TLU, m_stopScan;
    status_enum m_stat = unconfigured;

    Client *m_client;

    static void startScan(void* arg);
    void acquire();

};


#endif //H35CLIENTPRODUCER_H35CLIENTPRODUCER_H
