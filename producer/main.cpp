/****************************************************************************
** Created by Stefano Terzo (stefano.terzo@cern.ch)
**
** Institut de Física d'Altes Energies (IFAE)
** Edifici CN UAB Campus
** 08193 Bellaterra (Barcelona), Spain
**
****************************************************************************/

#include <iostream>

#include "H35clientProducer.h"

#include "eudaq/Configuration.hh"
#include "eudaq/Producer.hh"
#include "eudaq/Logger.hh"
#include "eudaq/RawDataEvent.hh"
#include "eudaq/Timer.hh"
#include "eudaq/Utils.hh"
#include "eudaq/OptionParser.hh"
#include "H35clientHardware.hh"

using namespace std;

// The main function that will create a Producer instance and run it
int main(int /*argc*/, const char ** argv) {
    // You can use the OptionParser to get command-line arguments
    // then they will automatically be described in the help (-h) option
    eudaq::OptionParser op("EUDAQ Example Producer", "1.0",
                           "Just an example, modify it to suit your own needs");
    eudaq::Option<std::string> rctrl(op, "r", "runcontrol", "tcp://localhost:44000", "address", "The address of the RunControl.");
    eudaq::Option<std::string> level(op, "l", "log-level", "NONE", "level", "The minimum level for displaying log messages locally");
    eudaq::Option<std::string> name(op, "n", "name", "H35clientProducer", "string", "The name of this Producer");

    try {
        // This will look through the command-line arguments and set the options
        op.Parse(argv);
        // Set the Log level for displaying messages based on command-line
        EUDAQ_LOG_LEVEL(level.Value());
        std::cout << "Producer name = \"" << name.Value() << "\" connected to " << rctrl.Value() <<
        std::endl;
        if (name.IsSet()) {
            eudaq::mSleep(1000);
        }
        eudaq::mSleep(3000);
        // Create a producer
        H35clientProducer producer(name.Value(), rctrl.Value());
        // And set it running...
        producer.ReadoutLoop();
        // When the readout loop terminates, it is time to go
        std::cout << "Quitting" << std::endl;
    }
    catch (...) {
        // This does some basic error handling of common exceptions
        return op.HandleMainException();
    }
    return 0;
}