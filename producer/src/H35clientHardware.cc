#include "H35clientHardware.hh"
#include "eudaq/Utils.hh"

namespace eudaq {

  namespace {

      std::vector<unsigned char> MakeRawEvent(unsigned width, unsigned height) {
        std::vector<unsigned char> result(width * height * 2);
        size_t offset = 0;
        for (unsigned y = 0; y < height; ++y) {
          for (unsigned x = 0; x < width; ++x) {
            short charge = 0;
            setlittleendian(&result[offset], charge);
            offset += 2;
          }
        }
        return result;
      }

      std::vector<unsigned char>
      ZeroSuppressEvent(const std::vector<unsigned char> &data, unsigned width,
                        int threshold = 10) {
        unsigned height = data.size() / width / 2;
        std::vector<unsigned char> result;
        size_t inoffset = 0, outoffset = 0;
        for (unsigned y = 0; y < height; ++y) {
          for (unsigned x = 0; x < width; ++x) {
            short charge = getlittleendian<short>(&data[inoffset]);
            if (charge > threshold) {
              result.resize(outoffset + 6);
              setlittleendian<unsigned short>(&result[outoffset + 0], x);
              setlittleendian<unsigned short>(&result[outoffset + 2], y);
              setlittleendian<short>(&result[outoffset + 4], charge);
              outoffset += 6;
            }
            inoffset += 2;
          }
        }
        return result;
      }
  }

    H35clientHardware::H35clientHardware(Client *client)
      : m_numsensors(1), m_triggerid(0), m_client(client) {}

  void H35clientHardware::Setup(int) {
  }

  void H35clientHardware::PrepareForRun(int buffer, int clocks) {
      //m_timer.Restart();
      m_nextevent = 10.0;
      m_events_pending = true;
      m_triggerid = 0;

      m_buffer = buffer;

      Client::opcode byte = 0xb0;
      m_client->sendMessage(byte);
      m_client->readMessage(byte);
      std::stringstream msg;
      msg << std::bitset<32>(buffer).to_string() << std::bitset<32>(clocks).to_string();
      std::bitset<64> code = std::stoul(msg.str(), nullptr, 2);
      m_client->sendMessage(code);
  }

  bool H35clientHardware::EventsPending() const {
    //return m_timer.Seconds() > m_nextevent;
    //  return m_events_pending;
      return true;
  }

  unsigned H35clientHardware::NumSensors() const { return m_numsensors; }

  std::vector<unsigned char> H35clientHardware::ReadSensor(int sensorid) {
      std::vector<unsigned char> result;
      int32_t data[m_buffer];
      std::cout << "waiting for data...\n";
      m_client->readMessage(data, sizeof(int32_t) * m_buffer);
      std::cout << "...data received!\n";
      int row, col, timestamp;
      for (int i = 0; i < m_buffer; i++) {
          bool notimeout = m_client->decodeData(data[i], col, row, timestamp);
          if (notimeout) {
              std::cout << "col: " << col << " row: " << row << " timestamp: " << timestamp << "\n";
              result.push_back(col);
              result.push_back(row);
              result.push_back(timestamp);
              //result.push_back(data[i]);
          }
          else {
              std::cout << "TIMEOUT\n";
              m_events_pending = false;
          }
      }
      return result;
  }

  void H35clientHardware::CompletedEvent() {
    m_triggerid++;
    m_nextevent += 1.0;
  }

} // namespace eudaq
