/****************************************************************************
** Created by Stefano Terzo (stefano.terzo@cern.ch)
**
** Institut de Física d'Altes Energies (IFAE)
** Edifici CN UAB Campus
** 08193 Bellaterra (Barcelona), Spain
**
****************************************************************************/

#include "H35clientProducer.h"
#include <omnithread.h>


void H35clientProducer::startScan(void* arg) {
    H35clientProducer *producer = (H35clientProducer *) arg;
    producer->acquire();
}

void H35clientProducer::acquire() {
    m_stopScan = false;
    while (!m_stopScan) {
        bool evMade = this->makeAndSendEvents();
        if (evMade) {
            std::cout << "continue loop\n";
            m_client->sendMessage(std::bitset<32>(0));
        }
        else {
            std::cout << "exiting loop\n";
            break;
        }
    }

    m_client->sendMessage(std::bitset<32>(1));
    unsigned char byte = 0x0;
    //client_->sendMessage(byte);
    while (byte != 0xb0) {
        m_client->readMessage(byte);
        std::cout << std::hex << (int) byte << "\n";
    }
    //m_client->sendMessage(std::bitset<32>(1));
}


H35clientProducer::H35clientProducer(const std::string &name, const std::string &runcontrol)
        : eudaq::Producer(name, runcontrol), m_run(0), m_ev(0) {
    m_client = new Client();
    hardware = new eudaq::H35clientHardware(m_client);
}

  // This gets called whenever the DAQ is configured
  void H35clientProducer::OnConfigure(const eudaq::Configuration & config) {
      std::cout << "Configuring: " << config.Name() << std::endl;
      // Do any configuration of the hardware here
      config.Print();
      try {
          // Configuration file values are accessible as config.Get(name, default)
          m_address = config.Get("Address", "");
          if (m_address == "") throw eudaq::Exception("Address not defined");
          m_port = config.Get("Port", 0);
          if (m_port == -1) throw eudaq::Exception("Port not defined");
          m_clocks = config.Get("Clocks", -1);
          if (m_clocks == -1) throw eudaq::Exception("Clocks not defined");
          m_buffer = config.Get("Buffer", -1);
          if (m_buffer == -1) throw eudaq::Exception("Buffer not defined");

          m_confFileName = config.Get("Config", "");
          if (m_confFileName == "") throw eudaq::Exception("Config not defined");

          //default option in the example
          m_send_bore_delay = config.Get("boreDelay", 0);

          //connect the client
          bool created = m_client->createSocket();
          if (!created) throw eudaq::Exception("Failed to create socket");
          bool connected = m_client->connectSocket(m_address.c_str(), m_port);
          if (!connected) {
              m_client->closeSocket();
              std::cout << "Failed to connect to: " << m_address << " - port: " << m_port << std::endl;
              throw eudaq::Exception("Failed to connect");
          }

          bool loaded = m_client->loadConfig(m_confFileName);
          if (!loaded) throw eudaq::Exception("Config file not found");

          unsigned int ch = m_client->initExtBias();
          if (ch < 255) throw eudaq::Exception("Failed to initialise DAC7568");

          Client::nmoscode code = m_client->getNMOScode();
          m_client->setEnInj(0,0,1,code);
          m_client->setNMOScode(code);

          std::string msg;
          unsigned int matrix = m_client->configure();
          if (matrix == 0) throw eudaq::Exception("Failed to configure");
          else if (matrix == 1) {
              msg = "NMOS ";
          }
          else if (matrix == 2) {
              msg = "CMOS ";
          }
          else if (matrix == 3) {
              msg = "NMOS and CMOS ";
          }

      m_maxEventNR = config.Get("numberOfEvents", 200);
      m_ID = config.Get("ID", 0);
      m_Skip = config.Get("skip", 0);
      std::cout << "m_maxEventNR = " << m_maxEventNR << std::endl;
//      hardware.Setup(m_exampleparam);
      m_TLU = config.Get("TLU", 0);

          // At the end, set the status that will be displayed in the Run Control.
          SetStatus(eudaq::Status::LVL_OK, msg + "Configured (" + config.Name() + ")");
          m_stat = configured;
          std::cout << "...Configured" << std::endl;

      } catch (const std::exception &e) {
          std::cout << "Caught exception: " << e.what() << "\n";
          SetStatus(eudaq::Status::LVL_ERROR, "Configuration Error");
      } catch (...) {
          printf("Unknown exception\n");
          SetStatus(eudaq::Status::LVL_ERROR, "Configuration Error");
      }
  }

  // This gets called whenever a new run is started
  // It receives the new run number as a parameter
  void H35clientProducer::OnStartRun(unsigned param) {
      std::cout << "Start Run: " << m_run << std::endl;
      m_stat = starting;
      m_run = param;
      m_ev = 0;
      eudaq::mSleep(m_send_bore_delay);

      // It must send a BORE to the Data Collector
      eudaq::RawDataEvent bore(eudaq::RawDataEvent::BORE(EVENT_TYPE, m_run));
      // You can set tags on the BORE that will be saved in the data file
      // and can be used later to help decoding
      bore.SetTag("TLU", m_TLU);
      bore.SetTag("ID", m_ID);
      bore.SetTimeStampToNow();
      // Send the event to the Data Collector
      SendEvent(bore);

      hardware->PrepareForRun(m_buffer,m_clocks);
      omni_thread::create(startScan,(void*)this);

      // At the end, set the status that will be displayed in the Run Control.
      m_stat = started;
      SetStatus(eudaq::Status::LVL_OK, "Running");
      std::cout << "... Started" << std::endl;


  }

  // This gets called whenever a run is stopped
  void H35clientProducer::OnStopRun() {
      std::cout << "Stopping Run" << std::endl;
      m_stat = stopping;

      m_stopScan = true;

      // wait until all events have been read out from the hardware
//      while (m_stat == stopping) {
//          eudaq::mSleep(20);
//      }
      eudaq::mSleep(20);

//      hardware->Stop();

      // Send an EORE after all the real events have been sent
      // You can also set tags on it (as with the BORE) if necessary
      auto EOREvent = eudaq::RawDataEvent::EORE(EVENT_TYPE, m_run, ++m_ev);
      EOREvent.SetTag("TLU", m_TLU);
      EOREvent.SetTag("ID", m_ID);
      EOREvent.SetTimeStampToNow();
      SendEvent(EOREvent);
      m_stat = configured;
      SetStatus(eudaq::Status::LVL_OK, "Run Stopped");
      std::cout << "Stopped \n " << m_ev << " Events send" << std::endl;
  }

  // This gets called when the Run Control is terminating,
  // we should also exit.
  void H35clientProducer::OnTerminate() {
      std::cout << "Terminating..." << std::endl;
      m_client->sendMessage('Q');
      m_client->closeSocket();
      delete m_client;
      delete hardware;
      m_stat = doTerminat;
      exit(0);
  }

  bool H35clientProducer::makeAndSendEvents() {
      if (!hardware->EventsPending()) {
          std::cout << "no events pending\n";
          return false;
      }
      if (m_maxEventNR < m_ev) {
          std::cout << "max event reached\n";
          eudaq::mSleep(1000);
          return false;
      }
      ++m_ev;
      eudaq::RawDataEvent ev(EVENT_TYPE, m_run, m_ev);
      ev.SetTag("TLU", m_TLU);
      ev.SetTag("ID", m_ID);

      for (unsigned plane = 0; plane < hardware->NumSensors(); ++plane) {
          // Read out a block of raw data from the hardware
          std::vector<unsigned char> buffer = hardware->ReadSensor(plane);
          // Each data block has an ID that is used for ordering the planes later
          // If there are multiple sensors, they should be numbered incrementally

          // Add the block of raw data to the event
          ev.AddBlock(plane, buffer);
      }
      ev.SetTimeStampToNow();
      hardware->CompletedEvent();
      // Send the event to the Data Collector
      if (m_ev % 10 == 0) {
          std::cout << "sending Event: " << m_ev << std::endl;
      }
      if (m_Skip != 0) {
          if (m_ev % m_Skip == 0) {
              return false;
          }
      }
      SendEvent(ev);

      std::cout << "Reconversion:" << "\n";
      for(size_t i=0; i<ev.NumBlocks(); ++i){
          eudaq::RawDataEvent::data_t data = ev.GetBlock(i);
          //m_client->decodeData(data,col,row,ts);
          for(int j=0; j<data.size(); j+=3) {
              int col, row, ts;
              col = data[j+0];
              row = data[j+1];
              ts = data[j+2];
              std::cout << "col: " << col << " row: " << row << " ts: " << ts << "\n";
          }
      }
      return true;
  }
  // This is just an example, adapt it to your hardware
  void H35clientProducer::ReadoutLoop() {
      // Loop until Run Control tells us to terminate
      while (m_stat != doTerminat) {

      }
  }

