/****************************************************************************
** Created by Stefano Terzo (stefano.terzo@cern.ch)  on 04/10/16
**
** Institut de Física d'Altes Energies (IFAE)
** Edifici CN UAB Campus
** 08193 Bellaterra (Barcelona), Spain
**
****************************************************************************/

#include <VoltageSource.h>

int VoltageSource::setVoltage(double V) {
    std::stringstream ss;
    if(this->getName().find("KEITHLEY 2280S")!=-1) {
        ss << ":SOUR:VOLT " << std::dec << V;
        std::string str = ss.str();
        this->write(str);
    }
    return 1;
}

void VoltageSource::setOutput(bool on) {
    std::string msg;
    if(this->getName().find("KEITHLEY 2280S")!=-1) {
        if(on) msg = ":OUTP ON";
        else msg = ":OUTP OFF";
        this->write(msg);
    }
}

double VoltageSource::readVoltage() {
    std::string msg;
    std::stringstream ss;
    double volt;
    if(this->getName().find("KEITHLEY 2280S")!=-1) {
        msg = ":MEAS:VOLT?";
        this->write(msg);
        this->read(msg);
        std::cout << msg << std::endl;
        ss << msg.substr(msg.find(",")+1,13);
        std::cout << ss.str() << std::endl;
        ss >> volt;
    }
    return volt;
}

double VoltageSource::readCurrent() {
    std::string msg;
    std::stringstream ss;
    double current = 0;
    if(this->getName().find("KEITHLEY 2280S")!=-1) {
        //msg = ":MEAS:CURR? 0.01,6";
        msg = ":MEAS:CURR?";
        this->write(msg);
        this->read(msg);
        std::cout << msg << std::endl;
        ss << msg.substr(msg.find(",")+1,13);
        std::cout << ss.str() << std::endl;
        ss >> current;
    }
    return current;
}

//double VoltageSource::readCurrent() {
//    std::string msg;
//    if(this->getName().find("KEITHLEY 2280S")!=-1) {
//        msg = ":FORM:ELEM \"READ,SOUR\"";
//        this->write(msg);
//        msg = ":FETC?";
//        this->write(msg);
//        this->read(msg);
//        std::cout << msg << std::endl;
//    }
//    return 0;
//}

int VoltageSource::setCurrentRange(double range, int resolution) {
    std::stringstream ss;
    if(this->getName().find("KEITHLEY 2280S")!=-1) {
        ss << ":CONF:CURR " << std::dec << range << "," << resolution;
    }
    std::string str = ss.str();
    return this->write(str);
}

int VoltageSource::setCurrentLimit(double Ilim) {
    std::stringstream ss;
    if(this->getName().find("KEITHLEY 2280S")!=-1) {
        ss << ":CURR " << std::dec << Ilim;
    }
    std::string str = ss.str();
    return this->write(str);
}