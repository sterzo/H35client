/****************************************************************************
** Created by Stefano Terzo (stefano.terzo@cern.ch)
**
** Institut de Física d'Altes Energies (IFAE)
** Edifici CN UAB Campus
** 08193 Bellaterra (Barcelona), Spain
**
****************************************************************************/

#include <Socket.h>

#include "string.h"
#include <string.h>
#include <errno.h>
#include <fcntl.h>

bool Socket::connectTCP(std::string address, const int port) {

    //this->setSocketBlocking(false);

    memset ( &serv_addr_, 0, sizeof ( serv_addr_ ) );

    if ( ! is_valid() ) return false;

    serv_addr_.sin_family = AF_INET;
    serv_addr_.sin_port = htons ( port );

    int status = inet_pton( AF_INET, address.c_str(), &serv_addr_.sin_addr );

    if ( errno == EAFNOSUPPORT ) return false;

    status = ::connect ( sockfd_, ( sockaddr * ) &serv_addr_, sizeof ( serv_addr_ ) );

    connected_ = status == 0;

    //this->setSocketBlocking(true);

    return connected_;
}

void Socket::connectSocket2(std::string address, int port) {
    if ( ! Socket::createSocket() )
    {
        throw std::string( "Could not create client socket." );
    }

    if (!Socket::connectTCP(address, port)) {
        throw std::string("Could not bind to port.");
    }
}

bool Socket::sendMessage(std::string message, int buffer) const{

    int status = ::send(sockfd_, message.c_str(), message.size(), NO_SIGNAL_PIPE);

    if (status == -1) {
        return false;
    }
    else {
        return true;
    }
}

int Socket::recv(std::string &message, unsigned int buffer_len) const {

    char buf[buffer_len + 1];

    message = "";

    memset(buf, 0, buffer_len + 1);

    int status = ::recv(sockfd_, buf, buffer_len, 0);


    if (status == -1) {
        std::cout << "status == -1   errno == " << errno << "  in Socket::recv\n";
        return 0;
    }
    else if (status == 0) {
        return 0;
    }
    else {
        message = buf;
        return status;
    }
}

int Socket::recv(std::string &message, unsigned int buffer_len, unsigned int timeout_sec) const {
    struct timeval tv;
    tv.tv_sec = timeout_sec;  /* 30 Secs Timeout */
    tv.tv_usec = 0;  // Not init'ing this can cause strange errors
    setsockopt(sockfd_, SOL_SOCKET, SO_RCVTIMEO, (const char*)&tv,sizeof(struct timeval));

    return recv(message,buffer_len);
}

bool Socket::createSocket() {
    sockfd_ = socket(AF_INET, SOCK_STREAM, 0);

    created_ = true;

    if (!is_valid())
        return false;

//    struct timeval tv;
//    tv.tv_sec = 2;
//    setsockopt(sockfd_, SOL_SOCKET, SO_RCVTIMEO,(struct timeval *)&tv,sizeof(struct timeval));

    // TIME_WAIT - argh
    int on = 1;

    return setsockopt(sockfd_, SOL_SOCKET, SO_REUSEADDR, (const char *) &on, sizeof(on)) != -1;
}

Socket::Socket() :
        sockfd_( -1 ) {
    connected_ = false;
    created_ = false;
}


Socket::~Socket() {
    if (created_) this->closeSocket();
}

void Socket::closeSocket(){
    if ( created_ && is_valid() ) ::close ( sockfd_ );
}


/** Returns true on success, or false if there was an error */
bool Socket::setSocketBlocking(bool blocking)
{
    if (sockfd_ < 0) return false;

    int flags = fcntl(sockfd_, F_GETFL, 0);
    if (flags < 0) return false;
    flags = blocking ? (flags&~O_NONBLOCK) : (flags|O_NONBLOCK);

    return fcntl(sockfd_, F_SETFL, flags) == 0;
}

bool Socket::bind( const int port ) {

    if (!is_valid()) {
        return false;
    }


    serv_addr_.sin_family = AF_INET;
    serv_addr_.sin_addr.s_addr = INADDR_ANY;
    serv_addr_.sin_port = htons (port);

    int bind_return = ::bind(sockfd_, (struct sockaddr *) &serv_addr_, sizeof(serv_addr_));

/*
    if(errno == EADDRINUSE) {
        printf("the port is not available. already to other process\n");
        return false;
    } else {
        printf("could not bind to process (%d) %s\n", errno, strerror(errno));
        return false;
    }
*/
    if (bind_return == -1) {
        return false;
    }

    return true;
}

int Socket::find_port(std::string address,int min,int max){

    for (int port = min; port < max; port++) {
        //std::cout << "Tring port: " << port << std::endl;
        bool error = false;
        try {
            connectSocket2(address, port);
        }
        catch (std::string s) {
            //std::cout << "Exception was caught: " << s << "\n";
            error = true;
            this->closeSocket();
        }

        if (!error) {
            //std::cout << "\t-->Found port: " << port << std::endl;
            //this->setSocketBlocking(true);
            return port;
        }
    }
    return 0;
}
