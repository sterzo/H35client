/****************************************************************************
** Created by Stefano Terzo (stefano.terzo@cern.ch)  on 04/10/16
**
** Institut de Física d'Altes Energies (IFAE)
** Edifici CN UAB Campus
** 08193 Bellaterra (Barcelona), Spain
**
****************************************************************************/

#include "ClimateChamber.h"

std::vector<std::string> ClimateChamber::readStatus() {
    std::string reply;
    this->sendMessage("$01I\r\n",100);
    std::cout << "reading command" << std::endl;
    if(this->read(reply)==Status::connected) {
        //std::cout << reply << std::endl;
    }
    else{
        std::cout << "Error reading interface\n";
        std::vector<std::string> tmp = {"0","0"};
        return tmp;
    }

    std::istringstream iss(reply);
    std::vector<std::string> tokens;

    //copy(std::istream_iterator<std::string>(iss),std::istream_iterator<std::string>(),std::back_inserter(tokens));


    std::string tmp;
    while (std::getline(iss, tmp, ' ')) {
        tokens.push_back(tmp);
    }

    std::vector<std::string> labels = {
            "Temp Nominal:\t\t","Temp Actual:\t\t",
            "Humidity Nominal:\t","Humidity Actual:\t",
            "Pt100_1 Nominal:\t","Pt100_1 Actual:\t\t",
            "Pt100_2 Nominal:\t","Pt100_2 Actual:\t\t",
            "Pt100_3 Nominal:\t","Pt100_3 Actual:\t\t",
            "Pt100_4 Nominal:\t","Pt100_4 Actual:\t\t",
            "Fan t.spa. Nominal:\t","Fan t.spa. Actual:\t",
    };

    for(unsigned int i=0; i<tokens.size()-1; i++){
        if(i==labels.size()) break;
        std::cout << labels[i] << tokens[i] << "\n";
    }

    std::vector<std::string> switches = {
            "output\ton:\t\t",
            "output\tstart:\t\t",
            "input\tAL-ERROR:\t",
            "output\tTemper:\t\t",
            "output\thumidity:\t",
            "output\tdew point:\t",
            "output\tcap.humid:\t",
            "output\tdehumidit:\t",
            "output\tadd.dehum:\t",
            "output\tcomp.air:\t",
            "output\tcorr. gas.:\t",
            "output\tCO2 cool:\t",
            "output\tfree out1:\t","output\tfree out2:\t",
            "output\tfree out3:\t","output\tfree out4:\t",
            "input\tfree in1:\t","input\tfree in2:\t",
            "input\tfree in3:\t","input\tfree in4:\t",
            "output\tAdjTempHi:\t","output\tAdjTempUn:\t","output\tAdjTempSp:\t",
            "output\tAdjTempPHi:\t","output\tAdjTempPUn:\t","output\tAdjTempPSp:\t"
    };

    for(unsigned int i=0; i<tokens[tokens.size()-1].size(); ++i){
        if(i==switches.size()) break;
        std::cout << switches[i] << tokens[tokens.size()-1][i] << std::endl;
    }

    return tokens;
}

bool ClimateChamber::setTemperature(double temp) {
    std::stringstream ss_;
    ss_ << temp;
    std::string setCmd = "$01E " + ss_.str() + " 0000.0 0000.0 0000.0 0000.0 0000.0 0000.0 0101000000000000000000000000000\r\n";
    this->sendMessage(setCmd,100);
    std::string reply;
    if(this->read(reply)==Status::connected) {
        std::cout << "Chamber reply: " << reply << std::endl;
    }
    else{
        std::cout << "Error reading interface\n";
        return false;
    }

    return reply=="0";
}



