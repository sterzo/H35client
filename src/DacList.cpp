/****************************************************************************
** Created by Stefano Terzo (stefano.terzo@cern.ch) on 24/03/17
**
** Institut de Física d'Altes Energies (IFAE)
** Edifici CN UAB Campus
** 08193 Bellaterra (Barcelona), Spain
**
****************************************************************************/

#include "DacList.h"

DacList::DacList(const std::vector<unsigned int> defaultInternalDacs, QWidget *parent) : QWidget(parent) {
    QGridLayout *daclist_gridLayout = new QGridLayout(this);
    std::vector<QString> labels = {"VN(P)HBdig", "VPDel", "VNDel", "VPTrim", "VNComp", "BLR", "BLRpix", "VNBiasPix", "VNFBPix", "VPTrimPix",
                                   "VNTwDownPix", "VNTwPix", "VNLogicPix", "VPLoadPix", "VNSFPix","VNPix","VPABPix"};

    std::vector<bool> defaultsIO = {1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0};
    for (int i = 0; i < labels.size(); i++) {
        QLabel *label = new QLabel(labels[i], this);
        daclist_gridLayout->addWidget(label, i, 0, Qt::AlignLeft);
        QSpinBox *spinBox = new QSpinBox(this);
        spinBox->setButtonSymbols(QAbstractSpinBox::NoButtons);
        spinBox->setMaximum(0x3f);
        spinBox->setFixedSize(40,24);
        spinBox->setValue(defaultInternalDacs[i]);
        internalDacSpinBoxes_.push_back(spinBox);
        daclist_gridLayout->addWidget(spinBox,i,1,Qt::AlignLeft);
        connect(spinBox, SIGNAL(valueChanged(int)), this, SLOT(emitValueChanged()));

        QCheckBox *checkBox = new QCheckBox(this);
        checkBox->setChecked(defaultsIO[i]);
        checkBox->hide();
        internalDacCheckBoxes_.push_back(checkBox);
//daclist_gridLayout->addWidget(checkBox,1,i+1,Qt::AlignCenter);
//connect(checkBox, SIGNAL(stateChanged(int)), this, SLOT(emitValueChanged()));
    }
//    labels = {"nVPlusTwPix", "V_A", "nThTwPix", "V_B", "nVGatePix", "V_C", "nThPix", "V_D", "nBLPix", "V_E", "nTh", "V_F", "nBL", "V_G", "CASCGND", "V_H"};
//    QSpinBox *spinBox;
//    QDoubleSpinBox *doubleSpinBox;
//    for (int i = 0; i < labels.size(); i++) {
//        QLabel *label = new QLabel(labels[i], this);
//        daclist_gridLayout->addWidget(label, 2, i, Qt::AlignCenter);
//        if(i%2==0) {
//            spinBox = new QSpinBox(this);
//            spinBox->setButtonSymbols(QAbstractSpinBox::NoButtons);
//            spinBox->setMaximum(4095);
//            spinBox->setFixedSize(50,24);
//            externalDacSpinBoxes_.push_back(spinBox);
//            daclist_gridLayout->addWidget(spinBox,3,i,Qt::AlignCenter);
//        }
//        else {
//            doubleSpinBox = new QDoubleSpinBox(this);
//            doubleSpinBox->setButtonSymbols(QAbstractSpinBox::NoButtons);
//            doubleSpinBox->setSuffix(" V");
//            doubleSpinBox->setDecimals(2);
//            doubleSpinBox->setFixedSize(50,24);
//            doubleSpinBox->setMaximum(5.00);
//            daclist_gridLayout->addWidget(doubleSpinBox,3,i,Qt::AlignCenter);
//            QObject::connect(spinBox, static_cast<void (QSpinBox::*)(int)>(&QSpinBox::valueChanged), [=](int value){doubleSpinBox->setValue( value * 5. / 4095.);});
////QObject::connect(doubleSpinBox, static_cast<void (QDoubleSpinBox::*)(double)>(&QDoubleSpinBox::valueChanged), [=](double value){spinBox->setValue( value * 4095 / 5);});
//            QObject::connect(doubleSpinBox, &QDoubleSpinBox::editingFinished, [=](void){spinBox->setValue( doubleSpinBox->value() * 4095 / 5);});
//            spinBox->setValue(defaultExternalDacs_[i/2]);
//            connect(spinBox, SIGNAL(valueChanged(int)), this, SLOT(emitValueChanged()));
//        }
//
//    }
    this->setLayout(daclist_gridLayout);
    daclist_gridLayout->setVerticalSpacing(0);
}


