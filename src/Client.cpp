/****************************************************************************
** Created by Stefano Terzo (stefano.terzo@cern.ch) on 21/06/16
**
** Institut de Física d'Altes Energies (IFAE)
** Edifici CN UAB Campus
** 08193 Bellaterra (Barcelona), Spain
**
****************************************************************************/

#include "Client.h"
#include <math.h>

uint32_t gray_decode(uint32_t g)
{
    for (uint32_t bit = 1U << 31; bit > 1; bit >>= 1)
    {
        if (g & bit) g ^= bit >> 1;
    }
    return g;
}

//int gray_decode(int n) {
//    int p = n;
//    while (n >>= 1) p ^= n;
//    return p;
//}


void Client::setNMOScode(nmoscode code) {
    code_[Matrix::NMOS] = code;
}

void Client::setCMOScode(nmoscode code) {
    code_[Matrix::CMOS] = code;
}

void Client::setANA1code(nmoscode code) {
    code_[Matrix::analog1] = code;
}

void Client::setANA2code(nmoscode code) {
    code_[Matrix::analog2] = code;
}

void Client::setCode(Matrix matrix, nmoscode code) {
    code_[matrix] = code;
}

void Client::setLoadDAC(int row, bool enable, nmoscode &code){
    //SET ENABLE BIT ROW
    int rbit = 1280+(MAX_ROWS-row-1)*4;
    if(rbit < code.size()) code.set(rbit,enable);
    //std::cout << nmoscode_ << "\n";
}

void Client::setEnHitbus(int roc, bool enable, nmoscode &code){
    //SET ENABLE LOAD DAC CELL BIT
    int rbit = 0+(MAX_ROCS-roc-1)*2;
    if(rbit < code.size()) code.set(rbit,enable);
    //std::cout << nmoscode_ << "\n";
}

void Client::setLoadDACCell(int roc, bool enable, nmoscode &code){
    //SET ENABLE LOAD DAC CELL BIT
    int rbit = 1+(MAX_ROCS-roc-1)*2;
    if(rbit < code.size()) code.set(rbit,enable);
    //std::cout << nmoscode_ << "\n";
}

void Client::setAnaThCol(int col, short val, nmoscode &code) {
    this->setAnaThCol(col,std::bitset<2>(val),code);
}

void Client::setAnaThCol(int col, std::bitset<2> val, nmoscode &code){
    //SET PIXEL ANALOG TDAC NMOS CODE
    for(int b=0; b<val.size(); ++b) {
        int cbit = 80 + b + (MAX_COLS - col - 1) * 4;
        if(cbit < code.size()) code.set(cbit,val[b]);
    }
    //std::cout << code << "\n";
}

void Client::setDigThCol(int roc, short val, nmoscode &code){
    this->setDigThCol(roc,std::bitset<3>(val),code);
}

void Client::setDigThCol(int roc_col, std::bitset<3> val, nmoscode &code) {
    //int hbR      = 80 + (MAX_ROC_COLS - roc_col - 1)/2*20;
    int enDiscR = 81 + (MAX_ROC_COLS - roc_col - 1) / 2 * 20;
    int dac1r = 84 + (MAX_ROC_COLS - roc_col - 1) / 2 * 20;
    int dac0r = 85 + (MAX_ROC_COLS - roc_col - 1) / 2 * 20;
    int hbL      = 88 + (MAX_ROC_COLS - roc_col - 2)/2*20;
    int dac2r = 89 + (MAX_ROC_COLS - roc_col - 1) / 2 * 20;
    int dac0l = 92 + (MAX_ROC_COLS - roc_col - 2) / 2 * 20;
    int enDiscL = 93 + (MAX_ROC_COLS - roc_col - 2) / 2 * 20;
    int dac2l = 96 + (MAX_ROC_COLS - roc_col - 2) / 2 * 20;
    int dac1l = 97 + (MAX_ROC_COLS - roc_col - 2) / 2 * 20;

    if (roc_col<150) {
        int cbit[3];
        if (roc_col % 2 == 0) {
            cbit[0] = hbL;//dac0l;
            cbit[1] = dac1l;
            cbit[2] = dac2l;
            //val.set(0);
        }
        else {
            cbit[0] = dac0r;
            cbit[1] = dac1r;
            cbit[2] = dac2r;
            //val.set(0);
        }
        for (int b = 0; b < val.size(); ++b) {
            //int cbit = 81 + b + (b+roc_col%2)/2*2 + (MAX_ROC_COLS - roc_col)/2*11 + (MAX_ROC_COLS - roc_col - 1)/2*9;
            //std::cout << "BIT: " << cbit << " - val " << val[b] << std::endl;
            if (cbit[b] < code.size()) code.set(cbit[b], val[b]);
        }
    }
    else{
        int cbit[2];
        if (roc_col % 2 == 0) {
            cbit[0] = dac2l;
            cbit[1] = dac1l;
            //val.set(0);
        }
        else {
            cbit[0] = dac2r;
            cbit[1] = dac1r;
            //val.set(0);
        }
        for (int b = 0; b < 2; ++b) {
            //int cbit = 81 + b + (b+roc_col%2)/2*2 + (MAX_ROC_COLS - roc_col)/2*11 + (MAX_ROC_COLS - roc_col - 1)/2*9;
            //std::cout << "BIT: " << cbit << " - val " << val[b] << std::endl;
            if (cbit[b] < code.size()) code.set(cbit[b], val[b]);
        }
    }
    //std::cout << code << "\n";

}

void Client::setEnDigCol(int roc_col, bool enable, nmoscode &code) {
    int hbR      = 80 + (MAX_ROC_COLS - roc_col - 1)/2*20;
    int enDiscR  = 81 + (MAX_ROC_COLS - roc_col - 1)/2*20;
    int enDiscL  = 93 + (MAX_ROC_COLS - roc_col - 2)/2*20;
    int dac0r = 85 + (MAX_ROC_COLS - roc_col - 1) / 2 * 20;
    int dac0l = 92 + (MAX_ROC_COLS - roc_col - 2) / 2 * 20;
    int dac2l = 96 + (MAX_ROC_COLS - roc_col - 2) / 2 * 20;
    int dac2r = 89 + (MAX_ROC_COLS - roc_col - 1) / 2 * 20;
    int hbL      = 88 + (MAX_ROC_COLS - roc_col - 2)/2*20;

    int cbit;
    if(roc_col%2==0) cbit = dac0l;//enDiscL;
    else cbit = hbR;//enDiscR;

    if(cbit < code.size()) code.set(cbit,enable);
}

void Client::setEnHitbusCol(int roc_col, bool enable, nmoscode &code){
    //SET PIXEL ANALOG TDAC NMOS CODE
    //int cbit = 78 + (1+roc_col%2)/2*2 + (MAX_ROC_COLS - roc_col)/2*11 + (MAX_ROC_COLS - roc_col - 1)/2*9;
    int hbR      = 80 + (MAX_ROC_COLS - roc_col - 1)/2*20;
    int hbL      = 88 + (MAX_ROC_COLS - roc_col - 2)/2*20;
    int enDiscR  = 81 + (MAX_ROC_COLS - roc_col - 1)/2*20;
    int enDiscL  = 93 + (MAX_ROC_COLS - roc_col - 2)/2*20;


    int cbit;
    if(roc_col%2==0) cbit = enDiscL;//hbL;
    else cbit = enDiscR;//hbR;

    if(cbit < code.size()) code.set(cbit,enable);
    //std::cout << code << "\n";
}

void Client::setEnInj(int col, int row, bool enable, nmoscode &code){
    setEnInjCol(col, enable, code);
    setEnInjRow(row, enable, code);
}

void Client::setEnInjCol(int col, bool enable, nmoscode &code){
    //SET PIXEL EN_INJ BIT CODE
    int cbit = 81+(MAX_COLS-col-1)*4;
    if(cbit < code.size()) code.set(cbit,enable);
}

void Client::setEnInjRow(int row, bool enable, nmoscode &code){
    //SET ENABLE INJ BIT ROW
    int rbit = 1283+(MAX_ROWS-row-1)*4;
    if(rbit < code.size()) code.set(rbit,enable);
    //std::cout << nmoscode_ << "\n";
}

void Client::setEnMeasRow(int row, bool enable, nmoscode &code){
    //SET ENABLE MEAS ROW
    int rbit = 1281+(MAX_ROWS-row-1)*4;
    if(rbit < code.size()) code.set(rbit,enable);
    //std::cout << nmoscode_ << "\n";
}

void Client::setChipPower(bool on, nmoscode &code){
    //SET CONFIG BITS on and off
    code.set(1456,on);
    code.set(1351,on);
    //std::cout << nmoscode_ << "\n";
}

unsigned int Client::initExtBias() {
    unsigned int output = 0;

    Client::opcode opcode = OPC_PROG_DACS;

    unsigned int ctrl = 3;
    for (unsigned int addr = 0; addr < externalDACs_.size(); ++addr) {
        this->sendOpcode(opcode);

        std::stringstream msg;
        msg.str("");
        msg << std::bitset<4>(0).to_string() << std::bitset<4>(ctrl).to_string() <<
        std::bitset<4>(addr).to_string() << std::bitset<12>(externalDACs_[addr]).to_string() <<
        std::bitset<8>(0).to_string();
        Client::DAC7568 code = std::stoul(msg.str(), nullptr, 2);
        Client::DAC7568 reply;
        this->sendMessage(code);
        this->readMessage(reply);
        if (reply == OPC_PROG_DACS) output+=pow(2,addr);
    }

    return output;
}

bool Client::decodeData(Client::DAC7568 data, int &col, int &row, int &timestamp, unsigned long int &fpgaTS) {
    std::stringstream ss;
    if(data[31]==1){
        ss.str("");
        for (int bit = 30; bit > -1; --bit) {
            ss << data[bit];
        }
        fpgaTS = std::stoul(ss.str(), nullptr, 2);
        return false;
    }

    ss.str("");
    for (int bit = 7; bit > -1; --bit) {
        ss << data[bit];
    }
    int roc_row = std::stoi(ss.str(), nullptr, 2);

    if (roc_row == 0) {
        std::cout << "TIMEOUT: waring, this massage is outdate and should not appear \n";
        col = -1; row = -1; timestamp = 0;
        return false;
    }

    ss.str("");
    for (int bit = 15; bit > 7; --bit) {
        ss << data[bit];
    }
    //timestamp = std::stoi(ss.str(), nullptr, 2);
    //timestamp = time_stamp;
    timestamp = gray_decode(std::stoi(ss.str(), nullptr, 2));

    ss.str("");
    for (int bit = 21; bit > 15; --bit) {
        ss << data[bit];
    }
    int roc_col = std::stoi(ss.str(), nullptr, 2);

    int roc_matrix = data[22];

    ss.str("");
    for (int bit = 30; bit > 22; --bit) {
        ss << data[bit];
    }
    fpgaTS = std::stoul(ss.str(), nullptr, 2);

    col = roc_col;
    row = roc_row;
    this->convertROCtoPixel(col,row,roc_matrix);

    //std::cout << std::dec << "roc_matrix:\t" << roc_matrix << "\troc_col:\t" << roc_col << "\ttime_stamp:\t" << time_stamp <<"\troc_row:\t" << roc_row << std::endl;
    //std::cout << std::dec << "roc_matrix:\t" << roc_matrix << "\tcol:\t\t" << col << "\ttime_stamp:\t" << time_stamp <<"\trow:\t\t" << row << std::endl;

    return true;
}

void Client::convertROCtoPixel(int &col, int &row, int roc_matrix){
    int roc_col = col;
    int roc_row = row;
    if ((roc_col % 2) != 0) {
        col = (roc_col - 1) / 2 * 5 + (roc_row - 1) / 16 + roc_matrix * 150;
        row = (roc_row - 1) / 16 * 16 + 15 - (roc_row - 1);
        //row = ((roc_row-1)/16+1)*15+(roc_row-1)/16 - (roc_row-1);
    }
    else {
        col = (roc_col / 2 - 1) * 5 + 2 + (roc_row + 7) / 16 + roc_matrix * 150;
        row = (roc_row + 7) / 16 * 16 + 7 - (roc_row - 1);
        //row = ((roc_row+7)/16*2+1)*7+ (roc_row+7)/16*2 - (roc_row-1);
    }
}

void Client::convertROCtoPixel(int &col, int &row){
    row++;
    col++;
    int matrix = 0;

    if(col>60){
        col-= 60;
        matrix++;
    }

    Client::convertROCtoPixel(col,row,matrix);
}

bool Client::connectSocket(std::string address, int port) {
    bool connected = false;
    connected = this->connectTCP(address.c_str(), port);
    if (connected) {
        std::cout << "Connection established with: " << address << " - port: " << port << std::endl;
        std::string message;
        this->recv(message);
        std::cout << message << "\n";
    }

    return connected;
}


unsigned int Client::configure() {
    unsigned int output = 0;
    Client::nmoscode code;

    for (int i = 0; i < 2; i++) {
        Client::opcode opcode = OPC_PROG_NMOS;
        this->sendOpcode(opcode);

        code = this->getNMOScode();
        if(this->sendProgramCode(code) && i==1) output++; //if output==1 -> only NMOS matrix responded correctly
    }

    for (int i = 0; i < 2; i++) {
        Client::opcode opcode = OPC_PROG_ANA1;
        this->sendOpcode(opcode);

        code = this->getANA1code();
        if(this->sendProgramCode(code) && i==1) output+=2; //if output==2 -> only ANA1 matrix responded correctly
    }

    for (int i = 0; i < 2; i++) {
        Client::opcode opcode = OPC_PROG_ANA2;
        this->sendOpcode(opcode);

        code = this->getANA2code();
        if(this->sendProgramCode(code) && i==1) output+=4; //if output==4 -> only ANA2 matrix responded correctly
    }

    for (int i = 0; i < 2; i++) {
        Client::opcode opcode = OPC_PROG_CMOS;
        this->sendOpcode(opcode);

        code = this->getCMOScode();
        if(this->sendProgramCode(code) && i==1) output+=8; //if output==8 -> only CMOS matrix responded correctly
    }

    if(doPowerCycle_) this->powerCycle();
    if(resetDACs_) this->resetDACs();

    return output; //if output==0 -> all failed, if output==15 -> all responded correctly
}


Client::nmoscode Client::removeZeros(Client::nmoscode b){
    for(unsigned int i=0; i<b.size(); i++){
        if(!b.test(i)){
            b<<=1;
        }
    }
    return b;
}

bool Client::sendProgramCode(Client::nmoscode code){
    Client::programcode pcode(wirebond_.to_string() + code.to_string());
    Client::nmoscode reply;
    this->sendMessage(pcode);
    this->readMessage(reply);

    return removeZeros(code) == removeZeros(reply);
}

bool Client::programMatrix(Client::Matrix matrix, Client::nmoscode code){
    Client::opcode opcode;
    if(matrix == NMOS) opcode=OPC_PROG_NMOS;
    else if(matrix == analog1) opcode=OPC_PROG_ANA1;
    else if(matrix == analog1) opcode=OPC_PROG_ANA2;
    else if(matrix == CMOS) opcode=OPC_PROG_CMOS;
    else{
        std::cout << "ERROR: matrix index [" <<  matrix << "] out of range.";
        return false;
    }

    this->sendOpcode(opcode);
    return this->sendProgramCode(code);
}

bool Client::loadConfig(std::string fileName) {
    std::fstream infile;
    infile.open(fileName, std::fstream::in | std::fstream::binary);

    std::stringstream msg;
    msg << std::bitset<9>(0).to_string();

    if (infile.is_open()) {
        for (int i = 0; i < 17; ++i) {
            unsigned int val;
            bool checked;
            infile >> val;
            msg << std::bitset<6>(val).to_string();
            infile >> checked;
            msg << std::bitset<1>(checked).to_string();
        }
        msg << std::bitset<1344>(0).to_string();
        Client::nmoscode code(msg.str());
        this->setNMOScode(code);
        this->setCMOScode(code);
        this->setANA1code(code);
        this->setANA2code(code);

        std::vector<unsigned int> dacs;
        for (int i = 0; i < 8; i++) {
            int val;
            infile >> val;
            dacs.push_back(val);
        }
        this->setExternalDACs(dacs);
    }
    else {
        std::cout << "Unable to open file: " << fileName << "\n";
        return false;
    }

    infile.close();
    return true;
}

bool Client::sendOpcode(Client::opcode &opcode){
    this->sendMessage(opcode);
    Client::opcode replycode;
    this->readMessage(replycode);
    if(opcode != replycode){
        opcode = replycode;
        std::cout << "ERROR: wrong opcode\n";
        return false;
    }
    else return true;
}

bool Client::setOption(unsigned int opt_n, int value) {
    if(opt_n>=configOptions_.size() || configOptions_[opt_n].range.first>value || configOptions_[opt_n].range.second<value) return false;
    configOptions_[opt_n].funk(value,this);

    return true;
}

Client::Client()  {
    externalDACs_ = {1305, 1092, 2970, 1360, 1230, 1911, 1805, 328};
    wirebond_ = wirebondcode(1);
    resetDACs_= true;

    Option wirebond;
    wirebond.name = "Wirebond scheme (1: only monolithic 2: full): ";
    wirebond.range = std::make_pair(1,2);
    wirebond.value = 2;
    wirebond.funk = Client::setFullWirebond;
    configOptions_.push_back(wirebond);

    Option reset_roc_dacs;
    reset_roc_dacs.name = "Reset ROC dacs: ";
    reset_roc_dacs.range = std::make_pair(0,1);
    reset_roc_dacs.value = 1;
    reset_roc_dacs.funk = Client::setResetDACs;
    configOptions_.push_back(reset_roc_dacs);

    Option power_cycle;
    power_cycle.name = "Power cycle chip: ";
    power_cycle.range = std::make_pair(0,1);
    power_cycle.value = 1;
    power_cycle.funk = Client::setPowerCycle;
    configOptions_.push_back(power_cycle);
}

void Client::resetDACs() {
    std::cout << "resetting Digital disctiminator DACs...\n";
    Client::nmoscode code, old_code;
    old_code = code = this->getNMOScode();
    for(unsigned int i = 0; i < MAX_ROCS; ++i) {
        this->setLoadDACCell(i, true, code);
    }
    for(unsigned int i = 0; i < MAX_ROC_COLS; ++i) {
        this->setDigThCol(i, 7,code);
        this->setEnHitbusCol(i,true,code);
        this->setEnDigCol(i,true,code);
    }
    this->programMatrix(Matrix::NMOS, code);
    this->programMatrix(Matrix::NMOS, old_code);

    old_code = code = this->getCMOScode();
    for(unsigned int i = 0; i < MAX_ROCS; ++i) {
        this->setLoadDACCell(i, true, code);
    }
    for(unsigned int i = 0; i < MAX_ROC_COLS; ++i) {
        this->setDigThCol(i, 7,code);
        this->setEnHitbusCol(i,true,code);
        this->setEnDigCol(i,true,code);
    }
    this->programMatrix(Matrix::CMOS, code);
    this->programMatrix(Matrix::CMOS, old_code);
}

void Client::powerCycle() {
    std::cout << "power cycle chip...\n";
    Client::nmoscode code, old_code;
    old_code = code = this->getNMOScode();

    code[6] = 0;
    code[15] = 0;

    this->programMatrix(Matrix::NMOS, code);
    this->programMatrix(Matrix::NMOS, old_code);

    sleep(1);

    old_code = code = this->getCMOScode();

    code[6] = 1;
    code[15] = 1;

    this->programMatrix(Matrix::CMOS, code);
    this->programMatrix(Matrix::CMOS, old_code);
}