/****************************************************************************
** Created by Stefano Terzo (stefano.terzo@cern.ch) on 04/10/16
**
** Institut de Física d'Altes Energies (IFAE)
** Edifici CN UAB Campus
** 08193 Bellaterra (Barcelona), Spain
**
****************************************************************************/

#include "Instrument.h"



bool Instrument::connectGPIB(int primary_pna_address, int bdindex) {
#ifdef GPIB_VISA
/*
 *  First we open a session to the VISA resource manager.  We are
 *  returned a handle to the resource manager session that we must
 *  use to open sessions to specific instruments.
 */
    status_ = viOpenDefaultRM(&defaultRM_);
    if (status_ < VI_SUCCESS)
    {
        printf("Could not open a session to the VISA Resource Manager!\n");
        return false;
    }

    /*
 *  Next we use the resource manager handle to open a session to a
 *  GPIB instrument .  A handle to this session is
 *  returned in the handle inst.
 */
    std::stringstream ss;
    ss << "GPIB::" << primary_pna_address << "::INSTR";
    vi_status_ = viOpen (defaultRM_, (ViRsrc)ss.str().c_str(), VI_NULL, VI_NULL, &vi_inst_);
    if (vi_status_ < VI_SUCCESS)
    {
        printf("Could not open a session to the device simulator\n");
        return false;
    }

    vi_status_ = viSetAttribute(vi_inst_, VI_ATTR_TMO_VALUE, 5000);

    return true;
#endif
    std::cout << "WARNING: do you have NI VISA drivers installed?\n";
    return false;
}

int Instrument::GPIBRead (std::string &message, unsigned int buffer_len) {
#ifdef GPIB_VISA
    char buffer[buffer_len];
    vi_status_ = viRead(vi_inst_, (ViBuf)buffer, buffer_len, &vi_rcount_);
    if(vi_status_ < VI_SUCCESS) {
        status_ = Status::readError;
    }
    else{
        message = buffer;
        status_ = Status::connected;
    }
    return status_;
#endif
    std::cout << "WARNING: do you have NI VISA drivers installed?\n";
    return -1;
}

int Instrument::GPIBWrite(std::string &cmd) {
#ifdef GPIB_VISA
    vi_status_ = viWrite(vi_inst_, (ViBuf) cmd.c_str(), (ViUInt32) cmd.size(), &vi_rcount_);
    if (vi_status_ < VI_SUCCESS) status_ = Status::writeError;
    else status_ = Status::connected;
    return status_;
#endif
    std::cout << "WARNING: do you have NI VISA drivers installed?\n";
    return -1;
}

int Instrument::TCPRead(std::string &message, unsigned int buffer_len, unsigned int timeout_s) {
    if(Socket::recv(message, buffer_len, timeout_s)) status_ = Status::connected;
    else status_ = Status::readError;
    return status_;
}

int Instrument::TCPWrite(std::string &cmd) {
    std::string msg = cmd + '\n';
    if(Socket::sendMessage(msg,10)) status_ = Status::connected;
    else status_ = Status::writeError;
    return status_;
}

int Instrument::sendCmd(std::string &cmd){
    int status = write(cmd);
    if(status==Status::connected && cmd.find('?')) status = read(cmd);

    return status;
}

int Instrument::write(std::string &cmd){
    if(interface_==Interface::GPIB){status_ = GPIBWrite(cmd);}
    else if(interface_==Interface::TCP){status_ =  TCPWrite(cmd);}
    else {
        std::cout << "Write ERROR: Interface not implemented\n";
        status_ = Status::noInterface;
    }
    return status_;
}

int Instrument::read(std::string &message, unsigned int buffer_len, unsigned int timeout_s) {
    if(interface_==Interface::GPIB){status_ = GPIBRead(message);}
    else if(interface_==Interface::TCP){status_ = TCPRead(message,buffer_len,timeout_s);}
    else {
        std::cout << "Read ERROR: Interface not implemented\n";
        status_ = Status::noInterface;
    }
    return status_;
}

int Instrument::connect(){
    return this->connect(address_, port_);
}

int Instrument::connect(std::string address, int port){
    bool connected;
    if(interface_==Interface::GPIB){connected = this->connectGPIB(std::stoi(address),port);}
    else if(interface_==Interface::TCP){connected = Socket::createSocket() && Socket::connectTCP(address, port);}
    else {
        std::cout << "Connection ERROR: Interface not implemented\n";
        return status_ = Status::noInterface;
    }
    if (!connected) {
        std::cout << "Connection error: " << address << " - " << port << "\n";
        return status_ = Status::disconnected;
    }

    std::string iden = this->identify();
    std::cout << "Connected to: " << iden << std::endl;

    this->detectInstrument(iden);

//    this->reset();
    address_ = address;
    port_ = port;

    return status_;
}

bool Instrument::reset() {
    std::string rst = "*RST";
    std::string clr = "*CLS";
    std::string opc = "*OPC";
    if ( write(rst)!=1 || write(clr)!=1 || write(opc)!=1) {
        std::cout << "Reset failed\n";
        return false; // if reset fails, stop the program
    }
    else std::cout << "Device successfully resetted!\n";

    return true;
}

void Instrument::detectInstrument(std::string id) {
    name_ = id;
    //std::cout << "size: " << id.size() << "\n";
    if(id.find("KEITHLEY")!=-1 || id.find("Keithley")!=-1){
        if(id.find("2400")!=-1){
            type_ = Type::VoltageSource;
            name_ = "KEITHLEY 2400";
        }
        if(id.find("2280S")!=-1){
            type_ = Type::VoltageSource;
            name_ = "KEITHLEY 2280S";
        }
        if(id.find("3390")!=-1){
            type_ = Type::PulseGenerator;
            name_ = "Keithley 3390";
        }
    }
    else if(id.find("Agilent")!=-1){
        if(id.find("33220A")!=-1){
            type_ = Type::PulseGenerator;
            name_ = "Agilent 33220A";
        }
        if(id.find("33250A")!=-1){
            type_ = Type::PulseGenerator;
            name_ = "Agilent 33250A";
        }
    }
    else if(id.find("THURLBY")!=-1){
        name_ = "THURLBY ";
        if(id.find("TG5011")!=-1){
            type_ = Type::PulseGenerator;
            name_ += " TG5011";
        }
	if(id.find("TG5012A")!=-1){
	    type_ = Type::PulseGenerator;
            name_ += " TG5012A";	
	}
    }
    else if(id.find("TEKTRONIX")!=-1){
        name_ = "TEKTRONIX";
        if(id.find("AFG3251C")!=-1){
            type_ = Type::PulseGenerator;
            name_ += " AFG3251C";
        }
    }
    else if(id.size()==131){
        name_ = "VOTSCH VC7207";
        type_ = Type::ClimateChamber;
    }
    else{
        type_ = Type::Unknown;
    }
}

Instrument::~Instrument() {
#ifdef GPIB_VISA
    if(interface_==Interface::GPIB){
        //vi_status_ = viClose(vi_inst_);
        //vi_status_ = viClose(defaultRM_);
    }
#endif
}

std::string Instrument::identify() {
    std::string msg = "*IDN?";
    this->write(msg);
    //std::cout << "sent: " << msg << "\n";
    if(this->read(msg)==Status::readError){
        this->sendMessage("$01I\r\n",100);
        this->read(msg);
    }
    return msg;
}

bool Instrument::operationComplete() {
    std::string msg = "*OPC?";
    this->write(msg);
//    std::cout << "sent: " << msg << "\n";
    this->read(msg);

    return msg == "1";
}












