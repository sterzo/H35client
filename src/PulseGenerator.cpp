/****************************************************************************
** Created by Stefano Terzo (stefano.terzo@cern.ch)  on 04/10/16
**
** Institut de Física d'Altes Energies (IFAE)
** Edifici CN UAB Campus
** 08193 Bellaterra (Barcelona), Spain
**
****************************************************************************/

#include "PulseGenerator.h"

#include <iomanip> //std::setprecision

void PulseGenerator::setDisplayOn(bool on) {
    std::string onoff;

    if(on)
        onoff = " ON";
    else onoff = " OFF";

    if(this->getName().find("Agilent")!=-1 || this->getName().find("Keithley")!=-1) {
        std::string msg = ":DISP" + onoff;
        this->write(msg);
        std::cout << "sent: " << msg << "\n";
    }
    else if(this->getName().find("THURLBY")!=-1 ||this->getName().find("TEKTRONIX")!=-1 ){

    }
    else{
        std::string msg = ":DISP" + onoff;
        this->write(msg);
        std::cout << "sent: " << msg << "\n";
    }
}

void PulseGenerator::setOutputLoad(int ohms) {
    if(this->getName().find("Agilent")!=-1 || this->getName().find("Keithley")!=-1) {
        std::string msg = "OUTP:LOAD " + std::to_string(ohms);
        this->write(msg);
        std::cout << "sent: " << msg << "\n";
    }
    else if(this->getName().find("THURLBY")!=-1 ||this->getName().find("TEKTRONIX")!=-1 ){

    }
    else{
        std::string msg = "OUTP:LOAD " + std::to_string(ohms);
        this->write(msg);
        std::cout << "sent: " << msg << "\n";
    }
}

void PulseGenerator::setPulse(float width_sec, float freq_hz) {
    std::string msg;
    if(this->getName().find("Agilent")!=-1 || this->getName().find("Keithley")!=-1) {
        msg = "FUNC PULS";
        this->write(msg);
        std::cout << "sent: " << msg << "\n";

        std::stringstream str;
        str << std::scientific << width_sec;
        msg = "PULS:WIDT " + str.str();
        this->write(msg);
        std::cout << "sent: " << msg << "\n";

        msg = "FREQ " + std::to_string(freq_hz);
        this->write(msg);
        std::cout << "sent: " << msg << "\n";
    }
    else if(this->getName().find("TEKTRONIX")!=-1 ) {
        msg = "FUNC PULS";
        this->write(msg);
        std::cout << "sent: " << msg << "\n";

        std::stringstream str;
        str << std::scientific << width_sec;
       // msg = "PULS:WIDT " + str.str();
       // this->write(msg);
       // std::cout << "sent: " << msg << "\n";

        msg = "FREQ " + std::to_string(freq_hz);
        this->write(msg);
        std::cout << "sent: " << msg << "\n";

        msg = "PULS:WIDT " + str.str();
        this->write(msg);
        std::cout << "sent: " << msg << "\n";
    }
    else if(this->getName().find("THURLBY")!=-1){
        msg = "WAVE PULSE";
        this->write(msg);
        std::cout << "sent: " << msg << "\n";

        std::stringstream str;
        str << std::scientific << width_sec;
        msg = "PULSWID " + str.str() + "\n";
        this->write(msg);
        std::cout << "sent: " << msg << "\n";

        msg = "PULSFREQ " + std::to_string(freq_hz);
        this->write(msg);
        std::cout << "sent: " << msg << "\n";
    }
    else{
        msg = "FUNC PULS";
        this->write(msg);
        std::cout << "sent: " << msg << "\n";

        std::stringstream str;
        str << std::scientific << width_sec;
        msg = "PULS:WIDT " + str.str();
        this->write(msg);
        std::cout << "sent: " << msg << "\n";

        msg = "FREQ " + std::to_string(freq_hz);
        this->write(msg);
        std::cout << "sent: " << msg << "\n";
    }
}

void PulseGenerator::setAmplitude(float volts) {
    std::string msg;
    std::stringstream ss;
    if(this->getName().find("Agilent")!=-1 || this->getName().find("Keithley")!=-1 || this->getName().find("TEKTRONIX")!=-1 ) {
//        std::string msg = "*WAI";
//        this->write(msg);
//        std::cout << "sent: " << msg << "\n";

        msg = "VOLT:LOW 0V";
        this->write(msg);
        std::cout << "sent: " << msg << "\n";

        ss.str("");
        ss << "VOLT:HIGH " << std::setprecision(3) << volts << "V";
        msg = ss.str();
        this->write(msg);
        std::cout << "sent: " << ss.str() << "\n";
    }
    else if(this->getName().find("THURLBY")!=-1){
        ss.str("");
        ss << "HILVL " << std::setprecision(3) << volts;
        msg = ss.str();
        this->write(msg);
        std::cout << "sent: " << ss.str() << "\n";

        msg = "LOLVL 0";
        this->write(msg);
        std::cout << "sent: " << msg << "\n";

        ss.str("");
        ss << "HILVL " << std::setprecision(3) << volts;
        msg = ss.str();
        this->write(msg);
        std::cout << "sent: " << ss.str() << "\n";
    }
    else{
        std::string msg = "*WAI";
        this->write(msg);
        std::cout << "sent: " << msg << "\n";

        msg = "VOLT:LOW 0V";
        this->write(msg);
        std::cout << "sent: " << msg << "\n";

        ss.str("");
        ss << "VOLT:HIGH " << std::setprecision(3) << volts << "V";
        msg = ss.str();
        this->write(msg);
        std::cout << "sent: " << ss.str() << "\n";
    }
}

void PulseGenerator::setBurst(int n) {
    std::string msg;
    std::stringstream ss;
    if(this->getName().find("Agilent")!=-1 || this->getName().find("Keithley")!=-1) {
        msg = "TRIG:SOUR BUS";
        this->write(msg);
        std::cout << "sent: " << msg << "\n";

        ss.str("");
        ss << ":BURS:NCYC " << n;
        msg = ss.str();
        this->write(msg);
        std::cout << "sent: " << ss.str() << "\n";

        msg = ":BURS:STAT ON";
        this->write(msg);
        std::cout << "sent: " << msg << "\n";
    }
    else if(this->getName().find("TEKTRONIX")!=-1) {
        msg = "SOUR:BURS:MODE TRIG";
        this->write(msg);
        std::cout << "sent: " << msg << "\n";

        msg = "TRIG:SOUR EXT";
        this->write(msg);
        std::cout << "sent: " << msg << "\n";

        ss.str("");
        ss << ":BURS:NCYC " << n;
        msg = ss.str();
        this->write(msg);
        std::cout << "sent: " << ss.str() << "\n";

        msg = ":BURS:STAT ON";
        this->write(msg);
        std::cout << "sent: " << msg << "\n";
    }
    else if(this->getName().find("THURLBY")!=-1){
        msg = "BST NCYC";
        this->write(msg);
        std::cout << "sent: " << msg << "\n";

        ss.str("");
        ss << "BSTCOUNT " << n;
        msg = ss.str();
        this->write(msg);
        std::cout << "sent: " << ss.str() << "\n";

        msg = "BSTTRGSRC MAN";
        this->write(msg);
        std::cout << "sent: " << msg << "\n";
    }
    else{
        msg = "TRIG:SOUR BUS";
        this->write(msg);
        std::cout << "sent: " << msg << "\n";

        ss.str("");
        ss << ":BURS:NCYC " << n;
        msg = ss.str();
        this->write(msg);
        std::cout << "sent: " << ss.str() << "\n";

        msg = ":BURS:STAT ON";
        this->write(msg);
        std::cout << "sent: " << msg << "\n";
    }
}

void PulseGenerator::setOutput(bool on, int channel) {
    std::string onoff;
    std::string ch = "";

    if(on)
        onoff = " ON";
    else onoff = " OFF";

    if(channel>1) ch = std::to_string(channel);

    std::string msg;
    if(this->getName().find("Agilent")!=-1 || this->getName().find("Keithley")!=-1 || this->getName().find("TEKTRONIX")!=-1) {
        msg = ":OUTP" + ch + onoff;
        this->write(msg);
        std::cout << "sent: " << msg << "\n";
    }
    else if(this->getName().find("THURLBY")!=-1){
        msg = "OUTPUT" + ch + onoff;
        this->write(msg);
        std::cout << "sent: " << msg << "\n";
    }
    else{
        msg = ":OUTP" + ch + onoff;
        this->write(msg);
        std::cout << "sent: " << msg << "\n";
    }
}

std::string PulseGenerator::status() {
    std::string msg;
    std::stringstream ss;
    if(this->getName().find("Agilent")!=-1 || this->getName().find("Keithley")!=-1) {
        msg = "APPL?";
        this->write(msg);
        //std::cout << "sent: " << msg << "\n";
        this->read(msg);
        //std::cout << "received: " << msg << "\n";
        ss << msg;

        msg = "PULS:WIDT?";
        this->write(msg);
        //std::cout << "sent: " << msg << "\n";
        this->read(msg);
        //std::cout << "received: " << msg << "\n";
        ss << " - Width: " + msg;
    }
    else if(this->getName().find("TEKTRONIX")!=-1){

    }
    else if(this->getName().find("THURLBY")!=-1){

    }
    else{
        msg = "APPL?";
        this->write(msg);
        this->read(msg);
        ss << msg;

        msg = "PULS:WIDT?";
        this->write(msg);
        this->read(msg);
        ss << " - Width: " + msg;
    }
    return ss.str();
}

void PulseGenerator::trigger() {
    std::string msg;
    if (this->getName().find("Agilent") != -1 || this->getName().find("Keithley")!=-1) {
        msg = "*WAI";
        this->write(msg);
        //std::cout << "sent: " << msg << "\n";
        msg = ":TRIG";
        this->write(msg);
    }
    else if (this->getName().find("THURLBY") != -1 || this->getName().find("TEKTRONIX")!=-1) {
        msg = "*TRG";
        this->write(msg);
        //std::cout << "sent: " << msg << "\n";
    }
    else{
        msg = "*WAI";
        this->write(msg);
        msg = ":TRIG";
        this->write(msg);
    }
}













