/****************************************************************************
** Authors: Stefano Terzo (stefano.terzo@cern.ch)
**
** Institut de Física d'Altes Energies (IFAE)
** Edifici CN UAB Campus
** 08193 Bellaterra (Barcelona), Spain
**
****************************************************************************/

#ifndef ADVANCEDFORM_H
#define ADVANCEDFORM_H

#include <stdio.h>
#include <iostream>
#include <map>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>

#include <QWidget>
#include <QPixmap>
#include <QCheckBox>
#include <QLabel>
#include <QGridLayout>
#include <Qt>
#include <QLineEdit>
#include <QFormLayout>
#include <build/ui_advancedform.h>

#include "pageForm.h"

#include "Client.h"
#include "SocketException.h"

#define LINEBITS 32

namespace Ui {
class AdvancedForm;
}

class AdvancedForm : public PageForm {
Q_OBJECT

public:
    explicit AdvancedForm(ClientWidget *client, QWidget *parent = 0);

    ~AdvancedForm();

public slots:

    void initExtBias();

    void compareData_lineEdit();

private slots:

    void sendDAC7568();

    void on_send_command_pushbutton_clicked();

    void on_stop_command_pushbutton_clicked();

private:
    Ui::AdvancedForm *ui;

    std::vector<QCheckBox *> rowControl_checkBoxes_, columnControl_checkBoxes_, rocControl_checkBoxes_;
    std::vector<QWidget *> configControl_widgets_;
    std::vector<QLineEdit *> dataSent_lineEdits_, dataRecv_lineEdits_;

    Client *client_;
    unsigned char sentOpcode_, opcode_;
    bool stopScan_;

    enum Action {
        sendSingleCommand,
        sendOPCODE,
        initialiseExternalBias,
        programNMOS,
        programCMOS,
        programANA1,
        programANA2,
        programDAC7568,
        enableBiasPads,
        disableBiasPads,
        NMOSdataAcquisition,
        NMOSdataAcquisitionDMA,
        CMOSdataAcquisition,
        CMOSdataAcquisitionDMA,
        NMOStluAcquisition,
        NMOStluAcquisitionDMA,
        CMOStluAcquisition,
        CMOStluAcquisitionDMA,
        NMOStlusAcquisition,
        NMOStlusAcquisitionDMA,
        CMOStlusAcquisition,
        CMOStlusAcquisitionDMA,
        readDebug
    };

    bool acquireData();

    void dataAcquisition(unsigned char opcode, unsigned long int timeout);

    void readMessage(std::string &message);

//    void readMessage(unsigned char &byte);

    void readMessage(Client::DAC7568 &code);

    void readMessage(Client::nmoscode code);

    void sendMessage(Client::nmoscode code);

    void sendMessage(unsigned char byte);

    std::string get_dac_sent_verticalLayout();

    std::string get_dac_control_tabWidget();

    void set_data_lineEdit(std::vector<QLineEdit *> dacs, Client::nmoscode code);

    void triggeredAcquisition(unsigned char opcode, unsigned long int timeout);
};

#endif // ADVANCEDFORM_H
