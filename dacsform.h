/****************************************************************************
** Authors: Stefano Terzo (stefano.terzo@cern.ch)
**
** Institut de Física d'Altes Energies (IFAE)
** Edifici CN UAB Campus
** 08193 Bellaterra (Barcelona), Spain
**
****************************************************************************/

#ifndef DACSFORM_H
#define DACSFORM_H

#include <fstream>

#include <QWidget>
#include <QPixmap>
#include <QCheckBox>
#include <QLabel>
#include <QGridLayout>
#include <QSpinBox>
#include <Qt>
#include <QFileDialog>
#include <QLineEdit>
#include <QFormLayout>
#include <QAbstractSpinBox>
#include <QStackedWidget>
#include <QTabWidget>

#include "pageForm.h"
#include "pixelmap.h"
#include "DacList.h"

namespace Ui {
class DACsForm;
}

class DACsForm : public PageForm
{
    Q_OBJECT

public:
    explicit DACsForm(ClientWidget *client,QWidget *parent = 0);
    ~DACsForm();

    std::vector<PixelMap*> getAnalogPixMaps(){return analogPixMaps_;}
    std::vector<PixelMap*> getDigitalPixMaps(){return digitalPixMaps_;}

signals:
    void dacChanged();

public slots:
    bool save(QString fileName);
    bool load(QString fileName);
    bool load_TDAC(QString fileName, PixelMap *pixelmap);
    void updateDACs();

private slots:
    void updatePixelValue();

    void mapClick(int matrix, int col, int row);

    void on_setPixel_pushButton_clicked();

    void on_setCol_pushButton_clicked();

    void on_setRow_pushButton_clicked();

    void on_setMatrix_pushButton_clicked();

    void updateDACconf();

    void on_loadMask_pushButton_clicked();

    void on_applyMask_pushButton_clicked();

private:
    Ui::DACsForm *ui;

    ClientWidget *client_;

    QStackedWidget *currentMatrixDacs_;

    std::vector<PixelMap*> analogPixMaps_,digitalPixMaps_;
    //std::vector<QSpinBox*> internalDacSpinBoxes_;
    std::vector<QSpinBox*> externalDacSpinBoxes_;
    const std::vector<unsigned int> defaultExternalDacs_ = {1305,1092,2970,1360,1230,1911,1805,2702};
    const std::vector<unsigned int> defaultInternalDacs_ = {0x1,0xa,0xa,0xa,
                                                            0xa,0xa,0x1,0x1,
                                                            0x5,0xa,0x1e,0xf,
                                                            0x14,0x5,0x32,0x3c,
                                                            0xA};
    //std::vector<QCheckBox*> internalDacCheckBoxes_;
    std::vector<DacList*> daclists_;

    enum TDAC {analog, digital};
    enum TDACmask {CMOSanalog, CMOSdigital, NMOSanalog, NMOSdigital};
};

#endif // DACSFORM_H
