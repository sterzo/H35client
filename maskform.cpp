/****************************************************************************
** Authors: Stefano Terzo (stefano.terzo@cern.ch)
**
** Institut de Física d'Altes Energies (IFAE)
** Edifici CN UAB Campus
** 08193 Bellaterra (Barcelona), Spain
**
****************************************************************************/

#include "maskform.h"
#include "ui_maskform.h"

MaskForm::MaskForm(ClientWidget *client, QWidget *parent) :
    PageForm(client,parent),
    ui(new Ui::MaskForm)
{

    ui->setupUi(this);

    QWidget *maskMatrix = new QWidget(this);
    QGridLayout *maskMatrix_gridLayout = new QGridLayout(this);
    //H35 Matrix =============================================
    pixMaps_.push_back(new PixelMap(Client::Matrix::NMOS ,MAX_COLS, MAX_ROWS, 2, 10, 1, this));
    maskMatrix_gridLayout->addWidget(pixMaps_.back(),0,0);
    pixMaps_.push_back(new PixelMap(Client::Matrix::analog1 ,300, 23, 2, 10, 1, this));
    maskMatrix_gridLayout->addWidget(pixMaps_.back(),1,0);
    pixMaps_.push_back(new PixelMap(Client::Matrix::analog2 ,300, 23, 2, 10, 1, this));
    maskMatrix_gridLayout->addWidget(pixMaps_.back(),2,0);
    pixMaps_.push_back(new PixelMap(Client::Matrix::CMOS ,MAX_COLS, MAX_ROWS, 2, 10, 1, this));
    maskMatrix_gridLayout->addWidget(pixMaps_.back(),3,0);
    maskMatrix->setLayout(maskMatrix_gridLayout);
    ui->maskMatrix_scrollArea->setWidget(maskMatrix);
    //=========================================================
    maskMatrix_gridLayout->setHorizontalSpacing(-1);
    maskMatrix_gridLayout->setVerticalSpacing(-1);
    ui->matrix_spinBox->setMaximum(pixMaps_.size()-1);

    connect(ui->row_spinBox,SIGNAL(valueChanged(int)),this,SLOT(updatePixelValue()));
    connect(ui->col_spinBox,SIGNAL(valueChanged(int)),this,SLOT(updatePixelValue()));
    connect(ui->matrix_spinBox,SIGNAL(valueChanged(int)),this,SLOT(updatePixelValue()));
    for(int m=0; m<pixMaps_.size(); ++m)
    {
        connect(pixMaps_[m],SIGNAL(pixelClicked(int,int,int)),this,SLOT(mapClick(int, int,int)));
    }
}

MaskForm::~MaskForm()
{
    delete ui;
}

void MaskForm::on_test_pushButton_clicked()
{
    for(int c=0; c<MAX_COLS; ++c) {
        for(int r=0; r<MAX_ROWS; ++r) {
            pixMaps_[ui->matrix_spinBox->value()]->setPixel(c, r,true);
        }
    }
    pixMaps_[ui->matrix_spinBox->value()]->updateMap();
}


void MaskForm::on_pixelStatus_checkBox_clicked(bool checked)
{
    pixMaps_[ui->matrix_spinBox->value()]->setPixel(ui->col_spinBox->value(), ui->row_spinBox->value(), checked);
    pixMaps_[ui->matrix_spinBox->value()]->updateMap();
}

void MaskForm::updatePixelValue(){
    ui->pixelStatus_checkBox->setChecked(pixMaps_[ui->matrix_spinBox->value()]->getPixelStatus(ui->col_spinBox->value(),ui->row_spinBox->value()));
}

void MaskForm::mapClick(int matrix, int col, int row){
    ui->matrix_spinBox->setValue(matrix);
    ui->col_spinBox->setValue(col);
    ui->row_spinBox->setValue(row);
    this->updatePixelValue();
}

void MaskForm::updateMasks(Client::nmoscode code) {
    pixMaps_[0]->setMaskFromCode(code);
}
